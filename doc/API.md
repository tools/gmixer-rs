# ĞMixer HTTP API Documentation

Requests and responses are encoded with [bincode](https://crates.io/crates/bincode).

MIME type is `application/x-gmixer`.

### POST /get-mix-confirm

Content: `TxIdSeed`

Returns: `Result<MixConfirmOnionWrapperSecret, ResponseError>`

Description: Get the mix confirm identified by `input_tx_id_seed[1]`.

### POST /get-tx-id-list

Content: `TxIdListHash`

Returns: `Result<TxIdList, ResponseError>`

Description: Get a tx id list.

### GET /get-tx-id-lists

Returns: `Vec<TxIdListStore>`

Description: Get all the tx id lists known by the node.

### POST /post-tx-id-lists

Content: `Vec<TxIdListStore>`

Returns: `Result<(), ResponseError>`

Description: Send tx id lists to the node.

### POST /mix-confirm

Content: `MixConfirmOnionWrapperSecret`

Returns: `Result<(), ResponseError>`

Description: Send a mix confirm to the node.

### POST /mix-demand

Content: `MixDemandOnionWrapperSecret`

Returns: `Result<(), ResponseError>`

Description: Send a mix demand to the node.

### GET /node

Returns: `NodeSig`

Description: Get the node's info.

### POST /node

Content: `NodeSig`

Returns: `Result<(), ResponseError>`

Description: Register a node info.

### GET /nodes

Returns: `Vec<NodeSig>`

Description: Get the node's known nodes info (the info of the node itself is the first item).

### GET /pubkey-delegation

Returns: `Option<PubkeyDelegationSig>`

Description: Get the node's pubkey delegation signature.
