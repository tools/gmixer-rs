# ĞMixer

ĞMixer-rs is a rusty implementation of ĞMixer, [precedently implemented in Python](https://git.duniter.org/tuxmain/gmixer-py).

Work in progress.

## Protocol

[General protocol description](https://zettascript.org/projects/gmixer)

[HTTP API Documentation](doc/API.md) ([draft pad](https://pad.p2p.legal/BMgVFk8VRl-5UiWIcks28Q#))

## Build

[Install Rustup](https://rustup.rs/) and switch to nightly:

    $ rustup toolchain install nightly && rustup default nightly

Clone the repo:

    $ git clone https://git.duniter.org/tuxmain/gmixer-rs && cd gmixer-rs

### CLI client/server

    $ cargo build --release -p gmixer

Executable is `./target/release/gmixer`

Beware of your version of `glibc`: an executable compiled on a system using a recent version of `glibc` will not be compatible with older systems. (e.g. it won't run on Debian if it's compiled on ArchLinux)

This was only tested on GNU/Linux.

### GUI client (browser)

    $ cd gui
    $ sh build.sh

## Install

Put the executable somewhere accessible with your `PATH`. (typically `/usr/bin`)

## Use

    # init config
    gmixer server init

If you don't know what address to listen to, use `0.0.0.0:10953`. Listen address must follow the format `ip:port`.

Public address contains optionally the protocol (`http://` or `https://`) and the port.

    # sync nodes
    gmixer server sync -n https://other-node.tld:10953
    # start node
    gmixer server start
    
    # sync client
    gmixer client sync -n my-example-node.tld
    # prepare mix (amount is in Ğ1 cents, e.g. 123 is for 1.23Ğ1)
    gmixer client mix -r <recipient_pubkey> -a <amount>
    # wait a minute to let the nodes work...
    # then fetch mix confirm
    gmixer client confirm
    # send the tx to start mixing!
    gmixer client tx

NB: the `sync` subcommand understands IPv6, IPv4 and hostnames. Protocol and port are optional. Defaults are `http` and `10953`.

### Run multiple instances

Each instance needs its own data directory. Change it using `-d` option. It also needs a different port. Change both public and bind port in config.

You can sign several pubkey delegation documents for several nodes with the same member pubkey.

## Development

### v0.1.0

will be released when it will be working correctly, and usable in commandline by any geek.

### v0.2.0

will be released when the protocol will be fully implemented, with identity discredit.

### v1.0.0

will be released when known security and UX problems and bugs will be fixed.

## License

CopyLeft 2020-2021 Pascal Engélibert

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/.
