mkdir /srv/http/pub/gmixer-gui
cp -r static/* /srv/http/pub/gmixer-gui/
RUSTFLAGS="--remap-path-prefix $HOME=~" wasm-pack build --target web --out-dir /srv/http/pub/gmixer-gui/
