pub mod unlock;

use yew::prelude::*;

pub trait Dialog {
	fn new() -> Self;
	fn render(&self) -> Html;
}
