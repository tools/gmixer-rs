use crate::nodes::Path;

use dup_crypto::seeds::Seed32;
use gmixer_common::prelude::*;
use serde::{Deserialize, Serialize};
use std::collections::HashSet;
use web_sys::console;

#[derive(Debug, Deserialize, PartialEq, Serialize)]
pub enum MixStatus {
	/// Step 4: output tx received
	Complete,
	/// Step 2: confirmed, unsent tx
	Confirmed,
	/// Step 0: unsent mix demands
	Unsent,
	/// Step 1: sent, waiting for confirm
	WaitingForConfirm,
	/// Step 3: confirms, tx sent, waiting for output tx
	WaitingForTx,
}

impl MixStatus {
	pub fn to_string(&self) -> String {
		match self {
			Self::Complete => "Complete",
			Self::Confirmed => "Confirmed",
			Self::Unsent => "Unsent",
			Self::WaitingForConfirm => "Waiting for confirm",
			Self::WaitingForTx => "Waiting for transaction",
		}
		.into()
	}
}

#[derive(Deserialize, Serialize)]
pub struct PendingMixStore {
	pub ephemeral_keypair_seeds: Vec<Seed32>,
	pub mix_confirms: Vec<documents::mix_confirm::MixConfirmSig>,
	pub mix_demand_onion: documents::mix_demand::MixDemandOnion,
	pub mix_demands: Vec<documents::mix_demand::MixDemand>,
	pub node: Option<documents::node::Node>,
	pub output_tx_id_seed_1: TxIdSeed,
	pub path: Path,
	pub status: MixStatus,
}

fn set(k: &str, v: &str) {
	web_sys::window()
		.unwrap()
		.local_storage()
		.unwrap()
		.unwrap()
		.set(k, v)
		.map_err(|e| console::error_1(&format!("Storage: cannot set `{}`: {:?}", k, e).into()))
		.ok();
}

fn get(k: &str) -> Option<String> {
	web_sys::window()
		.unwrap()
		.local_storage()
		.unwrap()
		.unwrap()
		.get(k)
		.map_err(|e| console::error_1(&format!("Storage: cannot get `{}`: {:?}", k, e).into()))
		.unwrap()
}

fn delete(k: &str) {
	web_sys::window()
		.unwrap()
		.local_storage()
		.unwrap()
		.unwrap()
		.delete(k)
		.map_err(|e| console::error_1(&format!("Storage: cannot delete `{}`: {:?}", k, e).into()))
		.ok();
}

/// List pending mixes by input_tx_id_seed_0
pub fn list_pending_mixes() -> HashSet<protocol::TxIdSeed> {
	get("pending_mixes")
		.map(|raw| {
			base64::decode_config(raw, base64::URL_SAFE_NO_PAD)
				.map(|raw| bincode::deserialize(&raw).ok())
				.ok()
		})
		.flatten()
		.flatten()
		.unwrap_or_else(HashSet::new)
}

pub fn iter_pending_mixes() -> impl Iterator<Item = PendingMixStore> {
	list_pending_mixes()
		.into_iter()
		.filter_map(|k| get_pending_mix(&k))
}

pub fn set_pending_mix(pending_mix_store: &PendingMixStore) {
	set(
		&pending_mix_store.mix_demands[0]
			.input_tx_id_seed_0
			.to_string(),
		&base64::encode_config(
			bincode::serialize(pending_mix_store).unwrap(),
			base64::URL_SAFE_NO_PAD,
		),
	);
	let mut pending_mixes = list_pending_mixes();
	pending_mixes.insert(pending_mix_store.mix_demands[0].input_tx_id_seed_0.clone());
	set(
		"pending_mixes",
		&base64::encode_config(
			bincode::serialize(&pending_mixes).unwrap(),
			base64::URL_SAFE_NO_PAD,
		),
	);
}

pub fn get_pending_mix(input_tx_id_seed_0: &protocol::TxIdSeed) -> Option<PendingMixStore> {
	get(&input_tx_id_seed_0.to_string())
		.map(|raw| {
			base64::decode_config(raw, base64::URL_SAFE_NO_PAD)
				.map(|raw| bincode::deserialize(&raw).ok())
				.ok()
		})
		.flatten()
		.flatten()
}

pub fn remove_pending_mix(input_tx_id_seed_0: &protocol::TxIdSeed) {
	delete(&input_tx_id_seed_0.to_string());
}
