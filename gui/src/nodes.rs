use crate::config;

use dup_crypto::{bases::b58::ToBase58, keys::ed25519::PublicKey};
use gmixer_common::prelude::*;
use serde::{Deserialize, Serialize};
use std::{
	collections::HashMap,
	sync::{Arc, RwLock},
};
use web_sys::console;

pub struct NodeStore {
	pub node_sig: documents::node::NodeSig,
	pub received_time: u64,
}

pub struct Nodes {
	pub nodes: HashMap<PublicKey, HashMap<NodeId, NodeStore>>,
}

impl Nodes {
	pub fn new() -> Self {
		Self {
			nodes: HashMap::new(),
		}
	}
}

#[derive(Debug)]
pub enum BootstrapError {
	Network(errors::Error),
}

pub async fn bootstrap(
	nodes: Arc<RwLock<Nodes>>,
	config: Arc<config::ClientConfig>,
) -> Result<(), BootstrapError> {
	let resp = api::get_nodes(&reqwest::Client::new(), &config.network.bootstrap)
		.await
		.map_err(BootstrapError::Network)?;
	let mut nodes = nodes.write().unwrap();
	let t = web_sys::window().unwrap().performance().unwrap().now() as u64 / 1000;

	for node_sig in resp {
		if node_sig.verify(&config.protocol).is_ok() {
			if let Some(node_stores) = nodes.nodes.get_mut(&node_sig.inner.pubkey) {
				if let Some(node_store) = node_stores.get(&node_sig.inner.id) {
					if node_store.node_sig.inner.sigtime >= node_sig.inner.sigtime {
						continue;
					}
				}
				console::log_1(
					&format!(
						"Add node {}@{}",
						node_sig.inner.pubkey.to_base58(),
						node_sig.inner.address.to_string()
					)
					.into(),
				);
				node_stores.insert(
					node_sig.inner.id.clone(),
					NodeStore {
						node_sig,
						received_time: t,
					},
				);
				continue;
			}
			let pubkey = node_sig.inner.pubkey;
			let mut node_stores = HashMap::new();
			console::log_1(
				&format!(
					"Add node {}@{}",
					pubkey.to_base58(),
					node_sig.inner.address.to_string()
				)
				.into(),
			);
			node_stores.insert(
				node_sig.inner.id.clone(),
				NodeStore {
					node_sig,
					received_time: t,
				},
			);

			nodes.nodes.insert(pubkey, node_stores);
		}
	}

	Ok(())
}

#[derive(Clone, Deserialize, Serialize)]
pub struct Path {
	pub path: Vec<PublicKey>,
}

pub enum PathError {
	NotEnoughNodes,
}

impl Path {
	pub fn new(nodes: &Nodes, layers: usize) -> Result<Self, PathError> {
		if nodes.nodes.len() < layers {
			return Err(PathError::NotEnoughNodes);
		}

		let mut nodes = nodes.nodes.keys().collect();

		Ok(Self {
			path: ShuffleIter::new(&mut nodes)
				.enumerate()
				.take_while(|(i, _)| i < &layers)
				.map(|(_, pubkey)| *pubkey)
				.collect(),
		})
	}
}
