#![recursion_limit = "1024"]

mod config;
mod dialog;
mod nodes;
mod storage;

use dup_crypto::{
	bases::b58::ToBase58,
	keys::{
		ed25519::{
			Ed25519KeyPair, KeyPairFromSaltedPasswordGenerator, KeyPairFromSeed32Generator,
			PublicKey, SaltedPassword,
		},
		KeyPair, PublicKey as _,
	},
	seeds::Seed32,
};
use gmixer_common::prelude::*;
use itertools::izip;
use std::{
	collections::{BTreeMap, HashMap, HashSet},
	sync::{Arc, RwLock},
};
use wasm_bindgen::prelude::*;
use wasm_bindgen_futures::spawn_local;
use web_sys::console;
use yew::{prelude::*, html::Scope};

struct Model {
	link: Arc<Scope<Self>>,
	mix_demand_form: MixDemandFormModel,

	config: Arc<config::ClientConfig>,
	keypairs: HashMap<PublicKey, Ed25519KeyPair>,
	modals: BTreeMap<usize, Modal>,
	modals_ai: usize,
	nodes: Arc<RwLock<nodes::Nodes>>,
	tasks: Arc<RwLock<Tasks>>,
}

struct MixDemandFormModel {
	amount: Amount,
	keypair: Option<Ed25519KeyPair>,
	layers: usize,
	path: Option<nodes::Path>,
	scrypt: (String, String),
}

struct Tasks {
	poll_mix_confirm: HashSet<TxIdSeed>,
}

struct UnlockModal {
	keypair: Option<Ed25519KeyPair>,
	scrypt: (String, String),
}

enum Modal {
	Unlock(UnlockModal),
}

enum Msg {
	AmountChanged(f64),
	ChangePath,
	CloseModal(usize),
	LayersChanged(usize),
	RemoveMix(TxIdSeed),
	ScryptPswChanged(String),
	ScryptSaltChanged(String),
	SendMixDemand(FocusEvent),
	SendTx(TxIdSeed),
	Unlock(usize, FocusEvent),
	UnlockModal(PublicKey),
	UnlockModalScryptPswChanged(usize, String),
	UnlockModalScryptSaltChanged(usize, String),
	Nope,
}

impl Component for Model {
	type Message = Msg;
	type Properties = ();
	fn create(ctx: &Context<Self>) -> Self {
		let link = Arc::new(ctx.link().clone());

		let config = Arc::new(config::ClientConfig::default());
		let nodes = Arc::new(RwLock::new(nodes::Nodes::new()));

		spawn_local({
			let link = link.clone();
			let nodes = nodes.clone();
			let config = config.clone();
			async move {
				if let Err(e) = nodes::bootstrap(nodes, config).await {
					console::error_1(&format!("Bootstrap: {:?}", e).into());
				} else {
					link.send_message(Msg::ChangePath);
				}
			}
		});

		Self {
			link,
			mix_demand_form: MixDemandFormModel {
				amount: Amount { val: 100, base: 0 },
				keypair: None,
				layers: 3,
				path: None,
				scrypt: ("".into(), "".into()),
			},
			config,
			keypairs: HashMap::new(),
			modals: BTreeMap::new(),
			modals_ai: 0,
			nodes,
			tasks: Arc::new(RwLock::new(Tasks {
				poll_mix_confirm: HashSet::new(),
			})),
		}
	}

	fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> ShouldRender {
		let window = web_sys::window().unwrap();
		let document = window.document().unwrap();
		let mut privkey_changed = false;
		match msg {
			Msg::AmountChanged(amount) => {
				self.mix_demand_form.amount = Amount {
					val: (amount * 100.0) as u32,
					base: 0,
				};
			}
			Msg::ChangePath => {
				self.mix_demand_form.path =
					match nodes::Path::new(&self.nodes.read().unwrap(), self.mix_demand_form.layers)
					{
						Ok(path) => Some(path),
						Err(nodes::PathError::NotEnoughNodes) => {
							console::error_1(&"Not enough nodes".into());
							None
						}
					}
			}
			Msg::LayersChanged(layers) => {
				if layers != self.mix_demand_form.layers {
					self.link.send_message(Msg::ChangePath);
				}
				self.mix_demand_form.layers = layers;
			}
			Msg::RemoveMix(tx_id_seed_0) => {
				storage::remove_pending_mix(&tx_id_seed_0);
			}
			Msg::ScryptPswChanged(psw) => {
				self.mix_demand_form.scrypt.1 = psw;
				privkey_changed = true;
			}
			Msg::ScryptSaltChanged(salt) => {
				self.mix_demand_form.scrypt.0 = salt;
				privkey_changed = true;
			}
			Msg::SendMixDemand(event) => {
				let event: &web_sys::Event = event.as_ref();
				let form = event.target().unwrap();
				let form: &JsValue = form.as_ref();
				let form = web_sys::HtmlFormElement::from(form.clone());
				match web_sys::FormData::new_with_form(&form) {
					Ok(formdata) => {
						let amount = Amount {
							val: (formdata
								.get("amount")
								.as_string()
								.unwrap()
								.parse::<f64>()
								.unwrap() * 100.0) as u32,
							base: 0,
						};
						let keypair = self.mix_demand_form.keypair.clone().unwrap();
						self.keypairs.insert(keypair.public_key(), keypair.clone());

						let path = self.mix_demand_form.path.as_ref().unwrap();

						let recipient =
							PublicKey::from_base58(&formdata.get("recipient").as_string().unwrap())
								.unwrap();
						let ephemeral_keypair_seeds: Vec<Seed32> = (0..path.path.len())
							.map(|_| Seed32::random().unwrap())
							.collect();
						let mix_demands = documents::mix_demand::gen_mix_demands(
							&amount,
							&keypair,
							"g1",
							&ephemeral_keypair_seeds
								.iter()
								.map(|seed| Ed25519KeyPair::from_seed(seed.clone()))
								.collect::<Vec<Ed25519KeyPair>>(),
							&path.path,
							&recipient,
						);
						let pending_mix_store = storage::PendingMixStore {
							ephemeral_keypair_seeds,
							mix_confirms: Vec::new(),
							mix_demand_onion: mix_demands.1,
							mix_demands: mix_demands.0,
							node: None,
							output_tx_id_seed_1: TxIdSeed::generate(),
							path: path.clone(),
							status: storage::MixStatus::Unsent,
						};
						let nodes = self.nodes.clone();
						let link = self.link.clone();
						let tasks = self.tasks.clone();
						spawn_local(async move {
							match send_mix_demand(keypair.clone(), nodes, pending_mix_store).await {
								Err(e) => {
									console::error_1(
										&format!("Error sending mix demand: {:?}", e).into(),
									);
								}
								Ok(pending_mix_store) => {
									console::log_1(&"Sent mix demand".into());
									link.send_message(Msg::Nope);
									spawn_local(async move {
										poll_mix_confirm(tasks, keypair, pending_mix_store).await;
										link.send_message(Msg::Nope);
									});
								}
							}
						});
					}
					Err(e) => console::error_1(&e),
				}
			}
			Msg::SendTx(input_tx_id_seed_0) => {
				if let Some(pending_mix) = storage::get_pending_mix(&input_tx_id_seed_0) {
					if let Some(keypair) = self.keypairs.get(&pending_mix.mix_demands[0].issuer) {
						let keypair = keypair.clone();
						let link = self.link.clone();
						let config = self.config.clone();
						spawn_local(async move {
							send_tx(config, vec![(keypair, vec![pending_mix])]).await;
							link.send_message(Msg::Nope);
						});
					}
				}
			}
			Msg::UnlockModal(_pubkey) => {
				self.modals.insert(
					self.modals_ai,
					Modal::Unlock(UnlockModal {
						keypair: None,
						scrypt: (String::new(), String::new()),
					}),
				);
				self.modals_ai += 1;
			}
			Msg::CloseModal(i) => {
				self.modals.remove(&i);
			}
			Msg::Unlock(i, _event) => {
				if let Some(Modal::Unlock(modal)) = self.modals.remove(&i) {
					if let Some(keypair) = modal.keypair {
						self.keypairs.insert(keypair.public_key(), keypair.clone());

						for pending_mix in storage::iter_pending_mixes() {
							if pending_mix.status == storage::MixStatus::WaitingForConfirm
								&& pending_mix.mix_demands[0].issuer == keypair.public_key()
								&& self
									.tasks
									.read()
									.unwrap()
									.poll_mix_confirm
									.contains(&pending_mix.mix_demands[0].input_tx_id_seed_0)
							{
								let keypair = keypair.clone();
								let link = self.link.clone();
								let tasks = self.tasks.clone();
								spawn_local(async move {
									poll_mix_confirm(tasks, keypair, pending_mix).await;
									link.send_message(Msg::Nope);
								});
							}
						}
					}
				}
			}
			Msg::UnlockModalScryptPswChanged(i, psw) => {
				if let Some(Modal::Unlock(modal)) = self.modals.get_mut(&i) {
					modal.scrypt.1 = psw;
					modal.keypair = Some(
						KeyPairFromSaltedPasswordGenerator::with_default_parameters().generate(
							SaltedPassword::new(modal.scrypt.0.clone(), modal.scrypt.1.clone()),
						),
					);
				}
			}
			Msg::UnlockModalScryptSaltChanged(i, salt) => {
				if let Some(Modal::Unlock(modal)) = self.modals.get_mut(&i) {
					modal.scrypt.0 = salt;
					modal.keypair = Some(
						KeyPairFromSaltedPasswordGenerator::with_default_parameters().generate(
							SaltedPassword::new(modal.scrypt.0.clone(), modal.scrypt.1.clone()),
						),
					);
				}
			}
			Msg::Nope => {}
		}

		if privkey_changed {
			self.mix_demand_form.keypair = Some(
				KeyPairFromSaltedPasswordGenerator::with_default_parameters().generate(
					SaltedPassword::new(
						self.mix_demand_form.scrypt.0.clone(),
						self.mix_demand_form.scrypt.1.clone(),
					),
				),
			);
			document
				.get_element_by_id("f-mix-pubkey")
				.unwrap()
				.set_attribute(
					"value",
					&self
						.mix_demand_form
						.keypair
						.as_ref()
						.map_or_else(|| "".into(), |keypair| keypair.public_key().to_base58()),
				)
				.ok();
		}

		true
	}

	fn changed(&mut self, _ctx: &Context<Self>) -> ShouldRender {
		false
	}

	fn view(&self, _ctx: &Context<Self>) -> Html {
		html! {<>
			<table border="1">
				<thead>
					<tr><th>{ "Issuer" }</th><th>{ "Recipient" }</th><th>{ "Amount" }</th><th>{ "Status" }</th><th>{ "Actions" }</th></tr>
				</thead>
				<tbody>{
					storage::iter_pending_mixes().map(|pending_mix| html! {
						<tr>
							<td>{ pending_mix.mix_demands[0].issuer.to_base58() }</td>
							<td>{ pending_mix.mix_demands[pending_mix.mix_demands.len()-1].recipient.to_base58() }</td>
							<td>{ pending_mix.mix_demands[0].amount.to_u64() as f64 / 100.0 }</td>
							<td>{ pending_mix.status.to_string() }</td>
							<td>{
								if self.keypairs.contains_key(&pending_mix.mix_demands[0].issuer) {
									if pending_mix.status == storage::MixStatus::Confirmed {
										let input_tx_id_seed_0 = pending_mix.mix_demands[0].input_tx_id_seed_0.clone();
										html! {
											<button onclick={self.link.callback(move |_| Msg::SendTx(input_tx_id_seed_0.clone()))}>{ "Send" }</button>
										}
									} else {
										html! {}
									}
								} else {
									let issuer = pending_mix.mix_demands[0].issuer;
									html! {
										<button onclick={self.link.callback(move |_| Msg::UnlockModal(issuer))}>{ "Unlock" }</button>
									}
								}
							}
								<button onclick={self.link.callback(move |_| Msg::RemoveMix(pending_mix.mix_demands[0].input_tx_id_seed_0.clone()))}>{ "Remove" }</button>
							</td>
						</tr>
					}).collect::<Html>()
				}</tbody>
			</table>
			<form onsubmit={self.link.callback(|v: FocusEvent| {
				v.prevent_default();
				console::log_1(&v.clone().into());
				Msg::SendMixDemand(v)
			})}>
				<fieldset>
					<legend>{ "Mix transaction" }</legend>

					<label for="f-mix-salt">{ "Salt:" }</label>
					<input type="password" name="salt" id="f-mix-salt" required=true onchange={self.link.callback(|v| if let ChangeData::Value(v) = v {
						Msg::ScryptSaltChanged(v)
					} else {
						Msg::Nope
					})} /><br/>
					<label for="f-mix-psw">{ "Password:" }</label>
					<input type="password" name="psw" id="f-mix-psw" required=true onchange={self.link.callback(|v| if let ChangeData::Value(v) = v {
						Msg::ScryptPswChanged(v)
					} else {
						Msg::Nope
					})} /><br/>

					<label for="f-mix-pubkey">{ "Pubkey:" }</label>
					<input type="text" name="pubkey" id="f-mix-pubkey" size="48" readonly=true/><br/>

					<label for="f-mix-amount">{ "Amount (Ğ1):" }</label>
					<input type="number" name="amount" id="f-mix-amount" value={format!("{}", self.mix_demand_form.amount.to_u64() as f64/100.0)} min="0.01" max="42949672.95" step="0.01" required=true onchange={self.link.callback(|v|
						if let ChangeData::Value(v) = v {
							if let Ok(v) = v.parse() {
								Msg::AmountChanged(v)
							} else { Msg::Nope }
						} else { Msg::Nope }
					)}/>
					<span class="validity"></span><br/>

					<p id="f-mix-path-label">{ "Path:" }</p>
					<ul id="f-mix-path" aria-labelledby="f-mix-path-label">
						{if let Some(path) = &self.mix_demand_form.path {
							{ path.path.iter().map(|node_pubkey| html!{<li><pre class="pubkey">{node_pubkey}</pre></li>}).collect::<Html>() }
						} else {html!{}}}
					</ul>
					<input type="button" value="Change path" onclick={self.link.callback(|_| Msg::ChangePath)} /><br/>

					<label for="f-mix-recipient">{ "Recipient's pubkey:" }</label>
					<input type="text" name="recipient" id="f-mix-recipient" maxlength="48" size="48" required=true/><br/>

					<details>
						<summary>{ "Advanced settings" }</summary>

						<label for="f-mix-layers">{ "Layers:" }</label>
						<input type="number" name="layers" id="f-mix-layers" value={format!("{}", self.mix_demand_form.layers)} min="1" max={format!("{}", std::usize::MAX-2)} step="1" required=true onchange={self.link.callback(|v|
							if let Some(v) = v {
								if let Ok(v) = v.parse() {
									Msg::LayersChanged(v)
								} else { Msg::Nope }
							} else { Msg::Nope }
						)}/>
						<span class="validity"></span>

					</details>

					<input type="submit"/>
				</fieldset>
			</form>

			{self.modals.iter().map(|(&modal_i, modal)| {
				match modal {
					Modal::Unlock(modal) => html! {
				<div id={format!("modal{}", modal_i)} class="modal">
					<form onsubmit={self.link.callback(move |v: FocusEvent| {
						v.prevent_default();
						console::log_1(&v.clone().into());
						Msg::Unlock(modal_i, v)
					})}>
						<fieldset>
							<legend>{ "Unlock" }</legend>

							<label for="f-unlock-salt">{ "Salt:" }</label>
							<input type="password" name="salt" id="f-unlock-salt" required=true onchange={self.link.callback(move |v| if let ChangeData::Value(v) = v {
								Msg::UnlockModalScryptSaltChanged(modal_i, v)
							} else {
								Msg::Nope
							})} /><br/>
							<label for="f-unlock-psw">{ "Password:" }</label>
							<input type="password" name="psw" id="f-unlock-psw" required=true onchange={self.link.callback(move |v| if let ChangeData::Value(v) = v {
								Msg::UnlockModalScryptPswChanged(modal_i, v)
							} else {
								Msg::Nope
							})} /><br/>

							<label for="f-unlock-pubkey">{ "Pubkey:" }</label>
							<input type="text" name="pubkey" id="f-unlock-pubkey" size="48" readonly=true value={modal.keypair.as_ref().map_or_else(String::new, |keypair| keypair.public_key().to_base58())}/><br/>

							<input type="button" value="Cancel" onclick={self.link.callback(move |_| Msg::CloseModal(modal_i))}/>
							<input type="submit"/>
						</fieldset>
					</form>
				</div>
			}}
			}).collect::<Html>()}
		</>}
	}
}

async fn send_mix_demand(
	keypair: Ed25519KeyPair,
	nodes: Arc<RwLock<nodes::Nodes>>,
	mut pending_mix_store: storage::PendingMixStore,
) -> Result<storage::PendingMixStore, ClientError> {
	let node_pubkey = pending_mix_store.path.path[0];
	let nodes = nodes.read().unwrap();
	let node_stores = nodes
		.nodes
		.get(&node_pubkey)
		.ok_or(ClientError::NoNodeError)?;

	let rclient = reqwest::Client::new();

	let mix_demand_onion_wrapper_secret =
		documents::mix_demand::MixDemandOnionWrapperSecret::from_mix_demand_onion_wrapper(
			&keypair,
			documents::mix_demand::MixDemandOnionWrapper {
				tx_id_seed_1: pending_mix_store.output_tx_id_seed_1.clone(),
				node_address: None,
				onion: pending_mix_store.mix_demand_onion.clone(),
			},
			&node_pubkey,
		);

	for node_store in ShuffleIter::new(
		&mut node_stores
			.iter()
			.map(|(_, node_store)| node_store)
			.collect(),
	) {
		if api::send_mix_demand_onion_wrapper_secret(
			&rclient,
			&mix_demand_onion_wrapper_secret,
			&node_store.node_sig.inner.address,
		)
		.await
		.is_ok()
		{
			pending_mix_store.node = Some(node_store.node_sig.inner.clone());
			break;
		}
	}
	if pending_mix_store.node.is_none() {
		return Err(ClientError::NoWorkingNodeError);
	}

	pending_mix_store.status = storage::MixStatus::WaitingForConfirm;

	storage::set_pending_mix(&pending_mix_store);

	Ok(pending_mix_store)
}

async fn poll_mix_confirm(
	tasks: Arc<RwLock<Tasks>>,
	keypair: Ed25519KeyPair,
	mut pending_mix: storage::PendingMixStore,
) {
	tasks
		.write()
		.unwrap()
		.poll_mix_confirm
		.insert(pending_mix.mix_demands[0].input_tx_id_seed_0.clone());

	let node = pending_mix.node.clone().unwrap();
	let rclient = reqwest::Client::new();
	for _ in 0..10 {
		if let Ok(mix_confirm_onion_wrapper_secret) =
			api::get_mix_confirm_blocking(&rclient, &node.address, &pending_mix.output_tx_id_seed_1)
				.await
		{
			if let Ok((mix_confirm_onion_wrapper, node_pubkey)) =
				mix_confirm_onion_wrapper_secret.decrypt(&keypair)
			{
				if node_pubkey != node.pubkey
					|| mix_confirm_onion_wrapper.tx_id_seed_1 != pending_mix.output_tx_id_seed_1
				{
					console::warn_1(&"Invalid mix confirm".into());
					break;
				}
				if let Ok(mix_confirm_sigs) = check_mix_confirms(
					pending_mix
						.ephemeral_keypair_seeds
						.iter()
						.map(|x| KeyPairFromSeed32Generator::generate(x.clone()))
						.collect(),
					mix_confirm_onion_wrapper.secret,
					&pending_mix.mix_demands,
					pending_mix.output_tx_id_seed_1.clone(),
					&pending_mix.path.path,
				) {
					if mix_confirm_onion_wrapper.tx_id_seed_2
						!= mix_confirm_sigs.get(0).unwrap().inner.input_tx_id_seeds[2]
					{
						console::warn_1(&"Invalid mix confirm".into());
						break;
					}

					pending_mix.mix_confirms = mix_confirm_sigs;
					pending_mix.status = storage::MixStatus::Confirmed;
					console::log_1(&"Valid mix confirm".into());

					storage::set_pending_mix(&pending_mix);
					break;
				} else {
					console::warn_1(&"Invalid mix confirm".into());
					break;
				}
			}
		}
	}

	tasks
		.write()
		.unwrap()
		.poll_mix_confirm
		.remove(&pending_mix.mix_demands[0].input_tx_id_seed_0);
}

fn check_mix_confirms(
	ephemeral_keypairs: Vec<Ed25519KeyPair>,
	mix_confirm_onion_secret: documents::mix_confirm::MixConfirmOnionSecret,
	mix_demands: &[documents::mix_demand::MixDemand],
	output_tx_id_seed_1: TxIdSeed,
	path: &[PublicKey],
) -> Result<Vec<documents::mix_confirm::MixConfirmSig>, Error> {
	let mut mix_confirm_sigs = Vec::<documents::mix_confirm::MixConfirmSig>::new();
	let mut mix_confirm_onion_secret_opt = Some(mix_confirm_onion_secret);

	let mut prev_ocs = None;

	for (ephemeral_keypair, expected_node_pubkey, mix_demand) in
		izip!(ephemeral_keypairs, path, mix_demands)
	{
		if let Some(Ok((mix_confirm_onion, node_pubkey))) = mix_confirm_onion_secret_opt
			.take()
			.map(|x| x.decrypt(&ephemeral_keypair))
		{
			if node_pubkey != *expected_node_pubkey
				|| mix_confirm_onion
					.mix_confirm_sig
					.verify_expected(expected_node_pubkey)
					.is_err() || mix_confirm_onion.mix_confirm_sig.inner.currency != mix_demand.currency
				|| mix_confirm_onion.mix_confirm_sig.inner.input_amount != mix_demand.amount
				|| mix_confirm_onion.mix_confirm_sig.inner.output_amount != mix_demand.amount
				|| mix_confirm_onion.mix_confirm_sig.inner.recipient != mix_demand.recipient
				|| mix_confirm_onion.mix_confirm_sig.inner.issuer != mix_demand.issuer
				|| mix_confirm_onion.mix_confirm_sig.inner.input_tx_id_seeds[0]
					!= mix_demand.input_tx_id_seed_0
				|| mix_confirm_onion.mix_confirm_sig.inner.output_tx_id_seeds[0]
					!= mix_demand.output_tx_id_seed_0
				|| mix_demand.output_tx_id_seed_2.clone().map_or(false, |x| {
					mix_confirm_onion.mix_confirm_sig.inner.output_tx_id_seeds[2] != x
				}) || (mix_confirm_sigs.is_empty()
				&& mix_confirm_onion.mix_confirm_sig.inner.input_tx_id_seeds[1]
					!= output_tx_id_seed_1)
				|| prev_ocs.map_or(false, |x| {
					mix_confirm_onion.mix_confirm_sig.inner.input_tx_id_seeds != x
				}) {
				println!("Error: Invalid pubkey");
				break;
			}

			if let Some(mix_confirm_onion_secret) = mix_confirm_onion.onion {
				mix_confirm_onion_secret_opt = Some(mix_confirm_onion_secret);
				prev_ocs = Some(
					mix_confirm_onion
						.mix_confirm_sig
						.inner
						.output_tx_id_seeds
						.clone(),
				);
				mix_confirm_sigs.push(mix_confirm_onion.mix_confirm_sig);
			} else {
				if mix_demand.onion.is_none() {
					mix_confirm_sigs.push(mix_confirm_onion.mix_confirm_sig);
					return Ok(mix_confirm_sigs);
				}
				return Err(Error::InvalidConfirms);
			}
		} else {
			return Err(Error::InvalidConfirms);
		}
	}
	Err(Error::InvalidConfirms)
}

async fn send_tx(
	config: Arc<config::ClientConfig>,
	mixes: Vec<(Ed25519KeyPair, Vec<storage::PendingMixStore>)>,
) {
	let rclient = reqwest::Client::new();

	for (keypair, mut mixes) in mixes {
		// Get sources
		let mut result = None;
		for gva_address in OrderedShuffleIter::new(config.network.gva_nodes.iter()) {
			match gva::get_sources(&rclient, gva_address, &keypair.public_key()).await {
				Ok(r) => {
					result = Some(r);
					break;
				}
				Err(e) => eprintln!("Error from {:?}: {:?}", gva_address, e),
			}
		}
		let result = result.expect("No working GVA node");
		let needed_amount = 0u64;
		let mut available_amount = 0u64;
		let mut sources: Vec<dubp::Source> = result
			.1
			.into_iter()
			.inspect(|x| available_amount += x.get_amount().to_u64())
			.collect();
		let blockstamp = result.0;

		let mut i_source = 0usize;
		let mut used_amount = 0u64;
		let mut mixes_to_send: Vec<(&mut storage::PendingMixStore, dubp::RawTx)> = vec![];

		// Generate transactions
		for mix in mixes.iter_mut() {
			let mix_confirm_0 = mix.mix_confirms.get(0).unwrap();
			let mut amount = mix_confirm_0.inner.input_amount.to_u64();

			println!(
				"--> {} {}",
				mix.mix_demands.last().unwrap().recipient.to_base58(),
				amount
			);
			mix.path
				.path
				.iter()
				.enumerate()
				.for_each(|(i, x)| println!("  {} {}", i + 1, x.to_base58()));

			if available_amount >= amount {
				used_amount += amount;
				// TODO better source selection algo
				let mut used_sources = vec![];
				let mut change = 0u64;
				for source in &sources[i_source..] {
					i_source += 1;
					used_sources.push(source);
					let source_amount = source.get_amount().to_u64();
					available_amount -= source_amount;
					if source_amount >= amount {
						change = source_amount - amount;
						break;
					}
					amount -= source_amount;
				}

				println!("  will be sent (change = {})", change);
				let mut wanted_outputs = vec![dubp::WantedOutput {
					amount: mix_confirm_0.inner.input_amount,
					recipient: mix_confirm_0.pubkey,
				}];
				if change != 0 {
					wanted_outputs.push(dubp::WantedOutput {
						amount: Amount {
							val: change as u32,
							base: 0,
						},
						recipient: keypair.public_key(),
					});
				}
				let tx_id = TxId::generate(&mix_confirm_0.inner.input_tx_id_seeds);
				let tx = dubp::generate_tx(
					&keypair.generate_signator(),
					&blockstamp,
					&tx_id.to_comment(),
					&config.protocol.currency,
					&wanted_outputs,
					&used_sources,
				);
				if change != 0 {
					// Remember output for chained txs
					sources.push(dubp::Source::Utxo(dubp::Utxo {
						amount: Amount {
							val: change as u32,
							base: 0,
						},
						index: 1,
						hash: hex::encode_upper(tx.hash()),
					}));
					available_amount += change;
				}
				mixes_to_send.push((mix, tx));
			}
		}

		// Ask user
		if mixes_to_send.is_empty() {
			println!(
				"Error: Not enough money to send any tx. ({} / {})",
				available_amount, needed_amount
			);
			return;
		} else if needed_amount > available_amount {
			println!(
				"Warning: Not enough money to send all the txs. ({} / {})",
				available_amount, needed_amount
			);
		}

		// todo prompt via modal

		// Send txs
		'txs: for (mix, tx) in mixes_to_send {
			let mut sent_times = 0u16;

			for gva_address in OrderedShuffleIter::new(config.network.gva_nodes.iter()) {
				println!("Send tx to {:?}", gva_address);
				if let Err(e) = gva::post_tx(&rclient, gva_address, tx.clone()).await {
					println!("Tx failed: {:?}", e);
				} else {
					println!("Sent tx");
					sent_times += 1;

					if sent_times == 1 {
						mix.status = storage::MixStatus::WaitingForTx;
						storage::set_pending_mix(mix);
					}

					if sent_times >= config.network.nodes_to_send_tx {
						continue 'txs;
					}
				}
			}

			if sent_times == 0 {
				println!("Tx failed with all GVA nodes!");
			} else {
				println!(
					"Tx sent to {}/{} GVA nodes",
					sent_times, config.network.nodes_to_send_tx
				);
			}
		}
	}
}

#[wasm_bindgen(start)]
pub fn run_app() {
	yew::start_app::<Model>();
}
