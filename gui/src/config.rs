use gmixer_common::prelude::*;
use std::str::FromStr;

pub struct NetworkConfig {
	/// Bootstrap GMixer node (temporary)
	pub bootstrap: NodeAddress,
	/// List of GVA nodes
	pub gva_nodes: Vec<Vec<GvaAddress>>,
	/// Node storage duration
	pub node_expire: u64,
	/// Number of GVA nodes to send each tx to (for redundancy)
	pub nodes_to_send_tx: u16,
}

impl Default for NetworkConfig {
	fn default() -> Self {
		NetworkConfig {
			bootstrap: NodeAddress::from_str("127.0.0.1").unwrap(),
			gva_nodes: vec![
				vec![GvaAddress(String::from("http://127.0.0.1:30901/gva"))],
				vec![
					GvaAddress(String::from("http://txmn.tk:30901/gva")),
					GvaAddress(String::from("https://g1.librelois.fr/gva")),
					GvaAddress(String::from("https://duniter-g1.p2p.legal/gva")),
				],
			],
			node_expire: 86400,
			nodes_to_send_tx: 3,
		}
	}
}

#[derive(Default)]
pub struct ClientConfig {
	pub network: NetworkConfig,
	pub only_member_nodes: bool,
	pub protocol: ProtocolConfig,
}
