use super::Dialog;

use dup_crypto::{
	bases::b58::ToBase58,
	keys::{
		ed25519::{
			Ed25519KeyPair, KeyPairFromSaltedPasswordGenerator, KeyPairFromSeed32Generator,
			PublicKey, SaltedPassword,
		},
		KeyPair, PublicKey as _,
	},
	seeds::Seed32,
};
use web_sys::console;
use yew::{prelude::*, html::Scope};

pub struct UnlockDialog {
	link: Scope<Self>,

	keypair: Option<Ed25519KeyPair>,
	scrypt: (String, String),
}

pub enum UnlockMsg {
	Close,
	Nope,
	Unlock(FocusEvent),
	ScryptPswChanged(String),
	ScryptSaltChanged(String),
}

impl Component for UnlockDialog {
	type Message = UnlockMsg;
	type Properties = ();

	fn create(ctx: &Context<Self>) -> Self {
		Self {
			link: ctx.link().clone(),
			keypair: None,
			scrypt: (String::new(), String::new()),
		}
	}

	fn update(&mut self, _ctx: &Context<Self>, _msg: Self::Message) -> ShouldRender {
		false
	}

	fn changed(&mut self, _ctx: &Context<Self>) -> ShouldRender {
		false
	}

	fn view(&self, _ctx: &Context<Self>) -> Html {
		html! {
			<div class="dialog dialog_unlock">
				<form onsubmit={self.link.callback(move |v: FocusEvent| {
					v.prevent_default();
					console::log_1(&v.clone().into());
					UnlockMsg::Unlock(v)
				})}>
					<fieldset>
						<legend>{ "Unlock" }</legend>

						<label for="f-unlock-salt">{ "Salt:" }</label>
						<input type="password" name="salt" id="f-unlock-salt" required=true onchange={self.link.callback(move |v| if let Some(v) = v {
							UnlockMsg::ScryptSaltChanged(v)
						} else {
							UnlockMsg::Nope
						})} /><br/>
						<label for="f-unlock-psw">{ "Password:" }</label>
						<input type="password" name="psw" id="f-unlock-psw" required=true onchange={self.link.callback(move |v| if let Some(v) = v {
							UnlockMsg::ScryptPswChanged(v)
						} else {
							UnlockMsg::Nope
						})} /><br/>

						<label for="f-unlock-pubkey">{ "Pubkey:" }</label>
						<input type="text" name="pubkey" id="f-unlock-pubkey" size="48" readonly=true value={self.keypair.as_ref().map_or_else(String::new, |keypair| keypair.public_key().to_base58())}/><br/>

						<input type="button" value="Cancel" onclick={self.link.callback(move |_| UnlockMsg::Close)}/>
						<input type="submit"/>
					</fieldset>
				</form>
			</div>
		}
	}
}
