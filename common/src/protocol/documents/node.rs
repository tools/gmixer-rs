use crate::{
	errors::*,
	network::*,
	protocol::{documents::pubkey_delegation::PubkeyDelegationSig, *},
};

use dup_crypto::keys::{
	ed25519::{PublicKey, Signator, Signature},
	PublicKey as _, Signator as _,
};
use serde::{Deserialize, Serialize};

const DOCUMENT: u8 = 1;

#[derive(Clone, Deserialize, PartialEq, Serialize)]
pub struct Node {
	pub address: NodeAddress,
	pub currency: String,
	pub id: NodeId,
	pub protocol_version: u16,
	pub pubkey: PublicKey,
	pub pubkey_delegation_sig: Option<PubkeyDelegationSig>,
	pub sigtime: u64,
	pub software_version: String,
}

#[derive(Clone, Deserialize, PartialEq, Serialize)]
pub struct NodeSig {
	protocol_mark: ProtocolMark,
	pub inner: Node,
	signature: Signature,
}

impl Node {
	pub fn sign(self, signator: &Signator) -> NodeSig {
		NodeSig {
			protocol_mark: ProtocolMark::new(DOCUMENT),
			signature: signator.sign(&bincode::serialize(&self).unwrap()),
			inner: self,
		}
	}

	pub fn verify_expire(&self, config: &ProtocolConfig) -> Result<(), DocumentError> {
		if let Some(pubkey_delegation_sig) = &self.pubkey_delegation_sig {
			pubkey_delegation_sig.verify_expire(config)?;
		}
		Ok(())
	}

	pub fn verify(&self, config: &ProtocolConfig) -> Result<(), DocumentError> {
		if self.currency != config.currency {
			return Err(DocumentError::DifferentCurrency);
		}
		self.verify_expire(config)
	}
}

impl NodeSig {
	pub fn verify(&self, config: &ProtocolConfig) -> Result<(), DocumentError> {
		self.inner.verify(config)?;
		if let Some(v) = &self.inner.pubkey_delegation_sig {
			v.verify(config)?;
		}
		self.inner
			.pubkey
			.verify(&bincode::serialize(&self.inner)?, &self.signature)?;
		Ok(())
	}
}
