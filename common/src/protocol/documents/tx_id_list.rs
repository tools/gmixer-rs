use crate::protocol::{TxId, TxIdListHash, TX_ID_LEN};

use serde::{Deserialize, Serialize};
use sha2::{Digest, Sha512_256};
use std::convert::TryInto;

#[derive(Deserialize, Serialize)]
pub struct TxIdList {
	pub tx_ids: Vec<TxId>,
}

impl TxIdList {
	pub fn hash(&self) -> TxIdListHash {
		let mut hasher = Sha512_256::new();
		hasher.update(unsafe {
			std::slice::from_raw_parts(
				self.tx_ids.as_ptr() as *const u8,
				self.tx_ids.len() * TX_ID_LEN,
			)
		});
		TxIdListHash(hasher.finalize().try_into().unwrap())
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_hash_tx_id_list() {
		assert_eq!(
			TxIdList {
				tx_ids: vec![
					TxId([
						36u8, 22, 39, 175, 133, 188, 98, 230, 153, 178, 20, 84, 39, 164, 184, 9,
						148, 147, 46, 164, 229, 180, 112, 37, 32, 198, 32, 189, 144, 224, 192, 59
					]),
					TxId([
						8u8, 197, 33, 207, 137, 158, 171, 143, 75, 32, 186, 237, 75, 79, 171, 116,
						115, 11, 152, 79, 34, 34, 157, 103, 166, 147, 185, 235, 31, 120, 156, 108
					]),
					TxId([
						108u8, 115, 110, 233, 54, 109, 152, 160, 147, 183, 214, 168, 65, 20, 31,
						173, 114, 117, 58, 88, 89, 68, 232, 227, 77, 2, 160, 215, 33, 225, 65, 165
					])
				]
			}
			.hash()
			.0,
			[
				23u8, 217, 112, 231, 131, 205, 49, 94, 121, 35, 159, 103, 135, 138, 226, 105, 232,
				43, 43, 56, 172, 212, 155, 94, 31, 97, 235, 240, 219, 23, 128, 107
			],
		);
	}
}
