use crate::{
	errors::*,
	protocol::{ProtocolConfig, ProtocolMark},
};

use dup_crypto::keys::{
	ed25519::{PublicKey, Signator, Signature},
	PublicKey as _, Signator as _,
};
use serde::{Deserialize, Serialize};
use std::time::{SystemTime, UNIX_EPOCH};

const DOCUMENT: u8 = 0;

/// Document that temporarily charges a WoT member's responsibility for a ĞMixer non-member public key
#[derive(Clone, Deserialize, PartialEq, Serialize)]
pub struct PubkeyDelegation {
	/// Signing member public key
	pub issuer: PublicKey,
	pub currency: String,
	/// ĞMixer node public key
	pub pubkey: PublicKey,
	/// Signature timestamp
	pub sigtime: u64,
}

#[derive(Clone, Deserialize, PartialEq, Serialize)]
pub struct PubkeyDelegationSig {
	protocol_mark: ProtocolMark,
	pub inner: PubkeyDelegation,
	signature: Signature,
}

impl PubkeyDelegationSig {
	pub fn create(
		signator: &Signator,
		pubkey: &PublicKey,
		currency: String,
	) -> PubkeyDelegationSig {
		let inner = PubkeyDelegation {
			issuer: signator.public_key(),
			currency,
			pubkey: *pubkey,
			sigtime: SystemTime::now()
				.duration_since(UNIX_EPOCH)
				.unwrap()
				.as_secs(),
		};
		let inner_raw = bincode::serialize(&inner).unwrap();
		PubkeyDelegationSig {
			protocol_mark: ProtocolMark::new(DOCUMENT),
			inner,
			signature: signator.sign(&inner_raw),
		}
	}

	pub fn verify_expire(&self, config: &ProtocolConfig) -> Result<(), DocumentError> {
		let now = SystemTime::now()
			.duration_since(UNIX_EPOCH)
			.unwrap()
			.as_secs();
		if self.inner.sigtime > now {
			return Err(DocumentError::ValidationFailed(
				ValidationError::SignedInFuture,
			));
		}
		match self
			.inner
			.sigtime
			.checked_add(config.pubkey_delegation_expire)
		{
			Some(val) => {
				if val < now {
					return Err(DocumentError::ValidationFailed(ValidationError::Expired));
				}
			}
			_ => {
				return Err(DocumentError::ValidationFailed(
					ValidationError::ExpireTimeOverflow,
				));
			}
		}
		Ok(())
	}

	pub fn verify(&self, config: &ProtocolConfig) -> Result<(), DocumentError> {
		self.verify_expire(config)?;
		self.inner
			.issuer
			.verify(&bincode::serialize(&self.inner)?, &self.signature)?;
		Ok(())
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use crate::test::*;
	use dup_crypto::keys::{ed25519::Ed25519KeyPair, KeyPair};

	#[test]
	fn test_correct_pubkey_delegation() -> Result<(), DocumentError> {
		let keypair = Ed25519KeyPair::generate_random().unwrap();
		let pubkey = random_pubkey();

		let pubkey_delegation_sig =
			PubkeyDelegationSig::create(&keypair.generate_signator(), &pubkey, String::from("foo"));

		pubkey_delegation_sig.verify(&ProtocolConfig::default())?;

		Ok(())
	}
}
