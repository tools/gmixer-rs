use crate::{dubp::Amount, errors::*, network::NodeAddress, protocol::TxIdSeed};

use dup_crypto::{
	keys::{
		ed25519::{Ed25519KeyPair, PublicKey},
		KeyPair,
	},
	private_message::{
		decrypt_private_message, encrypt_private_message, AuthenticationPolicy, ChaChaRounds,
		DecryptedMessage, METADATA_LEN,
	},
};
use serde::{Deserialize, Serialize};

const MIX_DEMAND_AAD: &[u8] = "gmixer\x00\x00".as_bytes();
const MIX_DEMAND_ONION_WRAPPER_AAD: &[u8] = "gmixer\x00\x01".as_bytes();

/// Encrypted mix demand (client -> node)
#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct MixDemandOnion(pub Vec<u8>);

/// Encrypted mix demand (node -> node)
#[derive(Deserialize, Serialize)]
pub struct MixDemandOnionWrapperSecret(Vec<u8>);

#[derive(Deserialize, Serialize)]
pub struct MixDemandOnionWrapper {
	pub tx_id_seed_1: TxIdSeed,
	/// Issuer node address (may be None if client)
	pub node_address: Option<NodeAddress>,
	pub onion: MixDemandOnion,
}

/// Document sent by client to a node
/// It contains data for mixing and onion data which will be passed to the next node.
#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct MixDemand {
	/// Transaction amount
	pub amount: Amount,
	/// Client ephemeral public key
	pub client_pubkey: PublicKey,
	pub currency: String,
	pub input_tx_id_seed_0: TxIdSeed,
	/// Transaction issuer (previous node)
	pub issuer: PublicKey,
	/// Onion of which the next node can decrypt the first layer
	pub onion: Option<MixDemandOnion>,
	pub output_tx_id_seed_0: TxIdSeed,
	pub output_tx_id_seed_2: Option<TxIdSeed>,
	/// Transaction recipient (next node)
	pub recipient: PublicKey,
}

impl MixDemand {
	pub fn from_mix_demand_onion(
		keypair: &Ed25519KeyPair,
		mix_demand_onion: MixDemandOnion,
	) -> Result<MixDemand, DocumentError> {
		let mut buffer = mix_demand_onion.0;
		if let DecryptedMessage {
			message: inner,
			sender_public_key: issuer,
			signature_opt: None,
		} = decrypt_private_message(MIX_DEMAND_AAD, ChaChaRounds::ChaCha20, &mut buffer, keypair)?
		{
			let mix_demand: MixDemand = bincode::deserialize(inner)?;

			if (mix_demand.onion == None) == (mix_demand.output_tx_id_seed_2 == None) {
				return Err(DocumentError::IncoherentPositionMarkers);
			}

			if issuer != mix_demand.client_pubkey {
				return Err(DocumentError::ValidationFailed(
					ValidationError::InvalidPublicKey,
				));
			}

			Ok(mix_demand)
		} else {
			Err(DocumentError::UnexpectedSignature)
		}
	}
}

impl MixDemandOnion {
	pub fn from_mix_demand(
		keypair: &Ed25519KeyPair,
		mix_demand: &MixDemand,
		node_pubkey: &PublicKey,
	) -> MixDemandOnion {
		let message = bincode::serialize(mix_demand).unwrap();
		let mut buffer: Vec<u8> = Vec::with_capacity(message.len() + METADATA_LEN);
		buffer.extend(&message[..]);

		encrypt_private_message(
			MIX_DEMAND_AAD,
			AuthenticationPolicy::PrivateAuthentication,
			ChaChaRounds::ChaCha20,
			&mut buffer,
			node_pubkey,
			keypair,
		)
		.unwrap();

		MixDemandOnion(buffer)
	}
}

impl MixDemandOnionWrapperSecret {
	pub fn from_mix_demand_onion_wrapper(
		keypair: &Ed25519KeyPair,
		mix_demand_onion_wrapper: MixDemandOnionWrapper,
		node_pubkey: &PublicKey,
	) -> MixDemandOnionWrapperSecret {
		let message = bincode::serialize(&mix_demand_onion_wrapper).unwrap();
		let mut buffer: Vec<u8> = Vec::with_capacity(message.len() + METADATA_LEN);
		buffer.extend(&message[..]);

		encrypt_private_message(
			MIX_DEMAND_ONION_WRAPPER_AAD,
			AuthenticationPolicy::PrivateAuthentication,
			ChaChaRounds::ChaCha20,
			&mut buffer,
			node_pubkey,
			keypair,
		)
		.unwrap();

		MixDemandOnionWrapperSecret(buffer)
	}
}

impl MixDemandOnionWrapper {
	pub fn from_mix_demand_onion_wrapper_secret(
		keypair: &Ed25519KeyPair,
		mix_demand_onion_wrapper_secret: MixDemandOnionWrapperSecret,
	) -> Result<(MixDemandOnionWrapper, PublicKey), DocumentError> {
		let mut buffer = mix_demand_onion_wrapper_secret.0;
		if let DecryptedMessage {
			message: inner,
			sender_public_key: sender_pubkey,
			signature_opt: None,
		} = decrypt_private_message(
			MIX_DEMAND_ONION_WRAPPER_AAD,
			ChaChaRounds::ChaCha20,
			&mut buffer,
			keypair,
		)? {
			Ok((bincode::deserialize(inner)?, sender_pubkey))
		} else {
			Err(DocumentError::MissingSignature)
		}
	}
}

pub fn gen_mix_demands(
	amount: &Amount,
	client_keypair: &Ed25519KeyPair,
	currency: &str,
	ephemeral_keypairs: &[Ed25519KeyPair],
	path: &[PublicKey],
	recipient: &PublicKey,
) -> (Vec<MixDemand>, MixDemandOnion) {
	let mut prec_node: Option<PublicKey> = None;
	let mut prec_input_tx_id_seed_0 = TxIdSeed::generate();
	let mut path_iter = path.iter().rev().peekable();
	let mut onion: Option<MixDemandOnion> = None;
	(
		ephemeral_keypairs
			.iter()
			.rev()
			.map(|ephemeral_keypair| {
				let &node_pubkey = path_iter.next().unwrap();
				let input_tx_id_seed_0 = TxIdSeed::generate();
				let mix_demand = MixDemand {
					amount: *amount,
					client_pubkey: ephemeral_keypair.public_key(),
					currency: currency.to_string(),
					input_tx_id_seed_0: input_tx_id_seed_0.clone(),
					issuer: {
						if let Some(&&next_node_pubkey) = path_iter.peek() {
							next_node_pubkey
						} else {
							client_keypair.public_key()
						}
					},
					onion: onion.take(),
					output_tx_id_seed_0: prec_input_tx_id_seed_0.clone(),
					output_tx_id_seed_2: {
						if prec_node.is_none() {
							Some(TxIdSeed::generate())
						} else {
							None
						}
					},
					recipient: {
						if let Some(prec_node_pubkey) = prec_node {
							prec_node_pubkey
						} else {
							*recipient
						}
					},
				};
				prec_input_tx_id_seed_0 = input_tx_id_seed_0;
				onion = Some(MixDemandOnion::from_mix_demand(
					ephemeral_keypair,
					&mix_demand,
					&node_pubkey,
				));
				prec_node = Some(node_pubkey);
				mix_demand
			})
			.collect::<Vec<MixDemand>>()
			.into_iter()
			.rev()
			.collect(),
		onion.unwrap(),
	)
}

#[cfg(test)]
mod tests {
	use super::*;
	use crate::{
		network::{Addr, NetworkProtocol},
		test::*,
	};
	use dup_crypto::keys::KeyPair;

	#[test]
	fn test_correct_mix_demand() -> Result<(), DocumentError> {
		let node1_keypair = Ed25519KeyPair::generate_random().unwrap();
		let node1_pubkey = node1_keypair.public_key();
		let node2_keypair = Ed25519KeyPair::generate_random().unwrap();
		let node2_pubkey = node2_keypair.public_key();
		let client_keypair = Ed25519KeyPair::generate_random().unwrap();
		let client_pubkey = client_keypair.public_key();

		let mix_demand = MixDemand {
			amount: Amount { val: 123, base: 0 },
			client_pubkey,
			currency: String::from("foo"),
			input_tx_id_seed_0: TxIdSeed::generate(),
			issuer: random_pubkey(),
			onion: None,
			output_tx_id_seed_0: TxIdSeed::generate(),
			output_tx_id_seed_2: Some(TxIdSeed::generate()),
			recipient: random_pubkey(),
		};
		let mix_demand_onion =
			MixDemandOnion::from_mix_demand(&client_keypair, &mix_demand, &node2_pubkey);

		let mix_demand_onion_wrapper = MixDemandOnionWrapper {
			tx_id_seed_1: TxIdSeed::generate(),
			node_address: Some(NodeAddress {
				addr: Addr::Hostname(String::from("localhost")),
				port: 10953,
				protocol: NetworkProtocol::Http,
			}),
			onion: mix_demand_onion,
		};

		let mix_demand_onion_wrapper_secret =
			MixDemandOnionWrapperSecret::from_mix_demand_onion_wrapper(
				&node1_keypair,
				mix_demand_onion_wrapper,
				&node2_pubkey,
			);

		let (mix_demand_onion_wrapper, sender_pubkey) =
			MixDemandOnionWrapper::from_mix_demand_onion_wrapper_secret(
				&node2_keypair,
				mix_demand_onion_wrapper_secret,
			)?;

		assert_eq!(node1_pubkey, sender_pubkey,);

		assert_eq!(
			mix_demand,
			MixDemand::from_mix_demand_onion(&node2_keypair, mix_demand_onion_wrapper.onion)?
		);

		Ok(())
	}

	#[test]
	fn test_gen_mix_demands() {
		let client_keypair = Ed25519KeyPair::generate_random().unwrap();
		let amount = Amount { val: 100, base: 0 };
		let path: Vec<PublicKey> = (0..3).map(|_| random_pubkey()).collect();
		let recipient = random_pubkey();
		let ephemeral_keypairs: Vec<Ed25519KeyPair> = (0..3)
			.map(|_| Ed25519KeyPair::generate_random().unwrap())
			.collect();
		let currency = String::from("g1");

		let (mix_demands, _) = gen_mix_demands(
			&amount,
			&client_keypair,
			&currency,
			&ephemeral_keypairs,
			&path,
			&recipient,
		);

		let mix_demand0: &MixDemand = mix_demands.get(0).unwrap();
		let mix_demand1: &MixDemand = mix_demands.get(1).unwrap();
		let mix_demand2: &MixDemand = mix_demands.get(2).unwrap();

		assert_eq!(mix_demand0.issuer, client_keypair.public_key());
		assert_eq!(&mix_demand1.issuer, path.get(0).unwrap());
		assert_eq!(&mix_demand2.issuer, path.get(1).unwrap());

		assert_eq!(&mix_demand0.recipient, path.get(1).unwrap());
		assert_eq!(&mix_demand1.recipient, path.get(2).unwrap());
		assert_eq!(mix_demand2.recipient, recipient);

		assert_eq!(
			mix_demand0.client_pubkey,
			ephemeral_keypairs.get(0).unwrap().public_key()
		);
		assert_eq!(
			mix_demand1.client_pubkey,
			ephemeral_keypairs.get(1).unwrap().public_key()
		);
		assert_eq!(
			mix_demand2.client_pubkey,
			ephemeral_keypairs.get(2).unwrap().public_key()
		);

		assert!(mix_demand0.output_tx_id_seed_2.is_none());
		assert!(mix_demand1.output_tx_id_seed_2.is_none());
		assert!(mix_demand2.output_tx_id_seed_2.is_some());

		assert_eq!(
			mix_demand0.output_tx_id_seed_0,
			mix_demand1.input_tx_id_seed_0
		);
		assert_eq!(
			mix_demand1.output_tx_id_seed_0,
			mix_demand2.input_tx_id_seed_0
		);
	}
}
