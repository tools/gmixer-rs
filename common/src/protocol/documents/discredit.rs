use crate::{
	common::{Blockstamp, Error},
	documents::mix_confirm::MixConfirmSig,
};

use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
pub struct Discredit {
	blockstamp: Blockstamp,
	mix_confirm_sig: MixConfirmSig,
}

impl Discredit {
	pub fn verify(&self) -> Result<(), Error> {
		self.mix_confirm_sig.verify()?;

		// TODO verify contract violation

		Ok(())
	}
}
