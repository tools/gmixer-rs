use dup_crypto::keys::{
	ed25519::{Ed25519KeyPair, PublicKey},
	KeyPair,
};

pub fn random_pubkey() -> PublicKey {
	Ed25519KeyPair::generate_random().unwrap().public_key()
}
