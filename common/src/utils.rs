use rand::{rngs::ThreadRng, Rng};

/// FY Shuffling iterator
pub struct ShuffleIter<'a, 'b, T> {
	items: &'a mut Vec<&'b T>,
	i: usize,
	rng: ThreadRng,
}

impl<'a, 'b, T> ShuffleIter<'a, 'b, T> {
	pub fn new(items: &'a mut Vec<&'b T>) -> Self {
		Self {
			i: items.len(),
			items,
			rng: rand::thread_rng(),
		}
	}

	pub fn reset(&mut self) {
		self.i = self.items.len();
	}
}

impl<'a: 'b, 'b, T> Iterator for ShuffleIter<'a, 'b, T> {
	type Item = &'b T;

	fn next(&mut self) -> Option<Self::Item> {
		if self.i == 0 {
			return None;
		}
		self.items.swap(self.rng.gen_range(0..self.i), self.i - 1);
		self.i -= 1;
		Some(unsafe { self.items.get_unchecked(self.i) })
	}
}

/// Ordered FY Shuffling iterator
/// Shuffles items within subsets, keeping subsets ordered
pub struct OrderedShuffleIter<'a, T, I: Iterator<Item = &'a Vec<T>>> {
	iterator: I,
	current: Option<Vec<&'a T>>,
	i: usize,
	rng: ThreadRng,
}

impl<'a, T, I: Iterator<Item = &'a Vec<T>>> OrderedShuffleIter<'a, T, I> {
	pub fn new(iterator: I) -> Self {
		Self {
			iterator,
			current: None,
			i: 0,
			rng: rand::thread_rng(),
		}
	}
}

impl<'a, T, I: Iterator<Item = &'a Vec<T>>> Iterator for OrderedShuffleIter<'a, T, I> {
	type Item = &'a T;

	fn next(&mut self) -> Option<Self::Item> {
		if self.i == 0 {
			loop {
				let v = self.iterator.next()?;
				if v.is_empty() {
					continue;
				}
				self.i = v.len();
				self.current = Some(v.iter().collect());
				break;
			}
		}
		let current = self.current.as_mut().unwrap();
		current.swap(self.rng.gen_range(0..self.i), self.i - 1);
		self.i -= 1;
		Some(unsafe { current.get_unchecked(self.i) })
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_shuffle_iter() {
		let mut v: Vec<&i32> = vec![];
		assert!(ShuffleIter::new(&mut v).next().is_none());

		let mut v = vec![&1];
		let mut iter = ShuffleIter::new(&mut v);
		assert_eq!(iter.next(), Some(&1));
		assert!(iter.next().is_none());

		let mut v = vec![&0, &1, &2, &3, &4, &5];
		let mut w = vec![];
		ShuffleIter::new(&mut v).for_each(|x| {
			assert!(!w.contains(x));
			w.push(*x);
		});
		(0..6).for_each(|x| assert!(w.contains(&x)));
	}

	#[test]
	fn test_ordered_shuffle_iter() {
		let v: Vec<Vec<i32>> = vec![];
		assert!(OrderedShuffleIter::new(v.iter()).next().is_none());

		let v: Vec<Vec<i32>> = vec![vec![], vec![], vec![]];
		assert!(OrderedShuffleIter::new(v.iter()).next().is_none());

		let v = vec![vec![1]];
		let mut iter = OrderedShuffleIter::new(v.iter());
		assert_eq!(iter.next(), Some(&1));
		assert!(iter.next().is_none());

		let v = vec![vec![0, 1, 2], vec![], vec![3], vec![4, 5, 6]];
		let mut iter = OrderedShuffleIter::new(v.iter());
		assert!(vec![
			vec![0, 1, 2],
			vec![0, 2, 1],
			vec![1, 0, 2],
			vec![1, 2, 0],
			vec![2, 0, 1],
			vec![2, 1, 0]
		]
		.contains(&vec![
			*iter.next().unwrap(),
			*iter.next().unwrap(),
			*iter.next().unwrap()
		]));
		assert_eq!(iter.next(), Some(&3));
		assert!(vec![
			vec![4, 5, 6],
			vec![4, 6, 5],
			vec![5, 4, 6],
			vec![5, 6, 4],
			vec![6, 4, 5],
			vec![6, 5, 4]
		]
		.contains(&vec![
			*iter.next().unwrap(),
			*iter.next().unwrap(),
			*iter.next().unwrap()
		]));
		assert!(iter.next().is_none());
	}
}
