use crate::{errors::*, network::NodeAddress, protocol::*};

pub const MIME: &str = "application/x-gmixer";

pub async fn send_mix_demand_onion_wrapper_secret(
	rclient: &reqwest::Client,
	mix_demand_onion_wrapper_secret: &documents::mix_demand::MixDemandOnionWrapperSecret,
	node_address: &NodeAddress,
) -> Result<(), Error> {
	Ok(bincode::deserialize::<Result<(), ResponseError>>(
		&rclient
			.post(node_address.to_url("/mix-demand").as_str())
			.body(bincode::serialize(mix_demand_onion_wrapper_secret)?)
			.header("Content-Type", MIME)
			.send()
			.await?
			.bytes()
			.await?,
	)??)
}

pub async fn get_node(
	rclient: &reqwest::Client,
	node_address: &NodeAddress,
) -> Result<documents::node::NodeSig, Error> {
	Ok(bincode::deserialize::<
		Result<documents::node::NodeSig, ResponseError>,
	>(
		&rclient
			.get(node_address.to_url("/node").as_str())
			.header("Accept", MIME)
			.send()
			.await?
			.bytes()
			.await?,
	)??)
}

pub async fn get_pubkey_delegation(
	node_address: &NodeAddress,
) -> Result<Option<documents::pubkey_delegation::PubkeyDelegationSig>, Error> {
	Ok(bincode::deserialize::<
		Result<Option<documents::pubkey_delegation::PubkeyDelegationSig>, ResponseError>,
	>(
		&reqwest::Client::new()
			.get(node_address.to_url("/pubkey-delegation").as_str())
			.header("Accept", MIME)
			.send()
			.await?
			.bytes()
			.await?,
	)??)
}

pub async fn get_nodes(
	rclient: &reqwest::Client,
	node_address: &NodeAddress,
) -> Result<Vec<documents::node::NodeSig>, Error> {
	Ok(bincode::deserialize::<Vec<documents::node::NodeSig>>(
		&rclient
			.get(node_address.to_url("/nodes").as_str())
			.header("Accept", MIME)
			.send()
			.await?
			.bytes()
			.await?,
	)?)
}

pub async fn get_mix_confirm(
	rclient: &reqwest::Client,
	node_address: &NodeAddress,
	tx_id_seed: &TxIdSeed,
) -> Result<documents::mix_confirm::MixConfirmOnionWrapperSecret, Error> {
	Ok(bincode::deserialize::<
		Result<documents::mix_confirm::MixConfirmOnionWrapperSecret, ResponseError>,
	>(
		&rclient
			.post(node_address.to_url("/get-mix-confirm").as_str())
			.body(bincode::serialize(tx_id_seed)?)
			.header("Accept", MIME)
			.send()
			.await?
			.bytes()
			.await?,
	)??)
}

pub async fn get_mix_confirm_blocking(
	rclient: &reqwest::Client,
	node_address: &NodeAddress,
	tx_id_seed: &TxIdSeed,
) -> Result<documents::mix_confirm::MixConfirmOnionWrapperSecret, Error> {
	Ok(bincode::deserialize::<
		Result<documents::mix_confirm::MixConfirmOnionWrapperSecret, ResponseError>,
	>(
		&rclient
			.post(node_address.to_url("/get-mix-confirm-blocking").as_str())
			.body(bincode::serialize(tx_id_seed)?)
			.header("Accept", MIME)
			.send()
			.await?
			.bytes()
			.await?,
	)??)
}

pub async fn get_tx_id_list(
	rclient: &reqwest::Client,
	node_address: &NodeAddress,
	tx_id_list_hash: &TxIdListHash,
) -> Result<documents::tx_id_list::TxIdList, Error> {
	Ok(bincode::deserialize::<
		Result<documents::tx_id_list::TxIdList, ResponseError>,
	>(
		&rclient
			.post(node_address.to_url("/get-tx-id-list").as_str())
			.body(bincode::serialize(tx_id_list_hash)?)
			.header("Accept", MIME)
			.send()
			.await?
			.bytes()
			.await?,
	)??)
}
