use crate::{
	dubp,
	dubp::*,
	errors::{Error, *},
	network::GvaAddress,
};

use dup_crypto::{
	bases::b58::ToBase58,
	keys::{ed25519::PublicKey, PublicKey as _},
};
use graphql_client::*;
use log::warn;
use sha2::{Digest, Sha256};
use std::str::FromStr;

#[derive(GraphQLQuery)]
#[graphql(
	schema_path = "src/network/graphql/schema.graphql",
	query_path = "src/network/graphql/query_sources.graphql",
	response_derives = "Debug"
)]
struct Sources;

#[derive(GraphQLQuery)]
#[graphql(
	schema_path = "src/network/graphql/schema.graphql",
	query_path = "src/network/graphql/query_tx_history.graphql",
	response_derives = "Debug"
)]
struct TxHistory;

#[derive(GraphQLQuery)]
#[graphql(
	schema_path = "src/network/graphql/schema.graphql",
	query_path = "src/network/graphql/query_txs_received.graphql",
	response_derives = "Debug"
)]
struct TxsReceived;

#[derive(GraphQLQuery)]
#[graphql(
	schema_path = "src/network/graphql/schema.graphql",
	query_path = "src/network/graphql/mutation_tx.graphql",
	response_derives = "Debug"
)]
struct Tx;

#[derive(GraphQLQuery)]
#[graphql(
	schema_path = "src/network/graphql/schema.graphql",
	query_path = "src/network/graphql/query_is_member.graphql",
	response_derives = "Debug"
)]
struct IsMember;

pub async fn is_member(
	rclient: &reqwest::Client,
	gva_address: &GvaAddress,
	pubkey: &PublicKey,
) -> Result<bool, Error> {
	let pubkey_b58 = pubkey.to_base58();
	let response_body: Response<is_member::ResponseData> = rclient
		.post(&gva_address.0)
		.json(&IsMember::build_query(is_member::Variables {
			pubkey: pubkey_b58,
		}))
		.send()
		.await?
		.json()
		.await
		.map_err(|_| Error::NetworkError(NetworkError::DeserializationFailed("Bad JSON".into())))?;

	if let Some(errors) = response_body.errors {
		for e in errors {
			warn!("GVA error: {}", e);
		}
	}

	let data = response_body.data.ok_or_else(|| {
		Error::NetworkError(NetworkError::DeserializationFailed("No data".into()))
	})?;
	Ok(data.idty.map_or(false, |idty| idty.is_member))
}

pub async fn get_sources(
	rclient: &reqwest::Client,
	gva_address: &GvaAddress,
	pubkey: &PublicKey,
) -> Result<(String, Vec<dubp::Source>), Error> {
	let pubkey_b58 = pubkey.to_base58();
	let response_body: Response<sources::ResponseData> = rclient
		.post(&gva_address.0)
		.json(&Sources::build_query(sources::Variables {
			script: format!("SIG({})", pubkey_b58),
			pubkey: pubkey_b58,
		}))
		.send()
		.await?
		.json()
		.await
		.map_err(|_| Error::NetworkError(NetworkError::DeserializationFailed("Bad JSON".into())))?;

	if let Some(errors) = response_body.errors {
		for e in errors {
			warn!("GVA error: {}", e);
		}
	}

	// TODO check blockstamp
	let data = response_body.data.ok_or_else(|| {
		Error::NetworkError(NetworkError::DeserializationFailed("No data".into()))
	})?;

	let block = data
		.blocks
		.edges
		.ok_or_else(|| {
			Error::NetworkError(NetworkError::DeserializationFailed("No block edges".into()))
		})?
		.into_iter()
		.last()
		.flatten()
		.ok_or_else(|| {
			Error::NetworkError(NetworkError::DeserializationFailed("No blocks".into()))
		})?
		.node;
	let blockstamp = format!("{}-{}", block.number, block.hash);
	/*let blockstamp = data
		.node
		.peer
		.ok_or_else(|| Error::NetworkError(NetworkError::DeserializationFailed("No peer".into())))?
		.blockstamp;
	if !regex::Regex::new(r"^\d+-[0-9A-F]{64}$")
		.unwrap()
		.is_match(&blockstamp)
	{
		return Err(Error::NetworkError(NetworkError::DeserializationFailed(
			"Bad blockstamp".into(),
		)));
	}*/

	Ok((
		blockstamp, /* String::from("403994-0000000C08AEFD743BE0D5D1A40033B06F3DE2E1EF81E7D47FF92564A266DC83"), */
		data.utxos_of_script
			.edges
			.ok_or_else(|| {
				Error::NetworkError(NetworkError::DeserializationFailed("No UTXO edges".into()))
			})?
			.into_iter()
			.filter_map(|x| {
				let x = x?.node;
				Some(dubp::Source::Utxo(dubp::Utxo {
					amount: Amount {
						val: x.amount as u32,
						base: x.base as u32,
					},
					index: x.output_index as u32,
					hash: x.tx_hash,
				}))
			})
			.chain(
				data.uds_of_pubkey
					.edges
					.ok_or_else(|| {
						Error::NetworkError(NetworkError::DeserializationFailed(
							"No UD edges".into(),
						))
					})?
					.into_iter()
					.filter_map(|x| {
						let x = x?.node;
						Some(dubp::Source::Ud(dubp::Ud {
							amount: Amount {
								val: x.amount as u32,
								base: x.base as u32,
							},
							block: x.block_number as u32,
							pubkey: *pubkey,
						}))
					}),
			)
			.collect(),
	))
}

pub async fn get_tx_history(
	rclient: &reqwest::Client,
	gva_address: &GvaAddress,
	pubkey: &PublicKey,
) -> Result<(Vec<dubp::Tx>, Vec<dubp::Tx>), Error> {
	let response_body: Response<tx_history::ResponseData> = rclient
		.post(&gva_address.0)
		.json(&TxHistory::build_query(tx_history::Variables {
			pubkey: pubkey.to_base58(),
		}))
		.send()
		.await?
		.json()
		.await
		.map_err(|e| {
			println!("{:?}", e);
			Error::NetworkError(NetworkError::DeserializationFailed("bad JSON".into()))
		})?;

	if let Some(errors) = response_body.errors {
		for e in errors {
			warn!("GVA error: {}", e);
		}
	}

	let data = response_body.data.ok_or_else(|| {
		Error::NetworkError(NetworkError::DeserializationFailed("no data".into()))
	})?;
	Ok((
		data.txs_history_bc
			.received
			.edges
			.ok_or_else(|| {
				Error::NetworkError(NetworkError::DeserializationFailed(
					"no received txs".into(),
				))
			})?
			.into_iter()
			.filter_map(|x| {
				let x = x?.node;
				Some(dubp::Tx {
					comment: x.comment,
					currency: x.currency,
					hash: x.hash,
					inputs: x
						.inputs
						.iter()
						.filter_map(|input| dubp::parse_tx_input(input).ok())
						.collect(),
					issuers: {
						// Can't use filter_map here because need to return parent closure
						let mut issuers = vec![];
						for issuer in x.issuers {
							issuers.push(PublicKey::from_base58(&issuer).ok()?);
						}
						issuers
					},
					outputs: x
						.outputs
						.iter()
						.map(|output| dubp::Output::from_str(output).ok())
						.collect(),
					signatures: x.signatures,
					unlocks: x.unlocks,
					written_time: x.written_time? as u64,
				})
			})
			.collect(),
		data.txs_history_bc
			.sent
			.edges
			.ok_or_else(|| {
				Error::NetworkError(NetworkError::DeserializationFailed("no sent txs".into()))
			})?
			.into_iter()
			.filter_map(|x| {
				let x = x?.node;
				Some(dubp::Tx {
					comment: x.comment,
					currency: x.currency,
					hash: x.hash,
					inputs: x
						.inputs
						.iter()
						.filter_map(|input| dubp::parse_tx_input(input).ok())
						.collect(),
					issuers: {
						// Can't use filter_map here because need to return parent closure
						let mut issuers = vec![];
						for issuer in x.issuers {
							issuers.push(PublicKey::from_base58(&issuer).ok()?);
						}
						issuers
					},
					outputs: x
						.outputs
						.iter()
						.map(|output| dubp::Output::from_str(output).ok())
						.collect(),
					signatures: x.signatures,
					unlocks: x.unlocks,
					written_time: x.written_time? as u64,
				})
			})
			.collect(),
	))
}

pub async fn get_txs_received(
	rclient: &reqwest::Client,
	gva_address: &GvaAddress,
	pubkey: &PublicKey,
) -> Result<(String, Vec<dubp::Tx>), Error> {
	let response_body: Response<txs_received::ResponseData> = rclient
		.post(&gva_address.0)
		.json(&TxsReceived::build_query(txs_received::Variables {
			pubkey: pubkey.to_base58(),
		}))
		.send()
		.await?
		.json()
		.await
		.map_err(|_| Error::NetworkError(NetworkError::DeserializationFailed("".into())))?;

	if let Some(errors) = response_body.errors {
		for e in errors {
			warn!("GVA error: {}", e);
		}
	}

	let data = response_body
		.data
		.ok_or_else(|| Error::NetworkError(NetworkError::DeserializationFailed("".into())))?;

	Ok((
		/*data.node
		.peer
		.ok_or(Error::NetworkError(NetworkError::DeserializationFailed(
			"".into(),
		)))?
		.blockstamp,*/
		//String::from("400644-000000050A6B23B839BB73A614E7786B60D7DBF812108CEAE1B8E961641702FC"),
		format!("{}-{}", data.current_block.number, data.current_block.hash),
		data.txs_history_bc
			.received
			.edges
			.ok_or_else(|| Error::NetworkError(NetworkError::DeserializationFailed("".into())))?
			.into_iter()
			.filter_map(|x| {
				let x = x?.node;
				Some(dubp::Tx {
					comment: x.comment,
					currency: x.currency,
					hash: x.hash,
					inputs: x
						.inputs
						.iter()
						.filter_map(|input| dubp::parse_tx_input(input).ok())
						.collect(),
					issuers: {
						// Can't use filter_map here because need to return parent closure
						let mut issuers = vec![];
						for issuer in x.issuers {
							issuers.push(PublicKey::from_base58(&issuer).ok()?);
						}
						issuers
					},
					outputs: x
						.outputs
						.iter()
						.map(|output| dubp::Output::from_str(output).ok())
						.collect(),
					signatures: x.signatures,
					unlocks: x.unlocks,
					written_time: x.written_time? as u64,
				})
			})
			.collect(),
	))
}

pub async fn post_tx(
	rclient: &reqwest::Client,
	gva_address: &GvaAddress,
	raw_tx: dubp::RawTx,
) -> Result<(), Error> {
	println!("{:?}", raw_tx);

	let response_body: Response<tx::ResponseData> = rclient
		.post(&gva_address.0)
		.json(&Tx::build_query(tx::Variables {
			raw_tx: raw_tx.0.clone(),
		}))
		.send()
		.await?
		.json()
		.await
		.map_err(|_| Error::NetworkError(NetworkError::DeserializationFailed("".into())))?;

	if let Some(errors) = response_body.errors {
		for e in errors {
			println!("GVA error: {}", e);
		}
	}

	if let Some(data) = response_body.data {
		let mut hasher = Sha256::new();
		hasher.update(raw_tx.0);
		println!("Hash: {}", hex::encode(hasher.finalize()));
		println!("Data: {:?}", data);
	} else {
		println!("NONE");
	}

	Ok(())
}
