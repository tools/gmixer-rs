use crate::errors::*;

use dup_crypto::{
	bases::b58::ToBase58,
	keys::{
		ed25519::{PublicKey, Signator},
		PublicKey as _, Signator as _, Signature,
	},
};
use serde::{Deserialize, Serialize};
use sha2::{Digest, Sha256};
use std::convert::TryInto;

#[derive(Clone, Copy, Debug, Deserialize, Eq, Hash, PartialEq, Serialize)]
pub struct Amount {
	pub val: u32,
	pub base: u32,
}

impl Amount {
	pub fn to_u64(self) -> u64 {
		(self.val as u64) * (10u64).pow(self.base)
	}
}

#[derive(Debug, Deserialize, Hash, PartialEq, Serialize)]
pub struct Blockstamp {
	pub hash: [u8; 32],
	pub number: u32,
}

impl Blockstamp {
	pub fn from_string(str: String) -> Result<Self, std::io::Error> {
		let mut split = str.splitn(2, '-');

		Self::from_string_values(
			split.next().ok_or_else(|| {
				std::io::Error::new(std::io::ErrorKind::InvalidData, "Missing number")
			})?,
			split.next().ok_or_else(|| {
				std::io::Error::new(std::io::ErrorKind::InvalidData, "Missing hash")
			})?,
		)
	}

	pub fn from_string_values(number: &str, hash: &str) -> Result<Self, std::io::Error> {
		Ok(Blockstamp {
			number: number.parse().map_err(|_| {
				std::io::Error::new(std::io::ErrorKind::InvalidData, "Invalid number")
			})?,
			hash: {
				let mut buf = [0; 32];
				hex::decode_to_slice(hash, &mut buf).map_err(|_| {
					std::io::Error::new(std::io::ErrorKind::InvalidData, "Invalid hash")
				})?;
				buf
			},
		})
	}
}

#[derive(Debug)]
pub struct Utxo {
	pub amount: Amount,
	pub index: u32,
	pub hash: String,
}

#[derive(Debug)]
pub struct Ud {
	pub amount: Amount,
	pub block: u32,
	pub pubkey: PublicKey,
}

#[derive(Debug)]
pub enum Source {
	Utxo(Utxo),
	Ud(Ud),
}

impl Source {
	pub fn get_amount(&self) -> &Amount {
		match self {
			Source::Ud(ud) => &ud.amount,
			Source::Utxo(utxo) => &utxo.amount,
		}
	}

	pub fn to_string(&self) -> String {
		match self {
			Source::Ud(Ud {
				amount,
				block,
				pubkey,
			}) => format!(
				"{}:{}:D:{}:{}",
				amount.val,
				amount.base,
				pubkey.to_base58(),
				block
			),
			Source::Utxo(Utxo {
				amount,
				index,
				hash,
			}) => format!("{}:{}:T:{}:{}", amount.val, amount.base, hash, index),
		}
	}

	pub fn from_output(output: &Output, hash: String, index: u32) -> Self {
		Self::Utxo(Utxo {
			amount: output.amount,
			index,
			hash,
		})
	}
}

#[derive(Debug)]
pub struct Output {
	pub amount: Amount,
	pub condition: String,
}

impl std::str::FromStr for Output {
	type Err = Error;

	fn from_str(raw: &str) -> Result<Self, Self::Err> {
		let terms: Vec<&str> = raw.split(':').collect();
		if terms.len() != 3 {
			return Err(Error::DeserializationError);
		}
		Ok(Output {
			amount: Amount {
				val: terms[0].parse().map_err(|_| Error::DeserializationError)?,
				base: terms[1].parse().map_err(|_| Error::DeserializationError)?,
			},
			condition: terms[2].into(),
		})
	}
}

#[derive(Debug)]
pub struct Tx {
	pub comment: String,
	pub currency: String,
	pub hash: String,
	pub issuers: Vec<PublicKey>,
	pub inputs: Vec<Source>,
	pub outputs: Vec<Option<Output>>,
	pub signatures: Vec<String>,
	pub unlocks: Vec<String>,
	pub written_time: u64,
}

pub fn parse_tx_input(input: &str) -> Result<Source, Error> {
	let terms: Vec<&str> = input.split(':').collect();
	if terms.len() != 5 {
		return Err(Error::DeserializationError);
	}
	let amount = Amount {
		val: terms[0].parse().map_err(|_| Error::DeserializationError)?,
		base: terms[1].parse().map_err(|_| Error::DeserializationError)?,
	};
	match terms[2] {
		"D" => Ok(Source::Ud(Ud {
			amount,
			block: terms[4].parse().map_err(|_| Error::DeserializationError)?,
			pubkey: PublicKey::from_base58(terms[3]).map_err(|_| Error::DeserializationError)?,
		})),
		"T" => Ok(Source::Utxo(Utxo {
			amount,
			index: terms[4].parse().map_err(|_| Error::DeserializationError)?,
			hash: terms[3].into(),
		})),
		_ => Err(Error::DeserializationError),
	}
}

pub struct WantedOutput {
	pub amount: Amount,
	pub recipient: PublicKey,
}

#[derive(Clone, Debug)]
pub struct RawTx(pub String);

impl RawTx {
	pub fn hash(&self) -> [u8; 32] {
		let mut hasher = Sha256::new();
		hasher.update(format!("{}\n", self.0));
		hasher.finalize().try_into().unwrap()
	}
}

pub fn generate_tx(
	signator: &Signator,
	blockstamp: &str,
	comment: &str,
	currency: &str,
	outputs: &[WantedOutput],
	sources: &[&Source],
) -> RawTx {
	// Generate document
	let tx_doc = format!(
		"Version: 10\nType: Transaction\nCurrency: {}\nBlockstamp: {}\nLocktime: \
		 0\nIssuers:\n{}\nInputs:\n{}\nUnlocks:\n{}\nOutputs:\n{}\nComment: {}\n",
		currency,
		blockstamp,
		signator.public_key().to_base58(),
		sources
			.iter()
			.map(|&x| x.to_string())
			.collect::<Vec<String>>()
			.join("\n"),
		(0..sources.len())
			.map(|i| format!("{}:SIG(0)", i))
			.collect::<Vec<String>>()
			.join("\n"),
		outputs
			.iter()
			.map(|x| format!("{}:0:SIG({})", x.amount.val, x.recipient.to_base58()))
			.collect::<Vec<String>>()
			.join("\n"),
		comment,
	);

	// Sign document
	// append detached signature
	let signature = signator.sign(tx_doc.as_bytes()).to_base64();

	RawTx(tx_doc + &signature)
}

#[cfg(test)]
pub mod tests {
	use super::*;

	#[test]
	fn test_raw_tx_hash() {
		assert_eq!(
			hex::encode_upper(
				RawTx(
					"Version: 10\nType: Transaction\nCurrency: g1\nBlockstamp: \
					 408274-0000002961A472C00C76F63B7EE16C49D22F7A5B5310AC2208A76A701F67DD02\\
					 nLocktime: \
					 0\nIssuers:\n45GfjkWCWQhJ3epJVGC2NSg1Rcu4Ue1vDD3kk9eLs5TQ\nInputs:\n1023:0:D:\
					 45GfjkWCWQhJ3epJVGC2NSg1Rcu4Ue1vDD3kk9eLs5TQ:396949\n1023:0:D:\
					 45GfjkWCWQhJ3epJVGC2NSg1Rcu4Ue1vDD3kk9eLs5TQ:397221\n1023:0:D:\
					 45GfjkWCWQhJ3epJVGC2NSg1Rcu4Ue1vDD3kk9eLs5TQ:397493\n1023:0:D:\
					 45GfjkWCWQhJ3epJVGC2NSg1Rcu4Ue1vDD3kk9eLs5TQ:397789\nUnlocks:\n0:SIG(0)\n1:\
					 SIG(0)\n2:SIG(0)\n3:SIG(0)\nOutputs:\n4000:0:\
					 SIG(6pbXkPbwvDarLJM6P7CcDyhgQxBCiySPpAAyC8TvTLUk)\n92:0:\
					 SIG(45GfjkWCWQhJ3epJVGC2NSg1Rcu4Ue1vDD3kk9eLs5TQ)\nComment: test \
					 GMixer\njKlwli1Uj8cQEzoH+sPTP0+Y1Ewrk6RdkXhF9fqi7/\
					 r0Y+aDYBSxPkDhFGLA5xV732YSDjhKiKMy5Wn6w7MTBA=="
						.into()
				)
				.hash()
			),
			"C49F755A7BECB5F4621A29DF28ECEB008DC19427AAFFC351A7675A0FC8768312"
		);
	}
}
