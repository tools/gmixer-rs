use regex::Regex;
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use std::{
	convert::TryInto,
	io,
	net::{Ipv4Addr, Ipv6Addr},
	str::FromStr,
};

pub mod api;
pub mod gva;

const NODE_ID_SIZE: usize = 32;

#[derive(Clone, Debug, Deserialize, Eq, Hash, PartialEq, Serialize)]
pub struct NodeId([u8; NODE_ID_SIZE]);

impl NodeId {
	pub fn generate() -> Self {
		Self(rand::random())
	}

	/// Serialize base64
	pub fn serialize<S: Serializer>(&self, s: S) -> Result<S::Ok, S::Error> {
		let base64 = base64::encode_config(&self.0, base64::URL_SAFE_NO_PAD);
		String::serialize(&base64, s)
	}

	/// Deserialize base64
	pub fn deserialize<'de, D: Deserializer<'de>>(d: D) -> Result<Self, D::Error> {
		let base64 = String::deserialize(d)?;
		base64::decode_config(base64.as_bytes(), base64::URL_SAFE_NO_PAD).map_or_else(
			|e| Err(serde::de::Error::custom(e)),
			|b| {
				b.try_into().map_or_else(
					|_| Err(serde::de::Error::custom("invalid length")),
					|b| Ok(NodeId(b)),
				)
			},
		)
	}
}

impl FromStr for NodeId {
	type Err = io::Error;
	fn from_str(raw: &str) -> Result<Self, Self::Err> {
		let mut buf = [0u8; NODE_ID_SIZE];
		base64::decode_config_slice(raw, base64::URL_SAFE_NO_PAD, &mut buf)
			.map_err(|_| io::Error::new(io::ErrorKind::Other, "Invalid NodeId"))?;
		Ok(NodeId(buf))
	}
}

impl ToString for NodeId {
	fn to_string(&self) -> String {
		base64::encode_config(self.0, base64::URL_SAFE_NO_PAD)
	}
}

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub enum Addr {
	Hostname(String),
	Ipv4(Ipv4Addr),
	Ipv6(Ipv6Addr),
}

impl FromStr for Addr {
	type Err = io::Error;
	fn from_str(raw: &str) -> Result<Self, Self::Err> {
		Ok(if raw.starts_with('[') {
			Self::Ipv6(
				Ipv6Addr::from_str(raw)
					.map_err(|_| io::Error::new(io::ErrorKind::Other, "Invalid IPv6"))?,
			)
		} else if Regex::new(r"^(([01]?\d?\d|2[0-4]\d|25[0-5])\.){3}([01]?\d?\d|2[0-4]\d|25[0-5])$")
			.unwrap()
			.is_match(raw)
		{
			Self::Ipv4(
				Ipv4Addr::from_str(raw)
					.map_err(|_| io::Error::new(io::ErrorKind::Other, "Invalid IPv4"))?,
			)
		} else {
			Self::Hostname(String::from(raw))
		})
	}
}

impl ToString for Addr {
	fn to_string(&self) -> String {
		match self {
			Self::Hostname(v) => v.to_owned(),
			Self::Ipv4(v) => v.to_string(),
			Self::Ipv6(v) => format!("[{}]", v),
		}
	}
}

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub enum NetworkProtocol {
	Http,
	Https,
}

impl FromStr for NetworkProtocol {
	type Err = io::Error;
	fn from_str(raw: &str) -> Result<Self, Self::Err> {
		Ok(match raw {
			"http" => Self::Http,
			"https" => Self::Https,
			_ => {
				return Err(io::Error::new(io::ErrorKind::Other, "Unknown protocol"));
			}
		})
	}
}

impl ToString for NetworkProtocol {
	fn to_string(&self) -> String {
		String::from(match self {
			Self::Http => "http",
			Self::Https => "https",
		})
	}
}

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct NodeAddress {
	pub addr: Addr,
	pub port: u16,
	pub protocol: NetworkProtocol,
}

impl ToString for NodeAddress {
	fn to_string(&self) -> String {
		format!(
			"{}{}:{}",
			match self.protocol {
				NetworkProtocol::Http => "http://",
				NetworkProtocol::Https => "https://",
			},
			self.addr.to_string(),
			self.port,
		)
	}
}

impl NodeAddress {
	pub fn to_url(&self, path: &str) -> String {
		format!("{}{}", self.to_string(), path)
	}
}

impl FromStr for NodeAddress {
	type Err = io::Error;

	fn from_str(raw: &str) -> Result<Self, Self::Err> {
		if let Some(caps) =
			Regex::new(r"^/*((\w+):?/+)?([\w\.]+|\[[\da-fA-F:]+\])([:/](\d{1,5}))?/*$")
				.unwrap()
				.captures(raw)
		{
			Ok(Self {
				addr: caps
					.get(3)
					.map_or(Ok(Addr::Ipv4(Ipv4Addr::new(127, 0, 0, 1))), |x| {
						Addr::from_str(x.as_str()).map_err(|e| {
							io::Error::new(
								io::ErrorKind::Other,
								format!("Invalid address: {:?}", e),
							)
						})
					})?,
				port: caps.get(5).map_or(Ok(10953), |x| {
					x.as_str()
						.parse()
						.map_err(|_| io::Error::new(io::ErrorKind::Other, "Invalid port"))
				})?,
				protocol: caps.get(2).map_or(Ok(NetworkProtocol::Http), |x| {
					NetworkProtocol::from_str(x.as_str())
						.map_err(|_| io::Error::new(io::ErrorKind::Other, "Invalid protocol"))
				})?,
			})
		} else {
			Err(io::Error::new(io::ErrorKind::Other, "Invalid address"))
		}
	}
}

#[derive(Debug, Deserialize, Serialize)]
pub struct GvaAddress(pub String);
