pub mod stores;

use gmixer_common::prelude::*;
pub use stores::MixStatus;

use crate::server::config::ServerConfig;

use log::{info, warn};
use sled::{IVec, Tree};
use std::{
	collections::{BTreeMap, HashMap},
	fs,
	ops::DerefMut,
	path::Path,
	sync::{Arc, RwLock},
	time::{SystemTime, UNIX_EPOCH},
};

const DB_NAME_MIX_CONFIRM: &str = "mix-confirm";
const DB_NAME_MIX_DEMAND: &str = "mix-demand";
const DB_NAME_NODE: &str = "node";
const DB_NAME_TX_ID_LIST: &str = "tx-id-list";

pub struct Dbs {
	/// Mix confirms by output_tx_id_seed_1
	pub mix_confirm: Tree,
	/// Mix demands by output_tx_id_seed_1
	pub mix_demand: Tree,
	/// Nodes by public key
	pub node: Tree,
	/// TxIdLists by comment
	pub tx_id_list: Tree,
}

pub struct Indexes {
	/// Mix demands' output_tx_id_seed_1's by input_tx_id
	/// Only stores when they are confirmed but not mixed
	pub mix_confirm_by_input_tx_id: BTreeMap<TxId, TxIdSeed>,
	/// Mix demands' output_tx_id_seed_1's by input_tx_id_seed_1
	/// Only stores when they are not sent and have no node address
	pub mix_confirm_by_input_tx_id_seed_1: BTreeMap<TxIdSeed, TxIdSeed>,
}

pub fn load(path: &Path) -> Result<(Indexes, Dbs), Error> {
	let dbs = load_dbs(path)?;

	Ok((load_indexes(&dbs.mix_confirm), dbs))
}

pub fn load_dbs(path: &Path) -> Result<Dbs, Error> {
	fs::create_dir_all(path).map_err(|_| Error::CouldNotCreateDir)?;
	let db = sled::open(path)?;

	Ok(Dbs {
		mix_confirm: db.open_tree(DB_NAME_MIX_CONFIRM)?,
		mix_demand: db.open_tree(DB_NAME_MIX_DEMAND)?,
		node: db.open_tree(DB_NAME_NODE)?,
		tx_id_list: db.open_tree(DB_NAME_TX_ID_LIST)?,
	})
}

#[cfg(test)]
pub fn load_dbs_tmp() -> Result<Dbs, Error> {
	let db = sled::Config::new().temporary(true).open()?;

	Ok(Dbs {
		mix_confirm: db.open_tree(DB_NAME_MIX_CONFIRM)?,
		mix_demand: db.open_tree(DB_NAME_MIX_DEMAND)?,
		node: db.open_tree(DB_NAME_NODE)?,
		tx_id_list: db.open_tree(DB_NAME_TX_ID_LIST)?,
	})
}

pub fn load_indexes(mix_confirm: &Tree) -> Indexes {
	let mut mix_confirm_by_input_tx_id: BTreeMap<TxId, TxIdSeed> = BTreeMap::new();
	let mut mix_confirm_by_input_tx_id_seed_1: BTreeMap<TxIdSeed, TxIdSeed> = BTreeMap::new();

	for v in mix_confirm {
		let v = v.unwrap();
		if let Ok(mix_confirm_store) = bincode::deserialize::<stores::MixConfirmStore>(&v.1) {
			if mix_confirm_store.status == MixStatus::ConfirmSent {
				mix_confirm_by_input_tx_id.insert(
					TxId::generate(&mix_confirm_store.mix_confirm.input_tx_id_seeds),
					mix_confirm_store.mix_confirm.output_tx_id_seeds[1].clone(),
				);
			} else if mix_confirm_store.status == MixStatus::ConfirmNotSent
				&& mix_confirm_store.node_address.is_none()
			{
				mix_confirm_by_input_tx_id_seed_1.insert(
					mix_confirm_store.mix_confirm.input_tx_id_seeds[1].clone(),
					mix_confirm_store.mix_confirm.output_tx_id_seeds[1].clone(),
				);
			}
		}
	}

	Indexes {
		mix_confirm_by_input_tx_id,
		mix_confirm_by_input_tx_id_seed_1,
	}
}

fn clean_mix_demand(config: &ServerConfig, tree: &Tree) {
	if let Some(t) = SystemTime::now()
		.duration_since(UNIX_EPOCH)
		.unwrap()
		.as_secs()
		.checked_sub(config.protocol.mix_demand_expire)
	{
		let mut to_remove: Vec<IVec> = vec![];
		for v in tree.iter() {
			let v = v.unwrap();
			if let Ok(mix_demand_store) = bincode::deserialize::<stores::MixDemandStore>(&v.1) {
				if mix_demand_store.received_time < t {
					to_remove.push(v.0);
				}
			} else {
				to_remove.push(v.0);
			}
		}
		if !to_remove.is_empty() {
			info!("Cleaning {} mix demands", to_remove.len());
		}
		for i in to_remove {
			tree.remove(i).unwrap();
		}
	}
}

fn clean_mix_confirm(tree: &Tree, indexes: &RwLock<Indexes>) {
	let t = SystemTime::now()
		.duration_since(UNIX_EPOCH)
		.unwrap()
		.as_secs();
	let mut to_remove: Vec<IVec> = vec![];
	let mut to_remove_index: Vec<TxIdSeed> = vec![];
	for v in tree.iter() {
		let v = v.unwrap();
		if let Ok(mix_confirm_store) = bincode::deserialize::<stores::MixConfirmStore>(&v.1) {
			if mix_confirm_store.mix_confirm.expire_time < t {
				to_remove.push(v.0);
				to_remove_index.push(mix_confirm_store.mix_confirm.input_tx_id_seeds[1].clone());
			}
		} else {
			to_remove.push(v.0);
		}
	}
	if !to_remove.is_empty() {
		info!("Cleaning {} mix confirms", to_remove.len());
	}
	for i in to_remove {
		tree.remove(i).unwrap();
	}
	let mut indexes = indexes.write().unwrap();
	let mix_confirm_by_input_tx_id_seed_1 =
		&mut indexes.deref_mut().mix_confirm_by_input_tx_id_seed_1;
	for i in to_remove_index {
		// may attempt to remove not existing values that must be ignored
		mix_confirm_by_input_tx_id_seed_1.remove(&i);
	}
}

fn clean_node(config: &ServerConfig, tree: &Tree) {
	if let Some(t) = SystemTime::now()
		.duration_since(UNIX_EPOCH)
		.unwrap()
		.as_secs()
		.checked_sub(config.network.node_expire)
	{
		let mut n_removed = 0u32;
		let mut n_invalid = 0u32;
		let mut pubkey_to_remove: Vec<IVec> = vec![];
		for v in tree.iter() {
			let v = v.unwrap();

			if let Ok(mut node_stores) =
				bincode::deserialize::<HashMap<NodeId, stores::NodeStore>>(&v.1)
			{
				let mut node_to_remove = vec![];
				node_stores.iter().for_each(|(id, node_store)| {
					if node_store.received_time < t
						|| node_store
							.node_sig
							.inner
							.verify_expire(&config.protocol)
							.is_err() || node_store.node_sig.inner.id == config.node_id
					{
						node_to_remove.push(id.clone());
					}
				});
				if node_to_remove.len() == node_stores.len() {
					n_removed += node_to_remove.len() as u32;
					pubkey_to_remove.push(v.0);
				} else if !node_to_remove.is_empty() {
					n_removed += node_to_remove.len() as u32;
					node_to_remove.iter().for_each(|id| {
						node_stores.remove(id);
					});
					tree.insert(v.0, bincode::serialize(&node_stores).unwrap())
						.unwrap();
				}
			} else {
				pubkey_to_remove.push(v.0);
				n_invalid += 1;
			}
		}

		if n_invalid != 0 {
			warn!("Node: {} invalid entries", n_invalid);
		}
		if !pubkey_to_remove.is_empty() {
			info!(
				"Cleaning {} pubkeys, {} nodes",
				pubkey_to_remove.len(),
				n_removed
			);
		}
		for i in pubkey_to_remove {
			tree.remove(i).unwrap();
		}
	}
}

fn clean_tx_id_list(config: &ServerConfig, tree: &Tree) {
	if let Some(t) = SystemTime::now()
		.duration_since(UNIX_EPOCH)
		.unwrap()
		.as_secs()
		.checked_sub(config.tx_id_list_expire)
	{
		let mut to_remove = vec![];
		for v in tree.iter() {
			let (tx_id_list_hash, raw) = v.unwrap();
			if let Ok(tx_id_list_store) = bincode::deserialize::<stores::TxIdListStore>(&raw) {
				if tx_id_list_store.time < t {
					to_remove.push(tx_id_list_hash);
				}
			} else {
				to_remove.push(tx_id_list_hash);
			}
		}
		for i in to_remove {
			tree.remove(i).unwrap();
		}
	}
}

/// Remove expired or invalid entries
fn clean_dbs(config: &ServerConfig, dbs: &Dbs, indexes: &RwLock<Indexes>) {
	clean_mix_demand(config, &dbs.mix_demand);
	clean_mix_confirm(&dbs.mix_confirm, indexes);
	clean_node(config, &dbs.node);
	clean_tx_id_list(config, &dbs.tx_id_list);
}

pub async fn dbs_cleaner(config: Arc<ServerConfig>, dbs: Arc<Dbs>, indexes: Arc<RwLock<Indexes>>) {
	let mut interval =
		tokio::time::interval(tokio::time::Duration::from_secs(config.clean_db_interval));

	loop {
		interval.tick().await;

		clean_dbs(&config, &dbs, indexes.as_ref());
	}
}
