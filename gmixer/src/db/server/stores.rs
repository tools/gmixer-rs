use gmixer_common::prelude::*;

use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, PartialEq, Serialize)]
pub enum MixStatus {
	/// Step 1: received demand
	ConfirmNotSent,
	/// Step 2: confirm sent, tx not sent
	ConfirmSent,
	/// Step 3: tx sent
	TxSent,
}

#[derive(Deserialize, Serialize)]
pub struct MixDemandStore {
	pub input_tx_id_seed_1: TxIdSeed,
	pub mix_demand: documents::mix_demand::MixDemand,
	/// Issuer node address (may be None if client)
	pub node_address: Option<NodeAddress>,
	pub output_tx_id_seed_1: TxIdSeed,
	pub received_time: u64,
	pub sent: bool,
}

#[derive(Deserialize, Serialize)]
pub struct MixConfirmStore {
	pub mix_confirm_onion_wrapper_secret: documents::mix_confirm::MixConfirmOnionWrapperSecret,
	pub mix_confirm: documents::mix_confirm::MixConfirm,
	/// Issuer node address (may be None if client)
	pub node_address: Option<NodeAddress>,
	/// Whether confirm is sent
	pub status: MixStatus,
}

#[derive(Deserialize, Serialize)]
pub struct NodeStore {
	pub node_sig: documents::node::NodeSig,
	pub received_time: u64,
}

#[derive(Deserialize, Serialize)]
pub struct TxIdListStore {
	pub tx_id_list: documents::tx_id_list::TxIdList,
	pub time: u64,
}
