use gmixer_common::prelude::*;

use dup_crypto::{keys::ed25519::PublicKey, seeds::Seed32};
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, PartialEq, Serialize)]
pub enum MixStatus {
	/// Step 4: output tx received
	Complete,
	/// Step 2: confirmed, unsent tx
	Confirmed,
	/// Step 0: unsent mix demands
	Unsent,
	/// Step 1: sent, waiting for confirm
	WaitingForConfirm,
	/// Step 3: confirms, tx sent, waiting for output tx
	WaitingForTx,
}

#[derive(Deserialize, Serialize)]
pub struct NodeStore {
	pub node_sig: documents::node::NodeSig,
	pub received_time: u64,
}

#[derive(Deserialize, Serialize)]
pub struct PendingMixStore {
	pub ephemeral_keypairs_seeds: Vec<Seed32>,
	pub mix_confirms: Vec<documents::mix_confirm::MixConfirmSig>,
	pub mix_demand_onion: documents::mix_demand::MixDemandOnion,
	pub mix_demands: Vec<documents::mix_demand::MixDemand>,
	pub node: Option<documents::node::Node>,
	pub output_tx_id_seed_1: TxIdSeed,
	pub path: Vec<PublicKey>,
	pub status: MixStatus,
}
