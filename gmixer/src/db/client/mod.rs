pub mod stores;

use gmixer_common::prelude::*;
pub use stores::MixStatus;

use crate::client::config::ClientConfig;

use sled::{IVec, Tree};
use std::{
	collections::HashMap,
	fs,
	path::Path,
	time::{SystemTime, UNIX_EPOCH},
};

const DB_NAME_NODE: &str = "node";
const DB_NAME_PENDING_MIXES: &str = "pending-mixes";

pub struct Dbs {
	/// Nodes by public key
	pub node: Tree,
	/// Pending mixes by output tx id seed 0
	pub pending_mixes: Tree,
}

pub fn load(path: &Path) -> Result<Dbs, Error> {
	fs::create_dir_all(path).map_err(|_| Error::CouldNotCreateDir)?;
	let db = sled::open(path)?;

	Ok(Dbs {
		node: db.open_tree(DB_NAME_NODE)?,
		pending_mixes: db.open_tree(DB_NAME_PENDING_MIXES)?,
	})
}

fn clean_node(config: &ClientConfig, tree: &Tree) {
	if let Some(t) = SystemTime::now()
		.duration_since(UNIX_EPOCH)
		.unwrap()
		.as_secs()
		.checked_sub(config.network.node_expire)
	{
		let mut pubkey_to_remove: Vec<IVec> = vec![];
		for v in tree.iter() {
			let v = v.unwrap();

			if let Ok(mut node_stores) =
				bincode::deserialize::<HashMap<NodeId, stores::NodeStore>>(&v.1)
			{
				let mut node_to_remove = vec![];
				node_stores.iter().for_each(|(id, node_store)| {
					if node_store.received_time < t
						|| node_store
							.node_sig
							.inner
							.verify_expire(&config.protocol)
							.is_err()
					{
						node_to_remove.push(id.clone());
					}
				});
				if node_to_remove.len() == node_stores.len() {
					pubkey_to_remove.push(v.0);
				} else if !node_to_remove.is_empty() {
					node_to_remove.iter().for_each(|id| {
						node_stores.remove(id);
					});
					tree.insert(v.0, bincode::serialize(&node_stores).unwrap())
						.unwrap();
				}
			} else {
				pubkey_to_remove.push(v.0);
			}
		}
		for i in pubkey_to_remove {
			tree.remove(i).unwrap();
		}
	}
}

/// Remove expired or invalid entries
pub fn clean_dbs(config: &ClientConfig, dbs: &Dbs) {
	clean_node(config, &dbs.node);
}
