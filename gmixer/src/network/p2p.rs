use crate::{common::*, db, network::api::client, server::config::ServerConfig};
use gmixer_common::prelude::*;

use dup_crypto::{
	bases::b58::ToBase58,
	keys::{
		ed25519::{Ed25519KeyPair, PublicKey},
		KeyPair,
	},
};
use log::{debug, error, info, warn};
use std::{
	collections::{BTreeMap, HashMap, VecDeque},
	sync::{Arc, RwLock},
	time::{Duration, SystemTime, UNIX_EPOCH},
};

const MAIN_INTERVAL: u64 = 5;

pub async fn p2p_main(
	config: Arc<ServerConfig>,
	dbs: Arc<db::server::Dbs>,
	indexes: Arc<RwLock<db::server::Indexes>>,
	instant_requests: Arc<RwLock<VecDeque<InstantRequest>>>,
	keypair: Arc<Ed25519KeyPair>,
	local_node_sig: Arc<RwLock<documents::node::NodeSig>>,
	pubkey_delegation_sig: Arc<RwLock<Option<documents::pubkey_delegation::PubkeyDelegationSig>>>,
) {
	let rclient = reqwest::Client::new();

	//let mut interval = tokio::time::interval(tokio::time::Duration::from_secs(MAIN_INTERVAL));
	let interval = Duration::from_secs(MAIN_INTERVAL);

	let mut t = SystemTime::now()
		.duration_since(UNIX_EPOCH)
		.unwrap()
		.as_secs();

	let mut next_ask_nodes: u64 = t + config.node_fetch_interval;
	let mut next_spread_node_sig: u64 = 0;
	let mut next_treat_mix_confirms: u64 = 0;
	let mut next_treat_mix_demands: u64 = 0;
	let mut next_treat_txs: u64 = 0;
	let mut next_update_node_sig: u64 = 0;

	loop {
		//interval.tick().await;
		t = SystemTime::now()
			.duration_since(UNIX_EPOCH)
			.unwrap()
			.as_secs();

		if t > next_ask_nodes {
			ask_nodes(&config, &dbs, &rclient).await;
			next_ask_nodes = t + config.node_fetch_interval;
		}

		if t > next_update_node_sig {
			let mut local_node_sig_mutex = local_node_sig.write().unwrap();
			let pubkey_delegation_sig_mutex = pubkey_delegation_sig.read().unwrap();
			*local_node_sig_mutex = crate::common::node::generate_node_sig(
				&config,
				&keypair,
				&config.node_id,
				&pubkey_delegation_sig_mutex,
			);
			info!("Updated local node sig");
			next_update_node_sig = t + config.update_node_sig_interval;
		}

		if t > next_spread_node_sig {
			let local_node_sig_mutex = local_node_sig.read().unwrap();
			spread_node_sig(&dbs, &*local_node_sig_mutex, &rclient).await;
			next_spread_node_sig = t + config.spread_node_sig_interval;
		}

		if t > next_treat_mix_demands {
			treat_mix_demands(&config, &dbs, &*keypair, &rclient).await;
			next_treat_mix_demands = t + config.mix_demand_treat_interval;
		}

		if t > next_treat_mix_confirms {
			treat_mix_confirms(&dbs, &indexes, &rclient).await;
			next_treat_mix_confirms = t + config.mix_confirm_treat_interval;
		}

		if t > next_treat_txs {
			ask_tx_id_lists(&dbs, &rclient).await;
			treat_txs(&dbs, &indexes, instant_requests.clone(), &keypair, &config).await;
			next_treat_txs = t + config.txs_treat_interval;
		}

		while let Some(instant_request) = instant_requests.write().unwrap().pop_front() {
			match instant_request {
				InstantRequest::MixConfirm(k) => {
					if let Some(v) = dbs.mix_confirm.get(k).unwrap() {
						if let Ok(mix_confirm_store) =
							bincode::deserialize::<db::server::stores::MixConfirmStore>(&v)
						{
							if mix_confirm_store.status == db::server::MixStatus::ConfirmNotSent
								&& mix_confirm_store.node_address.is_some()
								&& mix_confirm_store.mix_confirm.expire_time >= t
							{
								treat_mix_confirm(&dbs, &indexes, mix_confirm_store, &rclient)
									.await;
							}
						}
					}
				}

				InstantRequest::MixDemand(k) => {
					if let Some(v) = dbs.mix_demand.get(k).unwrap() {
						if let Ok(mix_demand_store) =
							bincode::deserialize::<db::server::stores::MixDemandStore>(&v)
						{
							if !mix_demand_store.sent && mix_demand_store.mix_demand.onion.is_some()
							{
								treat_mix_demand(
									&config,
									&dbs,
									&keypair,
									mix_demand_store,
									&rclient,
								)
								.await;
							}
						}
					}
				}

				// TODO send several tx_id_lists per request
				InstantRequest::TxIdList(k) => {
					if let Some(v) = dbs.tx_id_list.get(k).unwrap() {
						if let Ok(tx_id_list_store) =
							bincode::deserialize::<db::server::stores::TxIdListStore>(&v)
						{
							info!(
								"Post tx id list {:?}",
								tx_id_list_store.tx_id_list.hash().to_comment()
							);
							let tx_id_list_stores = vec![tx_id_list_store];
							for raw in dbs.node.iter().values() {
								if let Ok(node_stores) = bincode::deserialize::<
									HashMap<NodeId, db::server::stores::NodeStore>,
								>(&raw.unwrap())
								{
									for node_store in node_stores.values() {
										if let Err(e) = client::post_tx_id_list(
											&rclient,
											&node_store.node_sig.inner.address,
											&tx_id_list_stores,
										)
										.await
										{
											warn!(
												"Cannot post TxIdList to {}: {:?}",
												node_store.node_sig.inner.address.to_string(),
												e
											);
										}
									}
								}
							}
						}
					}
				}
			}
		}

		tokio::time::sleep(interval).await;
	}
}

pub async fn ask_nodes(config: &ServerConfig, dbs: &db::server::Dbs, rclient: &reqwest::Client) {
	let mut received_node_sigs: HashMap<PublicKey, HashMap<NodeId, documents::node::NodeSig>> =
		HashMap::new();

	for raw in dbs.node.iter().values().flatten() {
		if let Ok(node_stores) =
			bincode::deserialize::<HashMap<NodeId, db::server::stores::NodeStore>>(&raw)
		{
			for node_store in node_stores.values() {
				if let Ok(new_node_sigs) =
					api::get_nodes(rclient, &node_store.node_sig.inner.address).await
				{
					for new_node_sig in new_node_sigs {
						if new_node_sig.inner.id == config.node_id {
							continue;
						}
						if new_node_sig.verify(&config.protocol).is_err() {
							warn!(
								"Bad NodeSig from {}",
								node_store.node_sig.inner.address.to_string()
							);
							continue;
						}

						if let Some(node_sigs) =
							received_node_sigs.get_mut(&new_node_sig.inner.pubkey)
						{
							if let Some(old_node_sig) = node_sigs.get(&new_node_sig.inner.id) {
								if old_node_sig.inner.sigtime >= new_node_sig.inner.sigtime {
									continue;
								}
							}
							node_sigs.insert(new_node_sig.inner.id.clone(), new_node_sig);
						} else {
							let pubkey = new_node_sig.inner.pubkey;
							let mut node_sigs = HashMap::new();
							node_sigs.insert(new_node_sig.inner.id.clone(), new_node_sig);
							received_node_sigs.insert(pubkey, node_sigs);
						}
					}
				} else {
					warn!(
						"Cannot get NodeSigs from {}",
						node_store.node_sig.inner.address.to_string()
					);
				}
			}
		}
	}

	let now = SystemTime::now()
		.duration_since(UNIX_EPOCH)
		.unwrap()
		.as_secs();

	for (pubkey, new_node_sigs) in received_node_sigs {
		let mut node_sigs: HashMap<NodeId, db::server::stores::NodeStore>;

		if let Some(raw) = dbs.node.get(pubkey).unwrap() {
			node_sigs = bincode::deserialize(&raw).unwrap();

			for (node_id, new_node_sig) in new_node_sigs {
				if let Some(old_node_store) = node_sigs.get(&node_id) {
					if old_node_store.node_sig.inner.sigtime >= new_node_sig.inner.sigtime {
						continue;
					}
				}
				info!(
					"Added NodeSig {}@{}",
					new_node_sig.inner.id.to_string(),
					new_node_sig.inner.address.to_string()
				);
				node_sigs.insert(
					node_id,
					db::server::stores::NodeStore {
						node_sig: new_node_sig,
						received_time: now,
					},
				);
			}
		} else {
			node_sigs = new_node_sigs
				.into_iter()
				.map(|(k, v)| {
					info!(
						"Added NodeSig {}@{}",
						v.inner.id.to_string(),
						v.inner.address.to_string()
					);
					(
						k,
						db::server::stores::NodeStore {
							node_sig: v,
							received_time: now,
						},
					)
				})
				.collect();
		}

		dbs.node
			.insert(pubkey, bincode::serialize(&node_sigs).unwrap())
			.unwrap();
	}
}

async fn spread_node_sig(
	dbs: &db::server::Dbs,
	local_node_sig: &documents::node::NodeSig,
	rclient: &reqwest::Client,
) {
	info!("Spreading local node sig");
	let mut n_ok = 0u32;
	let mut n_err = 0u32;
	for raw in dbs.node.iter().values().flatten() {
		if let Ok(node_stores) =
			bincode::deserialize::<HashMap<NodeId, db::server::stores::NodeStore>>(&raw)
		{
			for node_store in node_stores.values() {
				if let Err(e) =
					client::post_node(rclient, &node_store.node_sig.inner.address, local_node_sig)
						.await
				{
					warn!("Cannot spread: {:?}", e);
					n_err += 1;
				} else {
					n_ok += 1;
				}
			}
		}
	}
	if n_ok != 0 || n_err != 0 {
		info!("Spread local node sig: {} ok, {} err", n_ok, n_err);
	}
}

async fn treat_mix_demands(
	config: &ServerConfig,
	dbs: &db::server::Dbs,
	keypair: &Ed25519KeyPair,
	rclient: &reqwest::Client,
) {
	let t = SystemTime::now()
		.duration_since(UNIX_EPOCH)
		.unwrap()
		.as_secs()
		.checked_sub(config.protocol.mix_demand_expire)
		.unwrap();
	for v in dbs.mix_demand.iter().values() {
		if let Ok(mix_demand_store) =
			bincode::deserialize::<db::server::stores::MixDemandStore>(&v.unwrap())
		{
			if !mix_demand_store.sent
				&& mix_demand_store.mix_demand.onion.is_some()
				&& mix_demand_store.received_time >= t
			{
				treat_mix_demand(config, dbs, keypair, mix_demand_store, rclient).await;
			}
		}
	}
}

async fn treat_mix_demand(
	config: &ServerConfig,
	dbs: &db::server::Dbs,
	keypair: &Ed25519KeyPair,
	mix_demand_store: db::server::stores::MixDemandStore,
	rclient: &reqwest::Client,
) {
	info!(
		"Mix demand: {} {}->{} {}",
		mix_demand_store.mix_demand.issuer,
		if mix_demand_store.node_address.is_none() {
			"|"
		} else {
			""
		},
		if mix_demand_store.mix_demand.onion.is_none() {
			"|"
		} else {
			""
		},
		mix_demand_store.mix_demand.recipient,
	);
	if let Some(node_stores) = dbs
		.node
		.get(&mix_demand_store.mix_demand.recipient)
		.unwrap()
	{
		if let Ok(node_stores) =
			bincode::deserialize::<HashMap<NodeId, db::server::stores::NodeStore>>(&node_stores)
		{
			// This part would be way simpler with for break values
			let mut sent = false;
			for node_address in ShuffleIter::new(
				&mut node_stores
					.values()
					.map(|node_store| &node_store.node_sig.inner.address)
					.collect(),
			) {
				match api::send_mix_demand_onion_wrapper_secret(
					rclient,
					&documents::mix_demand::MixDemandOnionWrapperSecret::from_mix_demand_onion_wrapper(
						keypair,
						documents::mix_demand::MixDemandOnionWrapper {
							tx_id_seed_1: mix_demand_store.output_tx_id_seed_1.clone(),
							node_address: Some(config.public_address.clone()),
							onion: mix_demand_store.mix_demand.onion.clone().unwrap(),
						},
						&mix_demand_store.mix_demand.recipient,
					),
					node_address,
				).await {
					Err(e) => {warn!("Error sending mix demand: {:?}", e); },
					_ => {sent = true; break;},
				}
			}
			if sent {
				let mut mix_demand_store = mix_demand_store;
				mix_demand_store.sent = true;
				dbs.mix_demand
					.insert(
						mix_demand_store.output_tx_id_seed_1.clone(),
						bincode::serialize(&mix_demand_store).unwrap(),
					)
					.unwrap();
			} else {
				warn!("Cannot send mix demand: no ok node");
			}
		}
	}
}

async fn treat_mix_confirms(
	dbs: &db::server::Dbs,
	indexes: &RwLock<db::server::Indexes>,
	rclient: &reqwest::Client,
) {
	let t = SystemTime::now()
		.duration_since(UNIX_EPOCH)
		.unwrap()
		.as_secs();
	for v in dbs.mix_confirm.iter().values() {
		if let Ok(mix_confirm_store) =
			bincode::deserialize::<db::server::stores::MixConfirmStore>(&v.unwrap())
		{
			if mix_confirm_store.status == db::server::MixStatus::ConfirmNotSent
				&& mix_confirm_store.node_address.is_some()
				&& mix_confirm_store.mix_confirm.expire_time >= t
			{
				treat_mix_confirm(dbs, indexes, mix_confirm_store, rclient).await;
			}
		}
	}
}

async fn treat_mix_confirm(
	dbs: &db::server::Dbs,
	indexes: &RwLock<db::server::Indexes>,
	mix_confirm_store: db::server::stores::MixConfirmStore,
	rclient: &reqwest::Client,
) {
	info!(
		"Mix confirm: {} -> {}",
		mix_confirm_store.mix_confirm.issuer, mix_confirm_store.mix_confirm.recipient,
	);

	if let Err(e) = client::send_mix_confirm_onion_wrapper_secret(
		rclient,
		&mix_confirm_store.mix_confirm_onion_wrapper_secret,
		&mix_confirm_store.node_address.clone().unwrap(),
	)
	.await
	{
		warn!("Error sending mix confirm: {:?}", e);
	} else {
		let mut mix_confirm_store = mix_confirm_store;
		mix_confirm_store.status = db::server::MixStatus::ConfirmSent;
		dbs.mix_confirm
			.insert(
				mix_confirm_store.mix_confirm.output_tx_id_seeds[1].clone(),
				bincode::serialize(&mix_confirm_store).unwrap(),
			)
			.unwrap();
		indexes.write().unwrap().mix_confirm_by_input_tx_id.insert(
			TxId::generate(&mix_confirm_store.mix_confirm.input_tx_id_seeds),
			mix_confirm_store.mix_confirm.output_tx_id_seeds[1].clone(),
		);
	}
}

async fn treat_txs(
	dbs: &db::server::Dbs,
	indexes: &RwLock<db::server::Indexes>,
	instant_requests: Arc<RwLock<VecDeque<InstantRequest>>>,
	keypair: &Ed25519KeyPair,
	config: &ServerConfig,
) {
	if indexes
		.read()
		.unwrap()
		.mix_confirm_by_input_tx_id
		.is_empty()
	{
		return;
	}

	info!("Handling transactions");
	/*let time = SystemTime::now()
		.duration_since(UNIX_EPOCH)
		.unwrap()
		.as_secs();
	let time_from = time
		.checked_sub(config.protocol.mix_confirm_expire)
		.unwrap_or(0);
	let time_to = time
		.checked_add(config.protocol.mix_confirm_expire)
		.unwrap();*/

	let rclient = reqwest::Client::new();

	// Fetch transactions
	let mut result = None;
	for gva_address in OrderedShuffleIter::new(config.network.gva_nodes.iter()) {
		if let Ok(r) = gva::get_txs_received(&rclient, gva_address, &keypair.public_key()).await {
			result = Some(r);
			break;
		}
	}
	if result.is_none() {
		error!("No working GVA node");
		return;
	}
	let (blockstamp, txs) = result.unwrap();

	// Look at each transaction

	let condition = format!("SIG({})", keypair.public_key().to_base58());
	let mut mixes_by_amount =
		HashMap::<Amount, (Vec<db::server::stores::MixConfirmStore>, Vec<dubp::Source>)>::new();
	for tx in &txs {
		debug!("Comment: {}", tx.comment);
		match decode_comment(&tx.comment) {
			Ok(DecodedComment::TxId(tx_id)) => {
				if let Some(output_tx_id_seed_1) = indexes
					.read()
					.unwrap()
					.mix_confirm_by_input_tx_id
					.get(&tx_id)
				{
					if let Some(raw) = dbs.mix_confirm.get(output_tx_id_seed_1).unwrap() {
						if let Ok(mix_confirm_store) =
							bincode::deserialize::<db::server::stores::MixConfirmStore>(&raw)
						{
							debug!("Found mix from TxId");
							if tx.written_time < mix_confirm_store.mix_confirm.expire_time
								&& tx.issuers.len() == 1 && tx.issuers[0]
								== mix_confirm_store.mix_confirm.issuer
							{
								debug!("Valid mix");
								let mut received_amount = 0;
								let outputs: Vec<(u32, &dubp::Output)> = tx
									.outputs
									.iter()
									.zip(0u32..)
									.filter_map(|(output, index)| {
										output.as_ref().and_then(|output| {
											(output.condition == condition).then(|| {
												received_amount += output.amount.to_u64();
												(index, output)
											})
										})
									})
									.collect();

								if received_amount
									< mix_confirm_store.mix_confirm.input_amount.to_u64()
								{
									warn!("Received tx but insufficient amount to mix!");
									continue;
								}

								if let Some(mixes_same_amount) = mixes_by_amount
									.get_mut(&mix_confirm_store.mix_confirm.output_amount)
								{
									mixes_same_amount.0.push(mix_confirm_store);
									outputs.into_iter().for_each(|(index, output)| {
										mixes_same_amount.1.push(dubp::Source::from_output(
											output,
											tx.hash.clone(),
											index,
										));
									});
								} else {
									mixes_by_amount.insert(
										mix_confirm_store.mix_confirm.output_amount,
										(
											vec![mix_confirm_store],
											outputs
												.into_iter()
												.map(|(index, output)| {
													dubp::Source::from_output(
														output,
														tx.hash.clone(),
														index,
													)
												})
												.collect(),
										),
									);
								}
							}
						}
					}
				}
			}
			Ok(DecodedComment::TxIdListHash(tx_id_list_hash)) => {
				if let Some(raw) = dbs.tx_id_list.get(&tx_id_list_hash.0).unwrap() {
					if let Ok(tx_id_list_store) =
						bincode::deserialize::<db::server::stores::TxIdListStore>(&raw)
					{
						debug!("Found TxIdList from TxIdListHash");

						// List mixes which could be linked with this tx
						let mut mixes =
							BTreeMap::<u64, Vec<db::server::stores::MixConfirmStore>>::new();

						for (input_tx_id, output_tx_id_seed_1) in
							indexes.read().unwrap().mix_confirm_by_input_tx_id.iter()
						{
							if tx_id_list_store.tx_id_list.tx_ids.contains(input_tx_id) {
								if let Some(raw) = dbs.mix_confirm.get(output_tx_id_seed_1).unwrap()
								{
									if let Ok(mix_confirm_store) = bincode::deserialize::<
										db::server::stores::MixConfirmStore,
									>(&raw)
									{
										if tx.written_time
											< mix_confirm_store.mix_confirm.expire_time && tx
											.issuers
											.len()
											== 1 && tx.issuers[0]
											== mix_confirm_store.mix_confirm.issuer
										{
											if let Some(v) = mixes
												.get_mut(&mix_confirm_store.mix_confirm.expire_time)
											{
												v.push(mix_confirm_store);
											} else {
												mixes.insert(
													mix_confirm_store.mix_confirm.expire_time,
													vec![mix_confirm_store],
												);
											}
										}
									}
								}
							}
						}
						debug!("Found {} involved mixes", mixes.len());

						// List interesting outputs of this tx
						let mut outputs: Vec<(u32, &dubp::Output)> = tx
							.outputs
							.iter()
							.zip(0u32..)
							.filter_map(|(output, index)| {
								output.as_ref().and_then(|output| {
									(output.condition == condition).then(|| (index, output))
								})
							})
							.collect();

						// Check which mixes can be fulfilled with these sources
						mixes
							.into_values()
							.flat_map(|v| v.into_iter())
							.map_while(|mix| {
								outputs
									.iter()
									.enumerate()
									.find_map(|(i, (_, output))| {
										(output.amount.to_u64()
											== mix.mix_confirm.input_amount.to_u64())
										.then_some(i)
									})
									.map(|i| {
										let (index, output) = outputs.remove(i);
										(
											mix,
											dubp::Source::from_output(
												output,
												tx.hash.clone(),
												index,
											),
										)
									})
							})
							.for_each(|(mix, source)| {
								if let Some(mixes) =
									mixes_by_amount.get_mut(&mix.mix_confirm.input_amount)
								{
									mixes.0.push(mix);
									mixes.1.push(source);
								} else {
									mixes_by_amount.insert(
										mix.mix_confirm.input_amount,
										(vec![mix], vec![source]),
									);
								}
							});
					}
				} else {
					warn!("Unknown TxIdList");
				}
			}
			Err(e) => {
				debug!("Cannot decode comment: {:?}", e)
			}
		}
	}

	let rclient = reqwest::Client::new();
	'txs2: for (mix_amount, (mut mixes, sources)) in mixes_by_amount.into_iter() {
		if mixes.len() < config.min_txs_per_mix as usize {
			continue;
		}

		// TODO optimize shuffling
		let mut shuffled_mixes = mixes.iter().collect();
		let shuffled_mixes: Vec<&db::server::stores::MixConfirmStore> =
			ShuffleIter::new(&mut shuffled_mixes).collect();

		let tx_id_list = documents::tx_id_list::TxIdList {
			tx_ids: shuffled_mixes
				.iter()
				.map(|&x| TxId::generate(&x.mix_confirm.output_tx_id_seeds))
				.collect(),
		};
		let tx_id_list_hash = tx_id_list.hash();
		let comment = tx_id_list_hash.to_comment();
		dbs.tx_id_list
			.insert(
				tx_id_list_hash.clone(),
				bincode::serialize(&db::server::stores::TxIdListStore {
					tx_id_list,
					time: SystemTime::now()
						.duration_since(UNIX_EPOCH)
						.unwrap()
						.as_secs(),
				})
				.unwrap(),
			)
			.unwrap();
		instant_requests
			.write()
			.unwrap()
			.push_back(InstantRequest::TxIdList(tx_id_list_hash));

		let mut outputs: Vec<dubp::WantedOutput> = shuffled_mixes
			.iter()
			.map(|&x| dubp::WantedOutput {
				amount: mix_amount,
				recipient: x.mix_confirm.recipient,
			})
			.collect();

		let change = sources
			.iter()
			.map(|source| source.get_amount().to_u64())
			.sum::<u64>()
			- mixes.len() as u64 * mix_amount.to_u64();
		if change > 0 {
			outputs.push(dubp::WantedOutput {
				amount: Amount {
					base: 0,
					val: change as u32,
				},
				recipient: keypair.public_key(),
			});
		}

		// TODO optimize shuffling
		let mut shuffled_sources = sources.iter().collect();
		let shuffled_sources: Vec<&dubp::Source> =
			ShuffleIter::new(&mut shuffled_sources).collect();

		let raw_tx = dubp::generate_tx(
			&keypair.generate_signator(),
			&blockstamp,
			&comment,
			&config.protocol.currency,
			&outputs,
			&shuffled_sources,
		);

		let mut sent_times = 0u16;

		for gva_address in OrderedShuffleIter::new(config.network.gva_nodes.iter()) {
			info!("Send tx to {:?}", gva_address);
			if let Err(e) = gva::post_tx(&rclient, gva_address, raw_tx.clone()).await {
				warn!("Tx failed: {:?}", e);
			} else {
				info!("Sent tx");
				sent_times += 1;

				if sent_times == 1 {
					for mix_confirm_store in mixes.iter_mut() {
						indexes.write().unwrap().mix_confirm_by_input_tx_id.remove(
							&TxId::generate(&mix_confirm_store.mix_confirm.input_tx_id_seeds),
						);
						mix_confirm_store.status = db::server::MixStatus::TxSent;
						dbs.mix_confirm
							.insert(
								mix_confirm_store.mix_confirm.output_tx_id_seeds[1].clone(),
								bincode::serialize(mix_confirm_store).unwrap(),
							)
							.unwrap();
					}
				}

				if sent_times >= config.network.nodes_to_send_tx {
					continue 'txs2;
				}
			}
		}

		if sent_times == 0 {
			error!("Tx failed with all GVA nodes!");
		} else {
			warn!(
				"Tx sent to {}/{} GVA nodes",
				sent_times, config.network.nodes_to_send_tx
			);
		}
	}

	dbs.mix_confirm.flush().unwrap();
	dbs.tx_id_list.flush().unwrap();
}

pub async fn ask_tx_id_lists(dbs: &db::server::Dbs, rclient: &reqwest::Client) {
	for raw in dbs.node.iter().values().flatten() {
		if let Ok(node_stores) =
			bincode::deserialize::<HashMap<NodeId, db::server::stores::NodeStore>>(&raw)
		{
			for node_store in node_stores.values() {
				if let Ok(tx_id_lists) =
					client::get_tx_id_lists(rclient, &node_store.node_sig.inner.address).await
				{
					for tx_id_list_store in tx_id_lists {
						let hash = bincode::serialize(&tx_id_list_store.tx_id_list.hash()).unwrap();
						if !dbs.tx_id_list.contains_key(&hash).unwrap() {
							dbs.tx_id_list
								.insert(hash, bincode::serialize(&tx_id_list_store).unwrap())
								.ok();
						}
					}
				}
			}
		}
	}
}
