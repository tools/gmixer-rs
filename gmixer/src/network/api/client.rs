use crate::db;
use gmixer_common::prelude::*;

pub async fn send_mix_confirm_onion_wrapper_secret(
	rclient: &reqwest::Client,
	mix_confirm_onion_wrapper_secret: &documents::mix_confirm::MixConfirmOnionWrapperSecret,
	node_address: &NodeAddress,
) -> Result<(), Error> {
	Ok(bincode::deserialize::<Result<(), ResponseError>>(
		&rclient
			.post(node_address.to_url("/mix-confirm").as_str())
			.body(bincode::serialize(mix_confirm_onion_wrapper_secret)?)
			.header("Content-Type", api::MIME)
			.send()
			.await?
			.bytes()
			.await?,
	)??)
}

pub async fn post_node(
	rclient: &reqwest::Client,
	node_address: &NodeAddress,
	node_sig: &documents::node::NodeSig,
) -> Result<(), Error> {
	Ok(bincode::deserialize::<Result<(), ResponseError>>(
		&rclient
			.post(node_address.to_url("/node").as_str())
			.body(bincode::serialize(node_sig)?)
			.header("Accept", api::MIME)
			.send()
			.await?
			.bytes()
			.await?,
	)??)
}

pub async fn post_tx_id_list(
	rclient: &reqwest::Client,
	node_address: &NodeAddress,
	tx_id_list_stores: &[db::server::stores::TxIdListStore],
) -> Result<(), Error> {
	Ok(bincode::deserialize::<Result<(), ResponseError>>(
		&rclient
			.post(node_address.to_url("/post-tx-id-lists").as_str())
			.body(bincode::serialize(tx_id_list_stores)?)
			.header("Accept", api::MIME)
			.send()
			.await?
			.bytes()
			.await?,
	)??)
}

pub async fn get_tx_id_lists(
	rclient: &reqwest::Client,
	node_address: &NodeAddress,
) -> Result<Vec<db::server::stores::TxIdListStore>, Error> {
	Ok(bincode::deserialize::<
		Vec<db::server::stores::TxIdListStore>,
	>(
		&rclient
			.get(node_address.to_url("/get-tx-id-lists").as_str())
			.header("Accept", api::MIME)
			.send()
			.await?
			.bytes()
			.await?,
	)?)
}
