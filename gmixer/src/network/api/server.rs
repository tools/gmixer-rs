use crate::{common::*, db, server::config::ServerConfig};
use gmixer_common::prelude::*;

use bytes::Bytes;
use dup_crypto::keys::{ed25519::Ed25519KeyPair, KeyPair};
use log::info;
use std::{
	collections::{HashMap, VecDeque},
	ops::DerefMut,
	sync::{Arc, RwLock},
	time::{Duration, SystemTime, UNIX_EPOCH},
};
use warp::Filter;

const REQUEST_MAX_SIZE: u64 = 1048576;
const BLOCKING_INTERVAL: u64 = 1;
const BLOCKING_TIMEOUT: u64 = 30;

pub async fn server_main(
	config: Arc<ServerConfig>,
	dbs: Arc<db::server::Dbs>,
	indexes: Arc<RwLock<db::server::Indexes>>,
	instant_requests: Arc<RwLock<VecDeque<InstantRequest>>>,
	keypair: Arc<Ed25519KeyPair>,
	local_node_sig: Arc<RwLock<documents::node::NodeSig>>,
	pubkey_delegation_sig: Arc<RwLock<Option<documents::pubkey_delegation::PubkeyDelegationSig>>>,
) {
	let route_mix_demand = warp::path("mix-demand")
		.and(warp::post())
		.and(warp::body::content_length_limit(REQUEST_MAX_SIZE))
		.and(warp::body::bytes())
		.map({
			let config = config.clone();
			let dbs = dbs.clone();
			let indexes = indexes.clone();
			let instant_requests = instant_requests.clone();
			let keypair = keypair.clone();
			move |raw: Bytes| {
				bincode::serialize(&serve_mix_demand(
					config.clone(),
					dbs.clone(),
					indexes.clone(),
					instant_requests.clone(),
					keypair.clone(),
					raw,
				))
				.unwrap()
			}
		});

	let route_get_mix_confirm = warp::path("get-mix-confirm")
		.and(warp::post())
		.and(warp::body::bytes())
		.and(warp::body::content_length_limit(REQUEST_MAX_SIZE))
		.map({
			let dbs = dbs.clone();
			let indexes = indexes.clone();
			move |raw: Bytes| {
				bincode::serialize(&serve_get_mix_confirm(dbs.clone(), indexes.clone(), raw))
					.unwrap()
			}
		});

	let route_get_mix_confirm_blocking = warp::path("get-mix-confirm-blocking")
		.and(warp::post())
		.and(warp::body::bytes())
		.and(warp::body::content_length_limit(REQUEST_MAX_SIZE))
		.and_then({
			let dbs = dbs.clone();
			let indexes = indexes.clone();
			move |raw: Bytes| {
				let dbs = dbs.clone();
				let indexes = indexes.clone();
				async move {
					Result::<Vec<u8>, std::convert::Infallible>::Ok(
						bincode::serialize(
							&serve_get_mix_confirm_blocking(dbs, indexes, raw).await,
						)
						.unwrap(),
					)
				}
			}
		});

	let route_mix_confirm = warp::path("mix-confirm")
		.and(warp::post())
		.and(warp::body::bytes())
		.and(warp::body::content_length_limit(REQUEST_MAX_SIZE))
		.map({
			let config = config.clone();
			let dbs = dbs.clone();
			move |raw: Bytes| {
				bincode::serialize(&serve_mix_confirm(
					config.clone(),
					dbs.clone(),
					indexes.clone(),
					instant_requests.clone(),
					keypair.clone(),
					raw,
				))
				.unwrap()
			}
		});

	let route_post_node = warp::path("node")
		.and(warp::post())
		.and(warp::body::bytes())
		.and(warp::body::content_length_limit(REQUEST_MAX_SIZE))
		.map({
			let config = config.clone();
			let dbs = dbs.clone();
			move |raw: Bytes| {
				bincode::serialize(&serve_post_node(config.clone(), dbs.clone(), raw)).unwrap()
			}
		});

	let route_pubkey_delegation = warp::path("pubkey-delegation")
		.and(warp::get())
		.map(move || bincode::serialize(&*pubkey_delegation_sig.read().unwrap()).unwrap());

	let route_get_node = warp::path("node").and(warp::get()).map({
		let local_node_sig = local_node_sig.clone();
		move || bincode::serialize(&*local_node_sig.read().unwrap()).unwrap()
	});

	let route_nodes = warp::path("nodes").and(warp::get()).map({
		let dbs = dbs.clone();
		move || bincode::serialize(&serve_nodes(dbs.clone(), local_node_sig.clone())).unwrap()
	});

	let route_get_tx_id_list = warp::path("get-tx-id-list")
		.and(warp::post())
		.and(warp::body::bytes())
		.and(warp::body::content_length_limit(REQUEST_MAX_SIZE))
		.map({
			let dbs = dbs.clone();
			move |raw: Bytes| bincode::serialize(&serve_get_tx_id_list(dbs.clone(), raw)).unwrap()
		});

	let route_post_tx_id_lists = warp::path("post-tx-id-lists")
		.and(warp::post())
		.and(warp::body::bytes())
		.and(warp::body::content_length_limit(REQUEST_MAX_SIZE))
		.map({
			let config = config.clone();
			let dbs = dbs.clone();
			move |raw: Bytes| {
				bincode::serialize(&serve_post_tx_id_lists(config.clone(), dbs.clone(), raw))
					.unwrap()
			}
		});

	let route_get_tx_id_lists = warp::path("get-tx-id-lists")
		.and(warp::get())
		.map(move || bincode::serialize(&serve_get_tx_id_lists(dbs.clone())).unwrap());

	// CORS sucks
	let route_fucking_cors = warp::options().map(|| {
		warp::reply::with_header(
			warp::reply::with_header(
				warp::reply(),
				"Access-Control-Allow-Headers",
				"Content-type",
			),
			"Access-Control-Allow-Methods",
			"POST, GET, OPTIONS",
		)
	});

	let routes = route_mix_demand
		.or(route_mix_confirm)
		.or(route_post_node)
		.or(route_get_node)
		.or(route_pubkey_delegation)
		.or(route_get_mix_confirm)
		.or(route_get_mix_confirm_blocking)
		.or(route_nodes)
		.or(route_get_tx_id_list)
		.or(route_post_tx_id_lists)
		.or(route_get_tx_id_lists)
		.or(route_fucking_cors)
		.map(|reply| warp::reply::with_header(reply, "Access-Control-Allow-Origin", "*"));

	warp::serve(routes).run(config.listen).await;
}

fn serve_mix_demand(
	config: Arc<ServerConfig>,
	dbs: Arc<db::server::Dbs>,
	indexes: Arc<RwLock<db::server::Indexes>>,
	instant_requests: Arc<RwLock<VecDeque<InstantRequest>>>,
	keypair: Arc<Ed25519KeyPair>,
	raw: Bytes,
) -> Result<(), ResponseError> {
	let mix_demand_store = decode_mix_demand(&keypair, raw)?;
	info!(
		"Mix demand: {} {}->{} {}",
		mix_demand_store.mix_demand.issuer,
		if mix_demand_store.node_address.is_none() {
			"|"
		} else {
			""
		},
		if mix_demand_store.mix_demand.onion.is_none() {
			"|"
		} else {
			""
		},
		mix_demand_store.mix_demand.recipient,
	);
	if mix_demand_store.mix_demand.onion.is_some() {
		dbs.mix_demand.insert(
			mix_demand_store.output_tx_id_seed_1.clone(),
			bincode::serialize(&mix_demand_store).map_err(|_| ResponseError::InternalError)?,
		)?;

		instant_requests
			.write()
			.unwrap()
			.push_back(InstantRequest::MixDemand(
				mix_demand_store.output_tx_id_seed_1,
			));
	} else if let Some(output_tx_id_seed_2) =
		mix_demand_store.mix_demand.output_tx_id_seed_2.clone()
	{
		let mix_confirm_store = generate_mix_confirm(
			&keypair,
			config.protocol.mix_confirm_expire,
			None,
			mix_demand_store,
			output_tx_id_seed_2,
		);

		if mix_confirm_store.node_address.is_none() {
			indexes
				.write()
				.unwrap()
				.mix_confirm_by_input_tx_id_seed_1
				.insert(
					mix_confirm_store.mix_confirm.input_tx_id_seeds[1].clone(),
					mix_confirm_store.mix_confirm.output_tx_id_seeds[1].clone(),
				);
		}

		dbs.mix_confirm.insert(
			mix_confirm_store.mix_confirm.output_tx_id_seeds[1].clone(),
			bincode::serialize(&mix_confirm_store).map_err(|_| ResponseError::InternalError)?,
		)?;

		instant_requests
			.write()
			.unwrap()
			.push_back(InstantRequest::MixConfirm(
				mix_confirm_store.mix_confirm.output_tx_id_seeds[1].clone(),
			));
	}

	Ok(())
}

fn decode_mix_demand(
	keypair: &Ed25519KeyPair,
	raw: Bytes,
) -> Result<db::server::stores::MixDemandStore, ResponseError> {
	let (mix_demand_onion_wrapper, sender_pubkey) =
		documents::mix_demand::MixDemandOnionWrapper::from_mix_demand_onion_wrapper_secret(
			keypair,
			bincode::deserialize(&raw).map_err(|_| ResponseError::DeserializationFailed)?,
		)?;

	let mix_demand = documents::mix_demand::MixDemand::from_mix_demand_onion(
		keypair,
		mix_demand_onion_wrapper.onion,
	)?;

	if mix_demand.issuer != sender_pubkey {
		return Err(ResponseError::ValidationFailed(
			ValidationError::InvalidPublicKey,
		));
	}

	if mix_demand.amount.val == 0 {
		return Err(ResponseError::AmountTooSmall);
	}

	Ok(db::server::stores::MixDemandStore {
		input_tx_id_seed_1: mix_demand_onion_wrapper.tx_id_seed_1,
		mix_demand,
		node_address: mix_demand_onion_wrapper.node_address,
		output_tx_id_seed_1: TxIdSeed::generate(),
		received_time: SystemTime::now()
			.duration_since(UNIX_EPOCH)
			.unwrap()
			.as_secs(),
		sent: false,
	})
}

fn serve_mix_confirm(
	config: Arc<ServerConfig>,
	dbs: Arc<db::server::Dbs>,
	indexes: Arc<RwLock<db::server::Indexes>>,
	instant_requests: Arc<RwLock<VecDeque<InstantRequest>>>,
	keypair: Arc<Ed25519KeyPair>,
	raw: Bytes,
) -> Result<(), ResponseError> {
	let mix_confirm_onion_wrapper_secret = decode_mix_confirm(raw)?;

	let (mix_confirm_onion_wrapper, recipient) =
		mix_confirm_onion_wrapper_secret.decrypt(&keypair)?;

	let mix_demand_store: db::server::stores::MixDemandStore = bincode::deserialize(
		&dbs.mix_demand
			.remove(mix_confirm_onion_wrapper.tx_id_seed_1.clone())?
			.ok_or(ResponseError::DemandNotFound)?,
	)
	.map_err(|_| ResponseError::InternalError)?;

	if !mix_demand_store.sent || recipient != mix_demand_store.mix_demand.recipient {
		return Err(ResponseError::DemandNotFound);
	}

	info!(
		"Mix confirm: {} {}->{} {}",
		mix_demand_store.mix_demand.issuer,
		if mix_demand_store.node_address.is_none() {
			"|"
		} else {
			""
		},
		if mix_demand_store.mix_demand.onion.is_none() {
			"|"
		} else {
			""
		},
		mix_demand_store.mix_demand.recipient,
	);

	let output_tx_id_seed_1 = mix_confirm_onion_wrapper.tx_id_seed_1.clone();
	let mix_confirm_store = generate_mix_confirm(
		&keypair,
		config.protocol.mix_confirm_expire,
		Some(mix_confirm_onion_wrapper.secret),
		mix_demand_store,
		mix_confirm_onion_wrapper.tx_id_seed_2,
	);

	dbs.mix_confirm.insert(
		output_tx_id_seed_1.clone(),
		bincode::serialize(&mix_confirm_store).map_err(|_| ResponseError::InternalError)?,
	)?;

	if mix_confirm_store.node_address.is_none() {
		indexes
			.write()
			.unwrap()
			.mix_confirm_by_input_tx_id_seed_1
			.insert(
				mix_confirm_store.mix_confirm.input_tx_id_seeds[1].clone(),
				output_tx_id_seed_1,
			);
	} else {
		instant_requests
			.write()
			.unwrap()
			.push_back(InstantRequest::MixConfirm(output_tx_id_seed_1));
	}

	Ok(())
}

fn decode_mix_confirm(
	raw: Bytes,
) -> Result<documents::mix_confirm::MixConfirmOnionWrapperSecret, ResponseError> {
	bincode::deserialize(&raw).map_err(|_| ResponseError::DeserializationFailed)
}

fn generate_mix_confirm(
	keypair: &Ed25519KeyPair,
	mix_confirm_expire: u64,
	mix_confirm_onion_secret: Option<documents::mix_confirm::MixConfirmOnionSecret>,
	mix_demand_store: db::server::stores::MixDemandStore,
	output_tx_id_seed_2: TxIdSeed,
) -> db::server::stores::MixConfirmStore {
	let mix_confirm = documents::mix_confirm::MixConfirm {
		currency: mix_demand_store.mix_demand.currency,
		demand_time: mix_demand_store.received_time,
		expire_time: SystemTime::now()
			.duration_since(UNIX_EPOCH)
			.unwrap()
			.as_secs()
			.checked_add(mix_confirm_expire)
			.unwrap(),
		issuer: mix_demand_store.mix_demand.issuer,
		input_amount: mix_demand_store.mix_demand.amount,
		input_tx_id_seeds: [
			mix_demand_store.mix_demand.input_tx_id_seed_0,
			mix_demand_store.input_tx_id_seed_1,
			TxIdSeed::generate(),
		],
		output_amount: mix_demand_store.mix_demand.amount,
		output_tx_id_seeds: [
			mix_demand_store.mix_demand.output_tx_id_seed_0,
			mix_demand_store.output_tx_id_seed_1,
			output_tx_id_seed_2,
		],
		recipient: mix_demand_store.mix_demand.recipient,
	};

	db::server::stores::MixConfirmStore {
		mix_confirm_onion_wrapper_secret:
			documents::mix_confirm::MixConfirmOnionWrapperSecret::from_mix_confirm_sig_and_onion(
				&mix_demand_store.mix_demand.client_pubkey,
				keypair,
				documents::mix_confirm::MixConfirmSig::from_mix_confirm(
					mix_confirm.clone(),
					&keypair.generate_signator(),
				),
				mix_confirm_onion_secret,
			),
		mix_confirm,
		node_address: mix_demand_store.node_address,
		status: db::server::MixStatus::ConfirmNotSent,
	}
}

fn serve_get_mix_confirm(
	dbs: Arc<db::server::Dbs>,
	indexes: Arc<RwLock<db::server::Indexes>>,
	raw: Bytes,
) -> Result<documents::mix_confirm::MixConfirmOnionWrapperSecret, ResponseError> {
	let input_tx_id_seed_1: TxIdSeed =
		bincode::deserialize(&raw).map_err(|_| ResponseError::DeserializationFailed)?;
	let mut mix_confirm_store = bincode::deserialize::<db::server::stores::MixConfirmStore>(
		&dbs.mix_confirm
			.get(
				indexes
					.write()
					.unwrap()
					.deref_mut()
					.mix_confirm_by_input_tx_id_seed_1
					.remove(&input_tx_id_seed_1)
					.ok_or(ResponseError::ConfirmNotFound)?,
			)?
			.ok_or(ResponseError::ConfirmNotFound)?,
	)
	.unwrap();
	mix_confirm_store.status = db::server::MixStatus::ConfirmSent;
	dbs.mix_confirm
		.insert(
			&mix_confirm_store.mix_confirm.output_tx_id_seeds[1],
			bincode::serialize(&mix_confirm_store).unwrap(),
		)
		.unwrap();
	indexes.write().unwrap().mix_confirm_by_input_tx_id.insert(
		TxId::generate(&mix_confirm_store.mix_confirm.input_tx_id_seeds),
		mix_confirm_store.mix_confirm.output_tx_id_seeds[1].clone(),
	);

	info!(
		"Mix confirm: {} |-> {}",
		mix_confirm_store.mix_confirm.issuer, mix_confirm_store.mix_confirm.recipient,
	);

	Ok(mix_confirm_store.mix_confirm_onion_wrapper_secret)
}

async fn serve_get_mix_confirm_blocking(
	dbs: Arc<db::server::Dbs>,
	indexes: Arc<RwLock<db::server::Indexes>>,
	raw: Bytes,
) -> Result<documents::mix_confirm::MixConfirmOnionWrapperSecret, ResponseError> {
	let input_tx_id_seed_1: TxIdSeed =
		bincode::deserialize(&raw).map_err(|_| ResponseError::DeserializationFailed)?;

	let interval = Duration::from_secs(BLOCKING_INTERVAL);
	let start = SystemTime::now();

	while SystemTime::now().duration_since(start).unwrap().as_secs() < BLOCKING_TIMEOUT {
		let resp: Result<documents::mix_confirm::MixConfirmOnionWrapperSecret, ResponseError> = try {
			let mut mix_confirm_store =
				bincode::deserialize::<db::server::stores::MixConfirmStore>(
					&dbs.mix_confirm
						.get(
							indexes
								.write()
								.unwrap()
								.deref_mut()
								.mix_confirm_by_input_tx_id_seed_1
								.remove(&input_tx_id_seed_1)
								.ok_or(ResponseError::ConfirmNotFound)?,
						)?
						.ok_or(ResponseError::ConfirmNotFound)?,
				)
				.unwrap();
			mix_confirm_store.status = db::server::MixStatus::ConfirmSent;
			dbs.mix_confirm
				.insert(
					&mix_confirm_store.mix_confirm.output_tx_id_seeds[1],
					bincode::serialize(&mix_confirm_store).unwrap(),
				)
				.unwrap();
			indexes.write().unwrap().mix_confirm_by_input_tx_id.insert(
				TxId::generate(&mix_confirm_store.mix_confirm.input_tx_id_seeds),
				mix_confirm_store.mix_confirm.output_tx_id_seeds[1].clone(),
			);

			info!(
				"Mix confirm: {} |-> {}",
				mix_confirm_store.mix_confirm.issuer, mix_confirm_store.mix_confirm.recipient,
			);

			mix_confirm_store.mix_confirm_onion_wrapper_secret
		};
		if resp.is_ok() {
			return resp;
		}

		tokio::time::sleep(interval).await;
	}
	Err(ResponseError::ConfirmNotFound)
}

fn serve_nodes(
	dbs: Arc<db::server::Dbs>,
	local_node_sig: Arc<RwLock<documents::node::NodeSig>>,
) -> Vec<documents::node::NodeSig> {
	let mut nodes = vec![(*local_node_sig.read().unwrap()).clone()];
	nodes.append(
		&mut dbs
			.node
			.iter()
			.values()
			.filter_map(|x| {
				if let Ok(raw) = x {
					if let Ok(node_stores) =
						bincode::deserialize::<HashMap<NodeId, db::server::stores::NodeStore>>(&raw)
					{
						Some(node_stores.into_iter().map(|(_, v)| v.node_sig))
					} else {
						None
					}
				} else {
					None
				}
			})
			.flatten()
			.collect(),
	);

	nodes
}

fn serve_post_node(
	config: Arc<ServerConfig>,
	dbs: Arc<db::server::Dbs>,
	raw: Bytes,
) -> Result<(), ResponseError> {
	let new_node_sig: documents::node::NodeSig =
		bincode::deserialize(&raw).map_err(|_| ResponseError::DeserializationFailed)?;

	if new_node_sig.inner.id == config.node_id {
		return Ok(());
	}
	new_node_sig.verify(&config.protocol)?;

	let mut node_stores = if let Some(raw) = dbs.node.get(new_node_sig.inner.pubkey).unwrap() {
		let node_stores: HashMap<NodeId, db::server::stores::NodeStore> =
			bincode::deserialize(&raw).map_err(|_| ResponseError::InternalError)?;
		if let Some(node_store) = node_stores.get(&new_node_sig.inner.id) {
			if node_store.node_sig.inner.sigtime >= new_node_sig.inner.sigtime {
				return Ok(());
			}
		}
		node_stores
	} else {
		HashMap::new()
	};
	info!(
		"Added NodeSig {}@{}",
		new_node_sig.inner.id.to_string(),
		new_node_sig.inner.address.to_string()
	);
	let node_pubkey = new_node_sig.inner.pubkey;
	node_stores.insert(
		new_node_sig.inner.id.clone(),
		db::server::stores::NodeStore {
			node_sig: new_node_sig,
			received_time: SystemTime::now()
				.duration_since(UNIX_EPOCH)
				.unwrap()
				.as_secs(),
		},
	);
	dbs.node
		.insert(
			node_pubkey,
			bincode::serialize(&node_stores).map_err(|_| ResponseError::InternalError)?,
		)
		.unwrap();

	Ok(())
}

fn serve_get_tx_id_list(
	dbs: Arc<db::server::Dbs>,
	raw: Bytes,
) -> Result<documents::tx_id_list::TxIdList, ResponseError> {
	let tx_id_list_hash: TxIdListHash =
		bincode::deserialize(&raw).map_err(|_| ResponseError::DeserializationFailed)?;

	Ok(bincode::deserialize::<db::server::stores::TxIdListStore>(
		&dbs.tx_id_list
			.get(&tx_id_list_hash)
			.unwrap()
			.ok_or(ResponseError::UnknownTxIdListHash)?,
	)
	.map_err(|_| ResponseError::InternalError)?
	.tx_id_list)
}

fn serve_post_tx_id_lists(
	config: Arc<ServerConfig>,
	dbs: Arc<db::server::Dbs>,
	raw: Bytes,
) -> Result<(), ResponseError> {
	let tx_id_list_stores: Vec<db::server::stores::TxIdListStore> =
		bincode::deserialize(&raw).map_err(|_| ResponseError::DeserializationFailed)?;

	let t = SystemTime::now()
		.duration_since(UNIX_EPOCH)
		.unwrap()
		.as_secs()
		.checked_sub(config.tx_id_list_expire)
		.ok_or(ResponseError::InternalError)?;

	for tx_id_list_store in tx_id_list_stores {
		let tx_id_list_hash = tx_id_list_store.tx_id_list.hash();
		if tx_id_list_store.time > t && !dbs.tx_id_list.contains_key(&tx_id_list_hash).unwrap() {
			info!("Added tx id list");
			dbs.tx_id_list
				.insert(
					tx_id_list_hash,
					bincode::serialize(&tx_id_list_store).unwrap(),
				)
				.unwrap();
		}
	}
	dbs.tx_id_list.flush().unwrap();

	Ok(())
}

fn serve_get_tx_id_lists(dbs: Arc<db::server::Dbs>) -> Vec<db::server::stores::TxIdListStore> {
	dbs.tx_id_list
		.iter()
		.values()
		.filter_map(|v| bincode::deserialize::<db::server::stores::TxIdListStore>(&v.unwrap()).ok())
		.collect()
}

#[cfg(test)]
mod tests {
	use super::*;
	use crate::common::tests::random_pubkey;
	use db::server::MixStatus;
	use dup_crypto::keys::KeyPair;
	use std::convert::TryFrom;

	#[test]
	fn test_correct_mix_demand() {
		let client_keypair = Ed25519KeyPair::generate_random().unwrap();
		let node1_keypair = Ed25519KeyPair::generate_random().unwrap();
		let node1_pubkey = node1_keypair.public_key();
		let node2_keypair = Ed25519KeyPair::generate_random().unwrap();
		let node2_pubkey = node2_keypair.public_key();

		let mix_demand = documents::mix_demand::MixDemand {
			amount: Amount { val: 123, base: 0 },
			client_pubkey: client_keypair.public_key(),
			currency: String::from("foo"),
			input_tx_id_seed_0: TxIdSeed::generate(),
			issuer: node1_pubkey,
			onion: None,
			output_tx_id_seed_0: TxIdSeed::generate(),
			output_tx_id_seed_2: Some(TxIdSeed::generate()),
			recipient: random_pubkey(),
		};
		let mix_demand_onion = documents::mix_demand::MixDemandOnion::from_mix_demand(
			&client_keypair,
			&mix_demand,
			&node2_pubkey,
		);
		let mix_demand_onion_wrapper = documents::mix_demand::MixDemandOnionWrapper {
			tx_id_seed_1: TxIdSeed::generate(),
			node_address: None,
			onion: mix_demand_onion,
		};
		let mix_demand_onion_wrapper_secret =
			documents::mix_demand::MixDemandOnionWrapperSecret::from_mix_demand_onion_wrapper(
				&node1_keypair,
				mix_demand_onion_wrapper,
				&node2_pubkey,
			);
		let mix_demand_store = decode_mix_demand(
			&node2_keypair,
			Bytes::from(bincode::serialize(&mix_demand_onion_wrapper_secret).unwrap()),
		)
		.unwrap();

		assert_eq!(mix_demand, mix_demand_store.mix_demand);
	}

	#[test]
	fn test_correct_mix_confirm() -> Result<(), ResponseError> {
		let client_keypair = Ed25519KeyPair::generate_random().unwrap();
		let node1_keypair = Ed25519KeyPair::generate_random().unwrap();
		let node2_keypair = Ed25519KeyPair::generate_random().unwrap();

		let mix_demand = documents::mix_demand::MixDemand {
			amount: Amount { val: 123, base: 0 },
			client_pubkey: client_keypair.public_key(),
			currency: String::from("foo"),
			input_tx_id_seed_0: TxIdSeed::generate(),
			issuer: client_keypair.public_key(),
			onion: Some(documents::mix_demand::MixDemandOnion(vec![42])),
			output_tx_id_seed_0: TxIdSeed::generate(),
			output_tx_id_seed_2: None,
			recipient: node2_keypair.public_key(),
		};
		let output_tx_id_seed_1 = TxIdSeed::generate();
		let mix_demand_store = db::server::stores::MixDemandStore {
			input_tx_id_seed_1: TxIdSeed::generate(),
			mix_demand,
			node_address: None,
			output_tx_id_seed_1: output_tx_id_seed_1.clone(),
			received_time: SystemTime::now()
				.duration_since(UNIX_EPOCH)
				.unwrap()
				.as_secs(),
			sent: false,
		};

		generate_mix_confirm(
			&node1_keypair,
			3600,
			Some(documents::mix_confirm::MixConfirmOnionSecret(vec![42])),
			mix_demand_store,
			TxIdSeed::generate(),
		);
		Ok(())
	}

	#[test]
	fn test_serve_correct_mix_demand_end() {
		let client_keypair = Ed25519KeyPair::generate_random().unwrap();
		let node1_keypair = Ed25519KeyPair::generate_random().unwrap();
		let node1_pubkey = node1_keypair.public_key();
		let node2_keypair = Ed25519KeyPair::generate_random().unwrap();
		let node2_pubkey = node2_keypair.public_key();

		let mix_demand = documents::mix_demand::MixDemand {
			amount: Amount { val: 123, base: 0 },
			client_pubkey: client_keypair.public_key(),
			currency: String::from("foo"),
			input_tx_id_seed_0: TxIdSeed::generate(),
			issuer: node1_pubkey,
			onion: None,
			output_tx_id_seed_0: TxIdSeed::generate(),
			output_tx_id_seed_2: Some(TxIdSeed::generate()),
			recipient: random_pubkey(),
		};
		let mix_demand_onion = documents::mix_demand::MixDemandOnion::from_mix_demand(
			&client_keypair,
			&mix_demand,
			&node2_pubkey,
		);
		let mix_demand_onion_wrapper = documents::mix_demand::MixDemandOnionWrapper {
			tx_id_seed_1: TxIdSeed::generate(),
			node_address: None,
			onion: mix_demand_onion,
		};
		let mix_demand_onion_wrapper_secret =
			documents::mix_demand::MixDemandOnionWrapperSecret::from_mix_demand_onion_wrapper(
				&node1_keypair,
				mix_demand_onion_wrapper,
				&node2_pubkey,
			);
		let raw = Bytes::from(bincode::serialize(&mix_demand_onion_wrapper_secret).unwrap());

		let dbs = Arc::new(db::server::load_dbs_tmp().unwrap());
		let instant_requests = Arc::new(RwLock::new(VecDeque::new()));

		serve_mix_demand(
			Arc::new(ServerConfig::default()),
			dbs.clone(),
			Arc::new(RwLock::new(db::server::load_indexes(&dbs.mix_confirm))),
			instant_requests.clone(),
			Arc::new(node2_keypair),
			raw,
		)
		.unwrap();

		assert!(dbs.mix_demand.is_empty());
		let mut iter = dbs.mix_confirm.iter();
		let (output_tx_id_seed_1, mix_confirm_store) = iter.next().unwrap().unwrap();
		let output_tx_id_seed_1 = TxIdSeed::try_from(output_tx_id_seed_1.as_ref()).unwrap();
		let mix_confirm_store: db::server::stores::MixConfirmStore =
			bincode::deserialize(&mix_confirm_store).unwrap();
		assert!(iter.next().is_none());

		assert_eq!(
			output_tx_id_seed_1,
			mix_confirm_store.mix_confirm.output_tx_id_seeds[1]
		);
		assert_eq!(
			mix_demand.input_tx_id_seed_0,
			mix_confirm_store.mix_confirm.input_tx_id_seeds[0]
		);
		assert_eq!(
			mix_demand.output_tx_id_seed_0,
			mix_confirm_store.mix_confirm.output_tx_id_seeds[0]
		);
		assert_eq!(
			mix_demand.output_tx_id_seed_2.unwrap(),
			mix_confirm_store.mix_confirm.output_tx_id_seeds[2]
		);
		assert_eq!(
			mix_demand.amount,
			mix_confirm_store.mix_confirm.input_amount
		);
		assert_eq!(
			mix_demand.amount,
			mix_confirm_store.mix_confirm.output_amount
		);
		assert_eq!(mix_demand.issuer, mix_confirm_store.mix_confirm.issuer);
		assert_eq!(
			mix_demand.recipient,
			mix_confirm_store.mix_confirm.recipient
		);
		assert_eq!(mix_demand.currency, mix_confirm_store.mix_confirm.currency);
		assert_eq!(mix_confirm_store.status, MixStatus::ConfirmNotSent);
		assert!(mix_confirm_store.node_address.is_none());
		assert_eq!(
			instant_requests.write().unwrap().pop_front().unwrap(),
			InstantRequest::MixConfirm(output_tx_id_seed_1),
		);
		assert!(instant_requests.read().unwrap().is_empty());
	}

	#[test]
	fn test_serve_correct_mix_demand_middle() {
		let client_keypair = Ed25519KeyPair::generate_random().unwrap();
		let node1_keypair = Ed25519KeyPair::generate_random().unwrap();
		let node1_pubkey = node1_keypair.public_key();
		let node2_keypair = Ed25519KeyPair::generate_random().unwrap();
		let node2_pubkey = node2_keypair.public_key();

		let mix_demand = documents::mix_demand::MixDemand {
			amount: Amount { val: 123, base: 0 },
			client_pubkey: client_keypair.public_key(),
			currency: String::from("foo"),
			input_tx_id_seed_0: TxIdSeed::generate(),
			issuer: node1_pubkey,
			onion: Some(documents::mix_demand::MixDemandOnion(vec![42])),
			output_tx_id_seed_0: TxIdSeed::generate(),
			output_tx_id_seed_2: None,
			recipient: random_pubkey(),
		};
		let mix_demand_onion = documents::mix_demand::MixDemandOnion::from_mix_demand(
			&client_keypair,
			&mix_demand,
			&node2_pubkey,
		);
		let tx_id_seed_1 = TxIdSeed::generate();
		let mix_demand_onion_wrapper = documents::mix_demand::MixDemandOnionWrapper {
			tx_id_seed_1: tx_id_seed_1.clone(),
			node_address: None,
			onion: mix_demand_onion,
		};
		let mix_demand_onion_wrapper_secret =
			documents::mix_demand::MixDemandOnionWrapperSecret::from_mix_demand_onion_wrapper(
				&node1_keypair,
				mix_demand_onion_wrapper,
				&node2_pubkey,
			);
		let raw = Bytes::from(bincode::serialize(&mix_demand_onion_wrapper_secret).unwrap());

		let dbs = Arc::new(db::server::load_dbs_tmp().unwrap());
		let instant_requests = Arc::new(RwLock::new(VecDeque::new()));

		serve_mix_demand(
			Arc::new(ServerConfig::default()),
			dbs.clone(),
			Arc::new(RwLock::new(db::server::load_indexes(&dbs.mix_confirm))),
			instant_requests.clone(),
			Arc::new(node2_keypair),
			raw,
		)
		.unwrap();

		assert!(dbs.mix_confirm.is_empty());
		let mut iter = dbs.mix_demand.iter();
		let (output_tx_id_seed_1, mix_demand_store) = iter.next().unwrap().unwrap();
		let output_tx_id_seed_1 = TxIdSeed::try_from(output_tx_id_seed_1.as_ref()).unwrap();
		let mix_demand_store: db::server::stores::MixDemandStore =
			bincode::deserialize(&mix_demand_store).unwrap();
		assert!(iter.next().is_none());

		assert_eq!(tx_id_seed_1, mix_demand_store.input_tx_id_seed_1);
		assert_eq!(output_tx_id_seed_1, mix_demand_store.output_tx_id_seed_1);
		assert!(!mix_demand_store.sent);
		assert!(mix_demand_store.node_address.is_none());
		assert_eq!(
			instant_requests.write().unwrap().pop_front().unwrap(),
			InstantRequest::MixDemand(output_tx_id_seed_1),
		);
		assert!(instant_requests.read().unwrap().is_empty());
	}

	#[test]
	fn test_serve_correct_mix_confirm() {
		let node1_keypair = Ed25519KeyPair::generate_random().unwrap();
		let node1_pubkey = node1_keypair.public_key();
		let node2_keypair = Ed25519KeyPair::generate_random().unwrap();
		let node2_pubkey = node2_keypair.public_key();
		let tx_id_seeds = [
			TxIdSeed::generate(),
			TxIdSeed::generate(),
			TxIdSeed::generate(),
		];
		let amount = Amount { base: 0, val: 100 };

		let dbs = Arc::new(db::server::load_dbs_tmp().unwrap());
		let indexes = Arc::new(RwLock::new(db::server::load_indexes(&dbs.mix_confirm)));
		let instant_requests = Arc::new(RwLock::new(VecDeque::new()));

		dbs.mix_demand
			.insert(
				tx_id_seeds[1].clone(),
				bincode::serialize(&db::server::stores::MixDemandStore {
					input_tx_id_seed_1: TxIdSeed::generate(),
					mix_demand: documents::mix_demand::MixDemand {
						amount: amount.clone(),
						client_pubkey: random_pubkey(),
						currency: String::from("g1"),
						input_tx_id_seed_0: TxIdSeed::generate(),
						issuer: random_pubkey(),
						onion: Some(documents::mix_demand::MixDemandOnion(vec![42])),
						output_tx_id_seed_0: tx_id_seeds[0].clone(),
						output_tx_id_seed_2: None,
						recipient: node2_pubkey,
					},
					node_address: None,
					output_tx_id_seed_1: tx_id_seeds[1].clone(),
					received_time: 0,
					sent: true,
				})
				.unwrap(),
			)
			.unwrap();

		let raw = bincode::serialize(
			&documents::mix_confirm::MixConfirmOnionWrapperSecret::from_mix_confirm_sig_and_onion(
				&random_pubkey(),
				&node2_keypair,
				documents::mix_confirm::MixConfirmSig::from_mix_confirm(
					documents::mix_confirm::MixConfirm {
						currency: String::from("g1"),
						demand_time: 0,
						expire_time: 1000,
						issuer: node1_pubkey,
						input_amount: amount.clone(),
						input_tx_id_seeds: tx_id_seeds.clone(),
						output_amount: amount,
						output_tx_id_seeds: [
							TxIdSeed::generate(),
							TxIdSeed::generate(),
							TxIdSeed::generate(),
						],
						recipient: random_pubkey(),
					},
					&node2_keypair.generate_signator(),
				),
				None,
			),
		)
		.unwrap();

		serve_mix_confirm(
			Arc::new(ServerConfig::default()),
			dbs.clone(),
			indexes.clone(),
			instant_requests,
			Arc::new(node1_keypair),
			Bytes::from(raw),
		)
		.unwrap();

		let indexes = indexes.write().unwrap();
		assert_eq!(dbs.mix_demand.len(), 0);
		assert_eq!(dbs.mix_confirm.len(), 1);
		assert_eq!(indexes.mix_confirm_by_input_tx_id.len(), 0);
		assert_eq!(indexes.mix_confirm_by_input_tx_id_seed_1.len(), 1);

		let mix_confirm_store: db::server::stores::MixConfirmStore =
			bincode::deserialize(&dbs.mix_confirm.get(&tx_id_seeds[1]).unwrap().unwrap()).unwrap();
		assert_eq!(
			indexes
				.mix_confirm_by_input_tx_id_seed_1
				.get(&mix_confirm_store.mix_confirm.input_tx_id_seeds[1])
				.unwrap(),
			&tx_id_seeds[1]
		);
		assert_eq!(mix_confirm_store.status, MixStatus::ConfirmNotSent);
		assert_eq!(
			mix_confirm_store.mix_confirm.output_tx_id_seeds,
			tx_id_seeds
		);
		assert_eq!(mix_confirm_store.mix_confirm.recipient, node2_pubkey);
	}
}
