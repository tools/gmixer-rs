#![feature(bool_to_option)]
#![feature(try_blocks)]

mod client;
mod common;
mod db;
mod network;
mod server;

use clap::Parser;
use common::*;
use dup_crypto::keys::KeyPair;

fn main() {
	env_logger::init();
	let opt = MainOpt::parse();

	match &opt.cmd {
		MainSubcommand::Client(ClientSubcommand::Confirm(subopt)) => {
			client::commands::confirm::confirm_cmd(&opt, subopt);
		}

		MainSubcommand::Client(ClientSubcommand::Init) => {
			client::init_config(&opt.dir.0).expect("Error: cannot init client config");
		}

		MainSubcommand::Client(ClientSubcommand::List) => {
			client::commands::list_mixes::list_cmd(&opt);
		}

		MainSubcommand::Client(ClientSubcommand::Mix(subopt)) => {
			client::commands::mix::mix_cmd(&opt, subopt);
		}

		MainSubcommand::Client(ClientSubcommand::NodeInfo(subopt)) => {
			client::commands::node_info::node_info_cmd(&opt, subopt);
		}

		MainSubcommand::Client(ClientSubcommand::SyncNodes(subopt)) => {
			client::commands::sync_nodes::sync_nodes_cmd(&opt, subopt);
		}

		MainSubcommand::Client(ClientSubcommand::Track) => {
			client::commands::track::track_cmd(&opt);
		}

		MainSubcommand::Client(ClientSubcommand::Tx) => {
			client::commands::tx::tx_cmd(&opt);
		}

		MainSubcommand::Server(ServerSubcommand::Init) => {
			server::init_config(&opt.dir.0).expect("Error: cannot init server config");
		}

		MainSubcommand::Server(ServerSubcommand::PubkeyDelegation) => {
			let config_file = server::read_config_file(
				opt.dir
					.0
					.join(server::SERVER_DIR)
					.join(server::CONFIG_FILE)
					.as_path(),
			)
			.expect("Error: cannot read server config file");
			server::init_pubkey_delegation_sig(
				&opt.dir
					.0
					.join(server::SERVER_DIR)
					.join(server::PUBKEY_DELEGATION_SIG_FILE),
				&read_seed(
					opt.dir
						.0
						.join(server::SERVER_DIR)
						.join(server::SEED_FILE)
						.as_path(),
				)
				.public_key(),
				config_file.config.protocol.currency,
			)
			.expect("Error: cannot update pubkey delegation signature");
		}

		MainSubcommand::Server(ServerSubcommand::Recover(subopt)) => {
			server::recover_cmd(&opt, subopt)
		}

		MainSubcommand::Server(ServerSubcommand::Start) => server::start(&opt),

		MainSubcommand::Server(ServerSubcommand::SyncNodes(subopt)) => {
			server::sync_nodes_cmd(&opt, subopt);
		}

		_ => todo!(),
	}
}
