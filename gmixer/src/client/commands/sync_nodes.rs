use super::super::{config::ClientConfig, DB_DIR};
use crate::{common::*, db};
use gmixer_common::prelude::*;

use std::{
	collections::HashMap,
	time::{SystemTime, UNIX_EPOCH},
};

#[tokio::main]
pub async fn sync_nodes_cmd(opt: &MainOpt, subopt: &SyncNodesSubcommand) {
	let config = ClientConfig::default();
	let dbs = db::client::load(opt.dir.0.join(DB_DIR).as_path())
		.expect("Error: cannot load client database");

	let nodes = api::get_nodes(&reqwest::Client::new(), &subopt.node_address)
		.await
		.unwrap();
	let t = SystemTime::now()
		.duration_since(UNIX_EPOCH)
		.unwrap()
		.as_secs();
	for node_sig in nodes {
		if node_sig.verify(&config.protocol).is_ok() {
			if let Some(entry) = dbs.node.get(node_sig.inner.pubkey).unwrap() {
				if let Ok(mut node_stores) =
					bincode::deserialize::<HashMap<NodeId, db::server::stores::NodeStore>>(&entry)
				{
					if let Some(node_store) = node_stores.get(&node_sig.inner.id) {
						if node_store.node_sig.inner.sigtime >= node_sig.inner.sigtime {
							continue;
						}
					}
					println!(
						"Added NodeSig {}@{}",
						node_sig.inner.id.to_string(),
						node_sig.inner.address.to_string()
					);
					let pubkey = node_sig.inner.pubkey;
					node_stores.insert(
						node_sig.inner.id.clone(),
						db::server::stores::NodeStore {
							node_sig,
							received_time: t,
						},
					);
					dbs.node
						.insert(pubkey, bincode::serialize(&node_stores).unwrap())
						.unwrap();
					continue;
				}
			}
			println!(
				"Added NodeSig {}@{}",
				node_sig.inner.id.to_string(),
				node_sig.inner.address.to_string()
			);
			let pubkey = node_sig.inner.pubkey;
			let mut node_stores = HashMap::new();
			node_stores.insert(
				node_sig.inner.id.clone(),
				db::server::stores::NodeStore {
					node_sig,
					received_time: t,
				},
			);
			dbs.node
				.insert(pubkey, bincode::serialize(&node_stores).unwrap())
				.unwrap();
		}
	}
	db::client::clean_dbs(&config, &dbs);
}
