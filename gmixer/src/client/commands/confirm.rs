use crate::{common::*, db, db::client::MixStatus};
use gmixer_common::prelude::*;

use super::super::DB_DIR;

use dup_crypto::keys::ed25519::{Ed25519KeyPair, KeyPairFromSeed32Generator, PublicKey};
use itertools::izip;

#[tokio::main]
pub async fn confirm_cmd(opt: &MainOpt, _subopt: &ClientConfirmSubcommand) {
	//let _config = ClientConfig::default();
	let dbs = db::client::load(opt.dir.0.join(DB_DIR).as_path())
		.expect("Error: cannot load client database");

	let client_keypair = prompt_keypair();

	let rclient = reqwest::Client::new();
	for mut pending_mix in dbs
		.pending_mixes
		.iter()
		.values()
		.filter_map(|x| bincode::deserialize::<db::client::stores::PendingMixStore>(&x.ok()?).ok())
		.filter(|x| x.status == MixStatus::WaitingForConfirm)
	{
		if let Some(node) = pending_mix.node.as_ref() {
			if let Ok(mix_confirm_onion_wrapper_secret) =
				api::get_mix_confirm(&rclient, &node.address, &pending_mix.output_tx_id_seed_1)
					.await
			{
				if let Ok((mix_confirm_onion_wrapper, node_pubkey)) =
					mix_confirm_onion_wrapper_secret.decrypt(&client_keypair)
				{
					if node_pubkey != node.pubkey
						|| mix_confirm_onion_wrapper.tx_id_seed_1 != pending_mix.output_tx_id_seed_1
					{
						println!("invalid");
						continue;
					}
					if let Ok(mix_confirm_sigs) = check_mix_confirms(
						pending_mix
							.ephemeral_keypairs_seeds
							.iter()
							.map(|x| KeyPairFromSeed32Generator::generate(x.clone()))
							.collect(),
						mix_confirm_onion_wrapper.secret,
						&pending_mix.mix_demands,
						pending_mix.output_tx_id_seed_1.clone(),
						&pending_mix.path,
					) {
						if mix_confirm_onion_wrapper.tx_id_seed_2
							!= mix_confirm_sigs.get(0).unwrap().inner.input_tx_id_seeds[2]
						{
							println!("invalid oc2");
							continue;
						}

						pending_mix.mix_confirms = mix_confirm_sigs;
						pending_mix.status = MixStatus::Confirmed;
						println!("confirmed");

						dbs.pending_mixes
							.insert(
								pending_mix
									.mix_demands
									.get(0)
									.unwrap()
									.input_tx_id_seed_0
									.clone(),
								bincode::serialize(&pending_mix).unwrap(),
							)
							.unwrap();
					} else {
						println!("invalid");
					}
				}
			}
		}
	}
}

fn check_mix_confirms(
	ephemeral_keypairs: Vec<Ed25519KeyPair>,
	mix_confirm_onion_secret: documents::mix_confirm::MixConfirmOnionSecret,
	mix_demands: &[documents::mix_demand::MixDemand],
	output_tx_id_seed_1: TxIdSeed,
	path: &[PublicKey],
) -> Result<Vec<documents::mix_confirm::MixConfirmSig>, Error> {
	let mut mix_confirm_sigs = Vec::<documents::mix_confirm::MixConfirmSig>::new();
	let mut mix_confirm_onion_secret_opt = Some(mix_confirm_onion_secret);

	let mut prev_ocs = None;

	for (ephemeral_keypair, expected_node_pubkey, mix_demand) in
		izip!(ephemeral_keypairs, path, mix_demands)
	{
		if let Some(Ok((mix_confirm_onion, node_pubkey))) = mix_confirm_onion_secret_opt
			.take()
			.map(|x| x.decrypt(&ephemeral_keypair))
		{
			if node_pubkey != *expected_node_pubkey
				|| mix_confirm_onion
					.mix_confirm_sig
					.verify_expected(expected_node_pubkey)
					.is_err() || mix_confirm_onion.mix_confirm_sig.inner.currency != mix_demand.currency
				|| mix_confirm_onion.mix_confirm_sig.inner.input_amount != mix_demand.amount
				|| mix_confirm_onion.mix_confirm_sig.inner.output_amount != mix_demand.amount
				|| mix_confirm_onion.mix_confirm_sig.inner.recipient != mix_demand.recipient
				|| mix_confirm_onion.mix_confirm_sig.inner.issuer != mix_demand.issuer
				|| mix_confirm_onion.mix_confirm_sig.inner.input_tx_id_seeds[0]
					!= mix_demand.input_tx_id_seed_0
				|| mix_confirm_onion.mix_confirm_sig.inner.output_tx_id_seeds[0]
					!= mix_demand.output_tx_id_seed_0
				|| mix_demand.output_tx_id_seed_2.clone().map_or(false, |x| {
					mix_confirm_onion.mix_confirm_sig.inner.output_tx_id_seeds[2] != x
				}) || (mix_confirm_sigs.is_empty()
				&& mix_confirm_onion.mix_confirm_sig.inner.input_tx_id_seeds[1]
					!= output_tx_id_seed_1)
				|| prev_ocs.map_or(false, |x| {
					mix_confirm_onion.mix_confirm_sig.inner.input_tx_id_seeds != x
				}) {
				println!("Error: Invalid pubkey");
				break;
			}

			if let Some(mix_confirm_onion_secret) = mix_confirm_onion.onion {
				mix_confirm_onion_secret_opt = Some(mix_confirm_onion_secret);
				prev_ocs = Some(
					mix_confirm_onion
						.mix_confirm_sig
						.inner
						.output_tx_id_seeds
						.clone(),
				);
				mix_confirm_sigs.push(mix_confirm_onion.mix_confirm_sig);
			} else {
				if mix_demand.onion.is_none() {
					mix_confirm_sigs.push(mix_confirm_onion.mix_confirm_sig);
					return Ok(mix_confirm_sigs);
				}
				return Err(Error::InvalidConfirms);
			}
		} else {
			return Err(Error::InvalidConfirms);
		}
	}
	Err(Error::InvalidConfirms)
}

#[cfg(test)]
mod tests {
	use super::*;
	use crate::tests::random_pubkey;
	use dup_crypto::{bases::b58::ToBase58, keys::KeyPair, seeds::Seed32};

	#[test]
	fn test_check_mix_confirms() {
		let nodes_keypairs: Vec<Ed25519KeyPair> = (0..3)
			.map(|_| Ed25519KeyPair::generate_random().unwrap())
			.collect();
		let nodes_pubkeys: Vec<PublicKey> = nodes_keypairs.iter().map(|x| x.public_key()).collect();

		let client_keypair = Ed25519KeyPair::generate_random().unwrap();
		let client_pubkey = client_keypair.public_key();
		let amount = Amount { val: 100, base: 0 };
		let recipient = random_pubkey();
		let ephemeral_keypairs_seeds: Vec<Seed32> =
			(0..3).map(|_| Seed32::random().unwrap()).collect();
		let ephemeral_keypairs: Vec<Ed25519KeyPair> = ephemeral_keypairs_seeds
			.iter()
			.map(|x| KeyPairFromSeed32Generator::generate(x.clone()))
			.collect();
		let ephemeral_pubkeys: Vec<PublicKey> =
			ephemeral_keypairs.iter().map(|x| x.public_key()).collect();
		let currency = String::from("g1");

		let (mix_demands, mix_demand_onion) = documents::mix_demand::gen_mix_demands(
			&amount,
			&client_keypair,
			&currency,
			&ephemeral_keypairs,
			&nodes_pubkeys,
			&recipient,
		);

		let pending_mix = db::client::stores::PendingMixStore {
			ephemeral_keypairs_seeds,
			mix_confirms: vec![],
			mix_demand_onion,
			mix_demands,
			node: Some(documents::node::Node {
				address: NodeAddress {
					addr: Addr::Hostname(String::from("localhost")),
					port: 10953,
					protocol: NetworkProtocol::Http,
				},
				currency: currency.clone(),
				id: NodeId::generate(),
				protocol_version: PROTOCOL_VERSION,
				pubkey: nodes_pubkeys.get(0).unwrap().clone(),
				pubkey_delegation_sig: None,
				sigtime: 0,
				software_version: String::from(SOFTWARE_VERSION),
			}),
			output_tx_id_seed_1: TxIdSeed::generate(),
			path: nodes_pubkeys.clone(),
			status: MixStatus::WaitingForConfirm,
		};

		let mix_confirm0 = documents::mix_confirm::MixConfirm {
			currency: currency.clone(),
			demand_time: 0,
			expire_time: 1000,
			issuer: client_pubkey.clone(),
			input_amount: amount.clone(),
			input_tx_id_seeds: [
				pending_mix
					.mix_demands
					.get(0)
					.unwrap()
					.input_tx_id_seed_0
					.clone(),
				pending_mix.output_tx_id_seed_1.clone(),
				TxIdSeed::generate(),
			],
			output_amount: amount.clone(),
			output_tx_id_seeds: [
				pending_mix
					.mix_demands
					.get(0)
					.unwrap()
					.output_tx_id_seed_0
					.clone(),
				TxIdSeed::generate(),
				TxIdSeed::generate(),
			],
			recipient: nodes_pubkeys.get(1).unwrap().clone(),
		};
		let mix_confirm1 = documents::mix_confirm::MixConfirm {
			currency: currency.clone(),
			demand_time: 0,
			expire_time: 1000,
			issuer: nodes_pubkeys.get(0).unwrap().clone(),
			input_amount: amount.clone(),
			input_tx_id_seeds: mix_confirm0.output_tx_id_seeds.clone(),
			output_amount: amount.clone(),
			output_tx_id_seeds: [
				pending_mix
					.mix_demands
					.get(1)
					.unwrap()
					.output_tx_id_seed_0
					.clone(),
				TxIdSeed::generate(),
				TxIdSeed::generate(),
			],
			recipient: nodes_pubkeys.get(2).unwrap().clone(),
		};
		let mix_confirm2 = documents::mix_confirm::MixConfirm {
			currency: currency.clone(),
			demand_time: 0,
			expire_time: 1000,
			issuer: nodes_pubkeys.get(1).unwrap().clone(),
			input_amount: amount.clone(),
			input_tx_id_seeds: mix_confirm1.output_tx_id_seeds.clone(),
			output_amount: amount.clone(),
			output_tx_id_seeds: [
				pending_mix
					.mix_demands
					.get(2)
					.unwrap()
					.output_tx_id_seed_0
					.clone(),
				TxIdSeed::generate(),
				pending_mix
					.mix_demands
					.get(2)
					.unwrap()
					.output_tx_id_seed_2
					.clone()
					.unwrap(),
			],
			recipient,
		};

		let mix_confirm_sigs: Vec<documents::mix_confirm::MixConfirmSig> =
			vec![mix_confirm0, mix_confirm1, mix_confirm2]
				.into_iter()
				.enumerate()
				.map(|(i, mix_confirm)| {
					documents::mix_confirm::MixConfirmSig::from_mix_confirm(
						mix_confirm,
						&nodes_keypairs.get(i).unwrap().generate_signator(),
					)
				})
				.collect();

		nodes_pubkeys
			.iter()
			.for_each(|x| println!("{}", x.to_base58()));

		let mix_confirm_onion_secret =
			izip!(mix_confirm_sigs.clone(), nodes_keypairs, ephemeral_pubkeys)
				.rev()
				.fold(
					None,
					|x, (mix_confirm_sig, node_keypair, ephemeral_pubkey)| {
						Some(
							documents::mix_confirm::MixConfirmOnionSecret::from_mix_confirm_onion(
								&ephemeral_pubkey,
								&node_keypair,
								&documents::mix_confirm::MixConfirmOnion {
									mix_confirm_sig,
									onion: x,
								},
							),
						)
					},
				)
				.unwrap();

		let rec_mix_confirm_sigs = check_mix_confirms(
			ephemeral_keypairs,
			mix_confirm_onion_secret,
			&pending_mix.mix_demands,
			pending_mix.output_tx_id_seed_1,
			&nodes_pubkeys,
		)
		.unwrap();

		assert_eq!(mix_confirm_sigs, rec_mix_confirm_sigs);
	}
}
