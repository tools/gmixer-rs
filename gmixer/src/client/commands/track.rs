use super::super::{config::ClientConfig, DB_DIR};
use crate::{common::*, db};
use gmixer_common::prelude::*;

use dup_crypto::{bases::b58::ToBase58, keys::ed25519::PublicKey};
use std::{collections::HashMap, convert::TryFrom};

#[tokio::main]
pub async fn track_cmd(opt: &MainOpt) {
	let config = ClientConfig::default();
	let dbs = db::client::load(opt.dir.0.join(DB_DIR).as_path())
		.expect("Error: cannot load client database");

	let nodes: Vec<PublicKey> = dbs
		.node
		.iter()
		.keys()
		.filter_map(|x| PublicKey::try_from(x.ok()?.as_ref()).ok())
		.collect();
	let nodes_ref: Vec<&PublicKey> = nodes.iter().collect();

	// TODO choose mix
	for raw in dbs.pending_mixes.iter().values() {
		if let Ok(pending_mix) = bincode::deserialize(&raw.unwrap()) {
			println!("### MIX");
			// TODO remove clone
			track_mix(&config, &dbs, nodes_ref.clone(), pending_mix).await;
		}
	}
}

/// Track a mix using the least possible requests
async fn track_mix(
	config: &ClientConfig,
	dbs: &db::client::Dbs,
	mut nodes: Vec<&PublicKey>,
	pending_mix_store: db::client::stores::PendingMixStore,
) {
	let rclient = reqwest::Client::new();
	let mut nodes_iter = ShuffleIter::new(&mut nodes);

	let mut txs = Vec::<Vec<dubp::Tx>>::new();
	let mut mix_nodes = Vec::<(Vec<usize>, Vec<usize>)>::new();
	for i in 0..pending_mix_store.path.len() + 1 {
		if txs.len() <= i {
			let node_pubkey = pending_mix_store.path[i & !1]; // last even node
			let mut result = None;
			for gva_address in OrderedShuffleIter::new(config.network.gva_nodes.iter()) {
				println!("Get tx history for tx {}", i);
				match gva::get_tx_history(&rclient, gva_address, &node_pubkey).await {
					Ok(v) => {
						result = Some(v);
						break;
					}
					Err(e) => println!("GVA Error: {:?}", e),
				}
			}
			let (received, sent) = result.expect("No working GVA node!");
			txs.push(received);
			txs.push(sent);
		}

		// Check issuer
		if i != 0 {
			let mut txs_issued = Vec::<usize>::new();
			for (txi, tx) in txs[i].iter().enumerate() {
				let mix_confirm_sig = &pending_mix_store.mix_confirms[i - 1];
				let condition = format!("SIG({})", mix_confirm_sig.inner.recipient.to_base58());
				if tx.currency == mix_confirm_sig.inner.currency
					&& mix_confirm_sig.inner.expire_time > tx.written_time
					// && mix_confirm_sig.inner.demand_time < tx.written_time
					&& tx.issuers.len() == 1
					&& tx.issuers[0] == mix_confirm_sig.pubkey
					// TODO check if all outputs are the same amount and there is enough from the issuer
					&& tx
						.outputs
						.iter()
						.filter_map(|output| output.as_ref().and_then(|output| (output.condition == condition).then_some(output.amount)))
						.map(|x| x.to_u64())
						.sum::<u64>() == mix_confirm_sig.inner.output_amount.to_u64()
					&& {
						let expected_tx_id = TxId::generate(&mix_confirm_sig.inner.output_tx_id_seeds);
						match decode_comment(&tx.comment) {
							Ok(DecodedComment::TxId(tx_id)) => tx_id == expected_tx_id,
							Ok(DecodedComment::TxIdListHash(tx_id_list_hash)) => {
								let mut tx_id_list = None;
								'nodes1: for node_pubkey in &mut nodes_iter {
									if let Some(raw) = dbs.node.get(node_pubkey).unwrap() {
										if let Ok(node_stores) = bincode::deserialize::<HashMap<NodeId, db::client::stores::NodeStore>>(&raw) {
											let mut node_stores_refs = node_stores.values().collect();
											for node_store in ShuffleIter::new(&mut node_stores_refs) {
												if let Ok(result) = api::get_tx_id_list(&rclient, &node_store.node_sig.inner.address, &tx_id_list_hash).await {
													if result.hash() == tx_id_list_hash {
														tx_id_list = Some(result);
													}
													break 'nodes1;
												}
											}
										}
									}
								}
								nodes_iter.reset();
								tx_id_list.map_or(false, |tx_id_list| tx_id_list.tx_ids.contains(&expected_tx_id))
							},
							_ => false,
						}
					} {
					txs_issued.push(txi);
				}
			}
			if txs_issued.is_empty() {
				println!("No valid tx found for issuer");
				break;
			} else {
				println!("Found {} txs for issuer", txs_issued.len());
				mix_nodes[i - 1].1.append(&mut txs_issued);
			}
		}

		// Check recipient
		if i != pending_mix_store.path.len() {
			let mut txs_received = Vec::<usize>::new();
			for (txi, tx) in txs[i].iter().enumerate() {
				let mix_confirm_sig = &pending_mix_store.mix_confirms[i];
				let condition = format!("SIG({})", mix_confirm_sig.pubkey.to_base58());
				if tx.currency == mix_confirm_sig.inner.currency
					&& mix_confirm_sig.inner.expire_time > tx.written_time
					// && mix_confirm_sig.inner.demand_time < tx.written_time
					&& tx.issuers.len() == 1
					&& tx.issuers[0] == mix_confirm_sig.inner.issuer
					// TODO check if all outputs are the same amount and there is enough from the issuer
					&& tx
						.outputs
						.iter()
						.filter_map(|output| output.as_ref().and_then(|output| (output.condition == condition).then_some(output.amount)))
						.map(|x| x.to_u64())
						.sum::<u64>() == mix_confirm_sig.inner.input_amount.to_u64()
					&& {
						let expected_tx_id = TxId::generate(&mix_confirm_sig.inner.input_tx_id_seeds);
						match decode_comment(&tx.comment) {
							Ok(DecodedComment::TxId(tx_id)) => tx_id == expected_tx_id,
							Ok(DecodedComment::TxIdListHash(tx_id_list_hash)) => {
								let mut tx_id_list = None;
								'nodes2: for node_pubkey in &mut nodes_iter {
									if let Some(raw) = dbs.node.get(node_pubkey).unwrap() {
										if let Ok(node_stores) = bincode::deserialize::<HashMap<NodeId, db::client::stores::NodeStore>>(&raw) {
											let mut node_stores_refs = node_stores.values().collect();
											for node_store in ShuffleIter::new(&mut node_stores_refs) {
												if let Ok(result) = api::get_tx_id_list(&rclient, &node_store.node_sig.inner.address, &tx_id_list_hash).await {
													if result.hash() == tx_id_list_hash {
														tx_id_list = Some(result);
													}
													break 'nodes2;
												}
											}
										}
									}
								}
								nodes_iter.reset();
								tx_id_list.map_or(false, |tx_id_list| tx_id_list.tx_ids.contains(&expected_tx_id))
							},
							_ => false,
						}
					} {
					txs_received.push(txi);
				}
			}
			if txs_received.is_empty() {
				println!("No valid tx found for recipient");
				break;
			} else {
				println!("Found {} txs for recipient", txs_received.len());

				if i != 0 {
					// check if at least one tx is valid for both issuer and recipient
					let mut ok = false;
					for tx in mix_nodes[i - 1].1.iter() {
						if txs_received.contains(tx) {
							ok = true;
							break;
						}
					}
					if !ok {
						println!("No common valid tx between issuer and recipient");
						break;
					}
				}

				mix_nodes.push((txs_received, vec![]));
			}
		}
	}
}
