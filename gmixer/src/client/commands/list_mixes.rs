use super::super::DB_DIR;
use crate::{common::*, db};
use gmixer_common::prelude::*;

use dup_crypto::bases::b58::ToBase58;

#[tokio::main]
pub async fn list_cmd(opt: &MainOpt) {
	let dbs = db::client::load(opt.dir.0.join(DB_DIR).as_path())
		.expect("Error: cannot load client database");

	dbs.pending_mixes
		.iter()
		.values()
		.filter_map(|x| x.ok())
		.for_each(|raw| {
			let pending_mix: db::client::stores::PendingMixStore =
				bincode::deserialize(&raw).unwrap();

			for (i, (mix_confirm, pubkey)) in pending_mix
				.mix_confirms
				.iter()
				.zip(pending_mix.path)
				.enumerate()
			{
				println!("#{}  {}", i, pubkey.to_base58());
				println!("  issuer:    {}", mix_confirm.inner.issuer.to_base58());
				println!("  recipient: {}", mix_confirm.inner.recipient.to_base58());
				println!(
					"  amount: {} -> {}",
					mix_confirm.inner.input_amount.val, mix_confirm.inner.output_amount.val
				);
				println!(
					"  ic: {}",
					TxId::generate(&mix_confirm.inner.input_tx_id_seeds).to_comment()
				);
				println!("   0: {:?}", mix_confirm.inner.input_tx_id_seeds[0]);
				println!("   1: {:?}", mix_confirm.inner.input_tx_id_seeds[1]);
				println!("   2: {:?}", mix_confirm.inner.input_tx_id_seeds[2]);
				println!(
					"  oc: {}",
					TxId::generate(&mix_confirm.inner.output_tx_id_seeds).to_comment()
				);
				println!("   0: {:?}", mix_confirm.inner.output_tx_id_seeds[0]);
				println!("   1: {:?}", mix_confirm.inner.output_tx_id_seeds[1]);
				println!("   2: {:?}", mix_confirm.inner.output_tx_id_seeds[2]);
				println!("  demand time: {}", mix_confirm.inner.demand_time);
				println!("  expire time: {}", mix_confirm.inner.expire_time);
				println!();
			}
		});
}
