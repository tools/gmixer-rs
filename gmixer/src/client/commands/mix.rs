use crate::{common::*, db, db::client::MixStatus};
use gmixer_common::prelude::*;

use super::super::DB_DIR;

use dup_crypto::{
	bases::b58::ToBase58,
	keys::ed25519::{Ed25519KeyPair, KeyPairFromSeed32Generator, PublicKey},
	seeds::Seed32,
};
use std::{collections::HashMap, convert::TryFrom};

#[tokio::main]
pub async fn mix_cmd(opt: &MainOpt, subopt: &ClientMixSubcommand) {
	let dbs = db::client::load(opt.dir.0.join(DB_DIR).as_path())
		.expect("Error: cannot load client database");

	let nodes: Vec<PublicKey> = dbs
		.node
		.iter()
		.keys()
		.filter_map(|x| PublicKey::try_from(x.ok()?.as_ref()).ok())
		.collect();
	let mut nodes_shuffled: Vec<&PublicKey> = nodes.iter().collect();
	let mut nodes_shuffler = ShuffleIter::new(&mut nodes_shuffled);

	let path = loop {
		let path: Vec<PublicKey> = (0..subopt.layers)
			.map(|_| *nodes_shuffler.next().expect("Not enough nodes"))
			.collect();
		path.iter()
			.enumerate()
			.for_each(|(i, &x)| println!("{}: {}", i, x.to_base58()));
		prompt("Is this path OK? [Yn] ");
		if scanrs::scanln().to_lowercase() != "n" {
			break path;
		}
		nodes_shuffler.reset();
	};

	let ephemeral_keypairs_seeds: Vec<Seed32> = (0..subopt.layers)
		.map(|_| Seed32::random().unwrap())
		.collect();
	let ephemeral_keypairs: Vec<Ed25519KeyPair> = ephemeral_keypairs_seeds
		.iter()
		.map(|x| KeyPairFromSeed32Generator::generate(x.clone()))
		.collect();

	let client_keypair = prompt_keypair();

	let (mix_demands, mix_demand_onion) = documents::mix_demand::gen_mix_demands(
		&Amount {
			val: subopt.amount,
			base: 0,
		},
		&client_keypair,
		&subopt.currency,
		&ephemeral_keypairs,
		&path,
		&subopt.recipient,
	);

	let mut pending_mix_store = db::client::stores::PendingMixStore {
		ephemeral_keypairs_seeds,
		mix_confirms: vec![],
		mix_demand_onion,
		mix_demands,
		node: None,
		output_tx_id_seed_1: TxIdSeed::generate(),
		path,
		status: MixStatus::Unsent,
	};

	send_mix_demand(&dbs, &client_keypair, &mut pending_mix_store)
		.await
		.unwrap();

	dbs.pending_mixes
		.insert(
			pending_mix_store
				.mix_demands
				.get(0)
				.unwrap()
				.input_tx_id_seed_0
				.clone(),
			bincode::serialize(&pending_mix_store).unwrap(),
		)
		.unwrap();
}

async fn send_mix_demand(
	dbs: &db::client::Dbs,
	keypair: &Ed25519KeyPair,
	pending_mix_store: &mut db::client::stores::PendingMixStore,
) -> Result<(), ClientError> {
	let node_pubkey = pending_mix_store.path.get(0).unwrap();

	let rclient = reqwest::Client::new();

	let node_stores = bincode::deserialize::<HashMap<NodeId, db::client::stores::NodeStore>>(
		&dbs.node
			.get(&node_pubkey)
			.unwrap()
			.ok_or(ClientError::NoNodeError)?,
	)
	.unwrap()
	.into_iter()
	.map(|(_, v)| v)
	.collect::<Vec<db::client::stores::NodeStore>>();

	let mix_demand_onion_wrapper_secret =
		documents::mix_demand::MixDemandOnionWrapperSecret::from_mix_demand_onion_wrapper(
			keypair,
			documents::mix_demand::MixDemandOnionWrapper {
				tx_id_seed_1: pending_mix_store.output_tx_id_seed_1.clone(),
				node_address: None,
				onion: pending_mix_store.mix_demand_onion.clone(),
			},
			node_pubkey,
		);

	for node_store in ShuffleIter::new(&mut node_stores.iter().collect()) {
		if api::send_mix_demand_onion_wrapper_secret(
			&rclient,
			&mix_demand_onion_wrapper_secret,
			&node_store.node_sig.inner.address,
		)
		.await
		.is_ok()
		{
			pending_mix_store.node = Some(node_store.node_sig.inner.clone());
			break;
		}
	}
	if pending_mix_store.node.is_none() {
		return Err(ClientError::NoWorkingNodeError);
	}

	pending_mix_store.status = MixStatus::WaitingForConfirm;

	Ok(())
}
