use super::super::{config::ClientConfig, DB_DIR};
use crate::{common::*, db};
use gmixer_common::prelude::*;

use dup_crypto::bases::b58::ToBase58;
use std::collections::HashMap;

#[tokio::main]
pub async fn node_info_cmd(opt: &MainOpt, subopt: &NodeInfoSubcommand) {
	let config = ClientConfig::default();

	if let Some(node_address) = &subopt.node_address {
		let rclient = reqwest::Client::new();
		api::get_nodes(&rclient, node_address)
			.await
			.unwrap()
			.into_iter()
			.enumerate()
			.for_each(|(i, node)| print_node_info(&config.protocol, i, node));
	} else {
		let dbs = db::client::load(opt.dir.0.join(DB_DIR).as_path())
			.expect("Error: cannot load client database");
		dbs.node
			.iter()
			.values()
			.filter_map(|x| {
				Some(
					bincode::deserialize::<HashMap<NodeId, db::client::stores::NodeStore>>(
						&x.ok()?,
					)
					.ok()?
					.into_values(),
				)
			})
			.flatten()
			.enumerate()
			.for_each(|(i, node)| print_node_info(&config.protocol, i, node.node_sig))
	}
}

fn print_node_info(config: &ProtocolConfig, i: usize, node: documents::node::NodeSig) {
	println!("{}. {}", i, node.inner.id.to_string());
	if let Err(e) = node.verify(config) {
		println!("ERROR: {:?}", e);
	}
	println!("  Currency: \"{}\"", node.inner.currency);
	println!("  Address:  {}", node.inner.address.to_string());
	println!("  Pubkey:   {}", node.inner.pubkey.to_base58());
	println!("  Software: \"{}\"", node.inner.software_version);
	println!("  Protocol: {}", node.inner.protocol_version);
	println!(
		"  Sigtime:  {}",
		humantime::format_rfc3339_seconds(
			std::time::UNIX_EPOCH
				.checked_add(std::time::Duration::from_secs(node.inner.sigtime))
				.unwrap()
		)
	);
	println!(
		"  Pk deleg: {}",
		node.inner
			.pubkey_delegation_sig
			.map_or("no".into(), |x| format!(
				"{}\n    Sigtime: {}",
				x.inner.issuer.to_base58(),
				humantime::format_rfc3339_seconds(
					std::time::UNIX_EPOCH
						.checked_add(std::time::Duration::from_secs(x.inner.sigtime))
						.unwrap()
				),
			))
	);
	println!();
}
