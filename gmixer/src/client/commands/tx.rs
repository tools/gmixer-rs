use super::super::{config::ClientConfig, DB_DIR};
use crate::{common::*, db, db::client::MixStatus};
use gmixer_common::prelude::*;

use dup_crypto::{bases::b58::ToBase58, keys::KeyPair};

#[tokio::main]
pub async fn tx_cmd(opt: &MainOpt) {
	let config = ClientConfig::default();
	let dbs = db::client::load(opt.dir.0.join(DB_DIR).as_path())
		.expect("Error: cannot load client database");

	// get mixes
	let mut needed_amount = 0u64;
	let mut mixes: Vec<db::client::stores::PendingMixStore> = dbs
		.pending_mixes
		.iter()
		.values()
		.filter_map(|x| bincode::deserialize::<db::client::stores::PendingMixStore>(&x.ok()?).ok())
		.filter(|x| x.status == MixStatus::Confirmed)
		.inspect(|x| needed_amount += x.mix_demands.get(0).unwrap().amount.to_u64())
		.collect();

	if mixes.is_empty() {
		println!("No transaction to send.");
		return;
	}

	let client_keypair = prompt_keypair();
	let client_pubkey = client_keypair.public_key();
	let signator = client_keypair.generate_signator();

	let rclient = reqwest::Client::new();

	// Get sources
	let mut result = None;
	for gva_address in OrderedShuffleIter::new(config.network.gva_nodes.iter()) {
		match gva::get_sources(&rclient, gva_address, &client_pubkey).await {
			Ok(r) => {
				result = Some(r);
				break;
			}
			Err(e) => eprintln!("Error from {:?}: {:?}", gva_address, e),
		}
	}
	let result = result.expect("No working GVA node");
	let mut available_amount = 0u64;
	let mut sources: Vec<dubp::Source> = result
		.1
		.into_iter()
		.inspect(|x| available_amount += x.get_amount().to_u64())
		.collect();
	let blockstamp = result.0;

	let mut i_source = 0usize;
	let mut used_amount = 0u64;
	let mut mixes_to_send: Vec<(&mut db::client::stores::PendingMixStore, dubp::RawTx)> = vec![];

	// Generate transactions
	for mix in mixes.iter_mut() {
		let mix_confirm_0 = mix.mix_confirms.get(0).unwrap();
		let mut amount = mix_confirm_0.inner.input_amount.to_u64();

		println!(
			"--> {} {}",
			mix.mix_demands.last().unwrap().recipient.to_base58(),
			amount
		);
		mix.path
			.iter()
			.enumerate()
			.for_each(|(i, x)| println!("  {} {}", i + 1, x.to_base58()));

		if available_amount >= amount {
			used_amount += amount;
			// TODO better source selection algo
			let mut used_sources = vec![];
			let mut change = 0u64;
			for source in &sources[i_source..] {
				i_source += 1;
				used_sources.push(source);
				let source_amount = source.get_amount().to_u64();
				available_amount -= source_amount;
				if source_amount >= amount {
					change = source_amount - amount;
					break;
				}
				amount -= source_amount;
			}

			println!("  will be sent (change = {})", change);
			let mut wanted_outputs = vec![dubp::WantedOutput {
				amount: mix_confirm_0.inner.input_amount,
				recipient: mix_confirm_0.pubkey,
			}];
			if change != 0 {
				wanted_outputs.push(dubp::WantedOutput {
					amount: Amount {
						val: change as u32,
						base: 0,
					},
					recipient: client_pubkey,
				});
			}
			let tx_id = TxId::generate(&mix_confirm_0.inner.input_tx_id_seeds);
			let tx = dubp::generate_tx(
				&signator,
				&blockstamp,
				&tx_id.to_comment(),
				&config.protocol.currency,
				&wanted_outputs,
				&used_sources,
			);
			if change != 0 {
				// Remember output for chained txs
				sources.push(dubp::Source::Utxo(dubp::Utxo {
					amount: Amount {
						val: change as u32,
						base: 0,
					},
					index: 1,
					hash: hex::encode_upper(tx.hash()),
				}));
				available_amount += change;
			}
			mixes_to_send.push((mix, tx));
		}
	}

	// Ask user
	if mixes_to_send.is_empty() {
		println!(
			"Error: Not enough money to send any tx. ({} / {})",
			available_amount, needed_amount
		);
		return;
	} else if needed_amount > available_amount {
		println!(
			"Warning: Not enough money to send all the txs. ({} / {})",
			available_amount, needed_amount
		);
	}
	prompt(&format!(
		"{} txs will be sent (total = {}). Continue? [yN]",
		mixes_to_send.len(),
		used_amount
	));
	if scanrs::scanln().to_lowercase() != "y" {
		return;
	}

	// Send txs
	'txs: for (mix, tx) in mixes_to_send {
		let mut sent_times = 0u16;

		for gva_address in OrderedShuffleIter::new(config.network.gva_nodes.iter()) {
			println!("Send tx to {:?}", gva_address);
			if let Err(e) = gva::post_tx(&rclient, gva_address, tx.clone()).await {
				println!("Tx failed: {:?}", e);
			} else {
				println!("Sent tx");
				sent_times += 1;

				if sent_times == 1 {
					mix.status = MixStatus::WaitingForTx;
					dbs.pending_mixes
						.insert(
							mix.mix_demands.get(0).unwrap().input_tx_id_seed_0.clone(),
							bincode::serialize(mix).unwrap(),
						)
						.unwrap();
				}

				if sent_times >= config.network.nodes_to_send_tx {
					continue 'txs;
				}
			}
		}

		if sent_times == 0 {
			println!("Tx failed with all GVA nodes!");
		} else {
			println!(
				"Tx sent to {}/{} GVA nodes",
				sent_times, config.network.nodes_to_send_tx
			);
		}
	}

	// old
	/*'txs: for (mix, tx) in mixes_to_send {
		for gva_address in OrderedShuffleIter::new(config.network.gva_nodes.iter()) {
			if let Err(e) = network::gva::post_tx(&rclient, gva_address, tx.clone()).await {
				println!("Error from {:?}: {:?}", gva_address, e);
			} else {
				println!("Tx sent to {:?}", gva_address);
				mix.status = MixStatus::WaitingForTx;
				dbs.pending_mixes
					.insert(
						mix.mix_demands.get(0).unwrap().input_tx_id_seed_0.clone(),
						bincode::serialize(mix).unwrap(),
					)
					.unwrap();
				continue 'txs;
			}
		}

		println!("Tx cannot be sent: no working GVA node!");
	}*/
}
