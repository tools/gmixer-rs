use crate::common::*;
use gmixer_common::prelude::*;

#[derive(Default)]
pub struct ClientConfig {
	pub network: NetworkConfig,
	pub only_member_nodes: bool,
	pub protocol: ProtocolConfig,
}
