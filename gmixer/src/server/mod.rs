use gmixer_common::prelude::*;
pub mod config;

use config::ServerConfig;

use crate::{common::*, db, network};

use dup_crypto::{
	bases::b58::ToBase58,
	keys::{
		ed25519::{Ed25519KeyPair, PublicKey},
		KeyPair,
	},
};
use log::{debug, info};
use serde::{Deserialize, Serialize};
use std::{
	collections::{HashMap, VecDeque},
	fs, io,
	path::Path,
	str,
	str::FromStr,
	sync::{Arc, RwLock},
	time::{SystemTime, UNIX_EPOCH},
};

pub const PUBKEY_DELEGATION_SIG_FILE: &str = "deleg.sig";
pub const CONFIG_FILE: &str = "config.json";
const DB_DIR: &str = "db";
pub const SEED_FILE: &str = "seed";
pub const SERVER_DIR: &str = "server";

struct ServerData {
	keypair: Ed25519KeyPair,
	config: ServerConfig,
	pubkey_delegation_sig: Option<documents::pubkey_delegation::PubkeyDelegationSig>,
}

#[derive(Deserialize, Serialize)]
pub struct ServerConfigFile {
	#[serde(flatten)]
	pub config: ServerConfig,
	pub use_pubkey_delegation: bool,
}

#[tokio::main]
pub async fn start(opt: &MainOpt) {
	let keypair = read_seed(opt.dir.0.join(SERVER_DIR).join(SEED_FILE).as_path());
	let server_data = read_config(&opt.dir.0, keypair).expect("Error: cannot read server config");

	let (indexes, dbs) = db::server::load(opt.dir.0.join(SERVER_DIR).join(DB_DIR).as_path())
		.expect("Error: cannot load server database");

	let dbs = Arc::new(dbs);
	let indexes = Arc::new(RwLock::new(indexes));

	ctrlc::set_handler({
		let dbs = dbs.clone();
		move || {
			dbs.mix_confirm.flush().unwrap();
			dbs.mix_demand.flush().unwrap();
			dbs.node.flush().unwrap();
			std::process::exit(0);
		}
	})
	.unwrap();

	let local_node_sig = Arc::new(RwLock::new(crate::common::node::generate_node_sig(
		&server_data.config,
		&server_data.keypair,
		&server_data.config.node_id,
		&server_data.pubkey_delegation_sig,
	)));
	let config = Arc::new(server_data.config);
	let keypair = Arc::new(server_data.keypair);
	let pubkey_delegation_sig = Arc::new(RwLock::new(server_data.pubkey_delegation_sig));

	info!(
		"Node ID: {}",
		local_node_sig.read().unwrap().inner.id.to_string()
	);
	info!("Pubkey: {}", keypair.public_key().to_base58());

	network::p2p::ask_nodes(&config, &dbs, &reqwest::Client::new()).await;

	let instant_requests = Arc::new(RwLock::new(VecDeque::new()));

	tokio::spawn({
		let config = config.clone();
		let dbs = dbs.clone();
		let indexes = indexes.clone();
		db::server::dbs_cleaner(config, dbs, indexes)
	});

	tokio::spawn({
		let config = config.clone();
		let dbs = dbs.clone();
		let indexes = indexes.clone();
		let instant_requests = instant_requests.clone();
		let keypair = keypair.clone();
		let local_node_sig = local_node_sig.clone();
		let pubkey_delegation_sig = pubkey_delegation_sig.clone();
		network::api::server::server_main(
			config,
			dbs,
			indexes,
			instant_requests,
			keypair,
			local_node_sig,
			pubkey_delegation_sig,
		)
	});

	network::p2p::p2p_main(
		config,
		dbs,
		indexes,
		instant_requests,
		keypair,
		local_node_sig,
		pubkey_delegation_sig,
	)
	.await
}

#[tokio::main]
pub async fn sync_nodes_cmd(opt: &MainOpt, subopt: &SyncNodesSubcommand) {
	let config = read_config_file(opt.dir.0.join(SERVER_DIR).join(CONFIG_FILE).as_path())
		.unwrap()
		.config;
	let dbs = db::server::load_dbs(opt.dir.0.join(SERVER_DIR).join(DB_DIR).as_path())
		.expect("Error: cannot load server database");
	let nodes = api::get_nodes(&reqwest::Client::new(), &subopt.node_address)
		.await
		.unwrap();
	let t = SystemTime::now()
		.duration_since(UNIX_EPOCH)
		.unwrap()
		.as_secs();
	for node_sig in nodes {
		if node_sig.inner.id == config.node_id {
			continue;
		}
		if let Err(e) = node_sig.verify(&config.protocol) {
			debug!("Rejected NodeSig: invalid: {:?}", e);
		} else {
			if let Some(entry) = dbs.node.get(node_sig.inner.pubkey).unwrap() {
				if let Ok(mut node_stores) =
					bincode::deserialize::<HashMap<NodeId, db::server::stores::NodeStore>>(&entry)
				{
					if let Some(node_store) = node_stores.get(&node_sig.inner.id) {
						if node_store.node_sig.inner.sigtime >= node_sig.inner.sigtime {
							debug!("Rejected NodeSig: older than known");
							continue;
						}
					}
					println!(
						"Added NodeSig {}@{}",
						node_sig.inner.id.to_string(),
						node_sig.inner.address.to_string()
					);
					let pubkey = node_sig.inner.pubkey;
					node_stores.insert(
						node_sig.inner.id.clone(),
						db::server::stores::NodeStore {
							node_sig,
							received_time: t,
						},
					);
					dbs.node
						.insert(pubkey, bincode::serialize(&node_stores).unwrap())
						.unwrap();
					continue;
				}
			}
			println!(
				"Added NodeSig {}@{}",
				node_sig.inner.id.to_string(),
				node_sig.inner.address.to_string()
			);
			let pubkey = node_sig.inner.pubkey;
			let mut node_stores = HashMap::new();
			node_stores.insert(
				node_sig.inner.id.clone(),
				db::server::stores::NodeStore {
					node_sig,
					received_time: t,
				},
			);
			dbs.node
				.insert(pubkey, bincode::serialize(&node_stores).unwrap())
				.unwrap();
		}
	}
}

#[tokio::main]
pub async fn recover_cmd(opt: &MainOpt, subopt: &RecoverSubcommand) {
	let config_file =
		read_config_file(opt.dir.0.join(SERVER_DIR).join(CONFIG_FILE).as_path()).unwrap();
	let keypair = read_seed(opt.dir.0.join(SERVER_DIR).join(SEED_FILE).as_path());
	println!("Pubkey: {}", keypair.public_key().to_base58());
	let rclient = reqwest::Client::new();

	println!("Fetching sources...");
	let mut result = None;
	for gva_address in OrderedShuffleIter::new(config_file.config.network.gva_nodes.iter()) {
		if let Ok(r) = gva::get_sources(&rclient, gva_address, &keypair.public_key()).await {
			result = Some(r);
			break;
		}
	}
	let (blockstamp, sources) = result.expect("No working GVA node");
	let mut amount = 0;
	let sources: Vec<dubp::Source> = sources
		.into_iter()
		.inspect(|x| amount += x.get_amount().to_u64())
		.collect();

	if amount == 0 {
		println!("Account is empty.");
		return;
	}

	println!(
		"{} will be sent to {}.",
		amount,
		subopt.recipient.to_base58()
	);
	println!("This may cancel mixes and harm your reputation of trustworthy human being.");
	prompt("Are you sure? [yN] ");
	if scanrs::scanln().to_lowercase() != "y" {
		println!("Nothing done.");
		return;
	}

	let raw_tx = dubp::generate_tx(
		&keypair.generate_signator(),
		&blockstamp,
		&String::new(),
		&config_file.config.protocol.currency,
		&[dubp::WantedOutput {
			amount: Amount {
				val: amount as u32,
				base: 0,
			},
			recipient: subopt.recipient,
		}],
		&sources.iter().collect::<Vec<&dubp::Source>>(),
	);

	dbg!(&raw_tx);

	for gva_address in OrderedShuffleIter::new(config_file.config.network.gva_nodes.iter()) {
		if let Err(e) = gva::post_tx(&rclient, gva_address, raw_tx.clone()).await {
			println!("Error: {:?}", e);
		} else {
			println!("Sent tx");
			return;
		}
	}
	println!("Error: no working GVA node.");
}

pub fn read_config_file(config_path: &Path) -> Result<ServerConfigFile, Error> {
	Ok(serde_json::from_str(
		str::from_utf8(&fs::read(config_path).expect("Error: cannot read server config file"))
			.expect("Error: bad encoding in server config file"),
	)?)
}

fn read_config(dir: &Path, keypair: Ed25519KeyPair) -> Result<ServerData, Error> {
	let config_file: ServerConfigFile =
		read_config_file(dir.join(SERVER_DIR).join(CONFIG_FILE).as_path())?;

	Ok(ServerData {
		keypair,
		config: config_file.config,
		pubkey_delegation_sig: if config_file.use_pubkey_delegation {
			Some(bincode::deserialize::<
				documents::pubkey_delegation::PubkeyDelegationSig,
			>(
				&fs::read(dir.join(SERVER_DIR).join(PUBKEY_DELEGATION_SIG_FILE))
					.expect("Error: cannot read pubkey delegation file"),
			)?)
		} else {
			None
		},
	})
}

pub fn init_config(dir: &Path) -> io::Result<Ed25519KeyPair> {
	fs::create_dir_all(dir.join(SERVER_DIR))?;

	let keypair = write_seed(dir.join(SERVER_DIR).join(SEED_FILE).as_path());

	let config_path = dir.join(SERVER_DIR).join(CONFIG_FILE);
	let config_file = if !config_path.is_file() {
		// TODO
		//prompt("Currency: ");
		//let currency = scanrs::scanln();

		println!(
			"You can sign your GMixer server's pubkey with your member private key, to proof to \
			 the world that your GMixer server is run by a real human."
		);
		println!("This is optional, but it some clients may not trust unsigned nodes.");
		prompt("Use public key delegation? [Yn] ");
		let use_pubkey_delegation = scanrs::scanln().to_lowercase() != "n";

		let config_file = ServerConfigFile {
			config: ServerConfig {
				listen: {
					loop {
						prompt("Listen to address: ");
						if let Ok(listen) = std::net::SocketAddr::from_str(&scanrs::scanln()) {
							break listen;
						}
					}
				},
				public_address: {
					loop {
						prompt("Public address: ");
						if let Ok(public_address) = NodeAddress::from_str(&scanrs::scanln()) {
							break public_address;
						}
					}
				},
				..Default::default()
			},
			use_pubkey_delegation,
		};

		fs::write(
			config_path,
			serde_json::to_string_pretty(&config_file).unwrap(),
		)
		.expect("Error: cannot write server config file");

		config_file
	} else {
		serde_json::from_str(
			str::from_utf8(&fs::read(config_path)?)
				.expect("Error: bad encoding in server config file"),
		)?
	};

	if config_file.use_pubkey_delegation {
		let pubkey_delegation_path = dir.join(SERVER_DIR).join(PUBKEY_DELEGATION_SIG_FILE);
		if !pubkey_delegation_path.is_file() {
			init_pubkey_delegation_sig(
				pubkey_delegation_path.as_path(),
				&keypair.public_key(),
				config_file.config.protocol.currency,
			)?;
		}
	}

	Ok(keypair)
}

pub fn init_pubkey_delegation_sig(
	pubkey_delegation_path: &Path,
	pubkey: &PublicKey,
	currency: String,
) -> io::Result<()> {
	fs::write(
		pubkey_delegation_path,
		bincode::serialize(&documents::pubkey_delegation::PubkeyDelegationSig::create(
			&prompt_keypair().generate_signator(),
			pubkey,
			currency,
		))
		.unwrap(),
	)
}
