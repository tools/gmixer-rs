use crate::common::*;
use gmixer_common::prelude::*;

use serde::{Deserialize, Serialize};
use std::net::{IpAddr, Ipv4Addr, SocketAddr};

#[derive(Deserialize, Serialize)]
pub struct ServerConfig {
	pub clean_db_interval: u64,
	/// Server listens to this address
	pub listen: SocketAddr,
	pub min_txs_per_mix: u16,
	pub mix_confirm_treat_interval: u64,
	pub mix_demand_treat_interval: u64,
	pub network: NetworkConfig,
	pub node_fetch_interval: u64,
	#[serde(with = "NodeId")]
	pub node_id: NodeId,
	pub protocol: ProtocolConfig,
	/// Address other nodes can access to
	pub public_address: NodeAddress,
	pub spread_node_sig_interval: u64,
	/// Duration we keep TxIdLists in cache
	pub tx_id_list_expire: u64,
	pub txs_treat_interval: u64,
	pub update_node_sig_interval: u64,
}

impl Default for ServerConfig {
	fn default() -> Self {
		ServerConfig {
			clean_db_interval: 600,
			listen: SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 10953),
			min_txs_per_mix: 5,
			mix_confirm_treat_interval: 300,
			mix_demand_treat_interval: 60,
			network: NetworkConfig::default(),
			node_fetch_interval: 600,
			node_id: NodeId::generate(),
			protocol: ProtocolConfig::default(),
			public_address: NodeAddress {
				addr: Addr::Hostname(String::from("localhost")),
				port: 10953,
				protocol: NetworkProtocol::Http,
			},
			spread_node_sig_interval: 600,
			tx_id_list_expire: 2678400,
			txs_treat_interval: 600,
			update_node_sig_interval: 600,
		}
	}
}
