use crate::{common::SOFTWARE_VERSION, server::config::ServerConfig};
use gmixer_common::prelude::*;

use dup_crypto::keys::{
	ed25519::{Ed25519KeyPair, PublicKey},
	KeyPair,
};
use std::time::{SystemTime, UNIX_EPOCH};

fn generate_node(
	config: &ServerConfig,
	id: &NodeId,
	pubkey: &PublicKey,
	pubkey_delegation_sig: &Option<PubkeyDelegationSig>,
) -> Node {
	Node {
		address: config.public_address.clone(),
		currency: config.protocol.currency.clone(),
		id: id.clone(),
		protocol_version: PROTOCOL_VERSION,
		pubkey: *pubkey,
		pubkey_delegation_sig: pubkey_delegation_sig.clone(),
		sigtime: SystemTime::now()
			.duration_since(UNIX_EPOCH)
			.unwrap()
			.as_secs(),
		software_version: String::from(SOFTWARE_VERSION),
	}
}

pub fn generate_node_sig(
	config: &ServerConfig,
	keypair: &Ed25519KeyPair,
	id: &NodeId,
	pubkey_delegation_sig: &Option<PubkeyDelegationSig>,
) -> NodeSig {
	generate_node(config, id, &keypair.public_key(), pubkey_delegation_sig)
		.sign(&keypair.generate_signator())
}

#[cfg(test)]
mod tests {
	use super::*;
	use dup_crypto::keys::ed25519::Ed25519KeyPair;
	use dup_crypto::keys::KeyPair;

	#[test]
	fn test_correct_node() -> Result<(), DocumentError> {
		let config = ServerConfig::default();
		let deleg_keypair = Ed25519KeyPair::generate_random().unwrap();
		let node_keypair = Ed25519KeyPair::generate_random().unwrap();

		generate_node_sig(
			&config,
			&node_keypair,
			&NodeId::generate(),
			&Some(PubkeyDelegationSig::create(
				&deleg_keypair.generate_signator(),
				&node_keypair.public_key(),
				String::from("foo"),
			)),
		)
		.verify(&config.protocol)
	}
}
