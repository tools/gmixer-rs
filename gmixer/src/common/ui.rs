use crate::common::*;

use clap::{Args, Parser, Subcommand};
use dup_crypto::keys::{
	ed25519::{Ed25519KeyPair, PublicKey, SaltedPassword},
	KeyPair,
};
use std::{
	convert::TryInto,
	ffi::OsStr,
	io,
	io::Write,
	path::{Path, PathBuf},
	str,
	str::FromStr,
};

#[derive(Debug)]
pub struct ConfigPath(pub PathBuf);

impl Default for ConfigPath {
	fn default() -> ConfigPath {
		ConfigPath(
			dirs::config_dir()
				.unwrap_or_else(|| Path::new(".config").to_path_buf())
				.join("gmixer"),
		)
	}
}

impl From<&OsStr> for ConfigPath {
	fn from(v: &OsStr) -> ConfigPath {
		ConfigPath(PathBuf::from(v))
	}
}

impl ToString for ConfigPath {
	fn to_string(&self) -> String {
		String::from(
			self.0
				.as_path()
				.to_str()
				.expect("Error: Config dir is not UTF-8!"),
		)
	}
}

fn pubkey_from_str(s: &str) -> Result<PublicKey, &'static str> {
	pubkey_from_b58(s).map_err(|_| "Invalid pubkey")
}

#[derive(Debug, Parser)]
#[clap(name = "gmixer")]
pub struct MainOpt {
	/// Directory
	#[clap(short, long, parse(from_os_str), default_value_t)]
	pub dir: ConfigPath,

	/// Verbose mode (-v, -vv, -vvv, etc.)
	#[clap(short, long, parse(from_occurrences))]
	pub verbose: u8,

	/// Subcommand
	#[clap(subcommand)]
	pub cmd: MainSubcommand,
}

#[derive(Debug, Parser)]
pub enum MainSubcommand {
	#[clap(subcommand)]
	Client(ClientSubcommand),

	#[clap(subcommand)]
	Server(ServerSubcommand),
}

#[derive(Debug, Subcommand)]
pub enum ClientSubcommand {
	/// Fetch mix confirm
	Confirm(ClientConfirmSubcommand),

	/// Send mix demand
	Demand,

	/// Initialize config
	Init,

	/// Update keypair
	Keypair,

	/// List tracked transactions
	List,

	/// Mix a transaction
	Mix(ClientMixSubcommand),

	/// Display info about nodes
	#[clap(name = "node")]
	NodeInfo(NodeInfoSubcommand),

	/// Get nodes from given node
	#[clap(name = "sync")]
	SyncNodes(SyncNodesSubcommand),

	/// Track a transaction being mixed
	Track,

	/// Send a transaction for mixing
	Tx,
}

#[derive(Debug, Args)]
pub struct ClientMixSubcommand {
	#[clap(short)]
	pub amount: u32,

	#[clap(short, default_value = "g1")]
	pub currency: String,

	#[clap(short, default_value = "3")]
	pub layers: u8,

	#[clap(short, parse(try_from_str=pubkey_from_str))]
	pub recipient: PublicKey,
}

#[derive(Debug, Parser)]
pub struct ClientConfirmSubcommand {}

#[derive(Debug, Parser)]
pub struct SyncNodesSubcommand {
	#[clap(short, parse(try_from_str=NodeAddress::from_str))]
	pub node_address: NodeAddress,
}

#[derive(Debug, Parser)]
pub struct NodeInfoSubcommand {
	#[clap(short, parse(try_from_str=NodeAddress::from_str))]
	pub node_address: Option<NodeAddress>,
}

#[derive(Debug, Parser)]
pub struct RecoverSubcommand {
	#[clap(short, parse(try_from_str=pubkey_from_str))]
	pub recipient: PublicKey,
}

#[derive(Debug, Subcommand)]
pub enum ServerSubcommand {
	/// Initialize config
	Init,

	/// Update pubkey delegation signature
	#[clap(name = "sig")]
	PubkeyDelegation,

	/// Send all the money on the account to a given pubkey
	Recover(RecoverSubcommand),

	/// Start server
	Start,

	/// Get nodes from given node
	#[clap(name = "sync")]
	SyncNodes(SyncNodesSubcommand),
}

pub fn prompt(val: &str) {
	print!("{}", val);
	io::stdout().flush().expect("Error: Cannot flush stdout");
}

/// Prompt scrypt credentials
pub fn prompt_keypair() -> Ed25519KeyPair {
	loop {
		let keypair =
			dup_crypto::keys::ed25519::KeyPairFromSaltedPasswordGenerator::with_default_parameters(
			)
			.generate(SaltedPassword::new(
				rpassword::prompt_password("Scrypt password: ").unwrap(),
				rpassword::prompt_password("Scrypt salt: ").unwrap(),
			));
		println!("{}", keypair.public_key());
		prompt("Is this the wanted pubkey? [Yn] ");
		if scanrs::scanln().to_lowercase() != "n" {
			return keypair;
		}
	}
}

pub fn read_seed_file(
	seed_file_path: &Path,
) -> io::Result<dup_crypto::keys::ed25519::Ed25519KeyPair> {
	Ok(dup_crypto::keys::ed25519::Ed25519KeyPair::from_seed(
		dup_crypto::seeds::Seed32::new(
			hex::decode(std::fs::read(seed_file_path)?)
				.expect("Invalid hex seed")
				.try_into()
				.expect("Invalid hex seed"),
		),
	))
}

pub fn write_seed_file(seed_file_path: &Path) -> dup_crypto::keys::ed25519::Ed25519KeyPair {
	let seed = dup_crypto::seeds::Seed32::random().unwrap();
	std::fs::write(seed_file_path, hex::encode(&seed)).unwrap();
	dup_crypto::keys::ed25519::Ed25519KeyPair::from_seed(seed)
}

pub fn read_mnemonic() -> dup_crypto::keys::ed25519::Ed25519KeyPair {
	loop {
		prompt("Mnemonic: ");
		if let Ok(mnemonic) = dup_crypto::mnemonic::Mnemonic::from_phrase(
			rpassword::read_password().unwrap(),
			dup_crypto::mnemonic::Language::English,
		) {
			let keypair = dup_crypto::keys::ed25519::Ed25519KeyPair::from_seed(
				dup_crypto::mnemonic::mnemonic_to_seed(&mnemonic),
			);
			prompt(&format!(
				"Is {} the correct pubkey? [Yn] ",
				keypair.public_key()
			));
			if scanrs::scanln().to_lowercase() != "n" {
				return keypair;
			}
		} else {
			println!("Invalid mnemonic!");
		}
	}
}

pub fn write_mnemonic() -> dup_crypto::keys::ed25519::Ed25519KeyPair {
	let mnemonic = dup_crypto::mnemonic::Mnemonic::new(
		dup_crypto::mnemonic::MnemonicType::Words12,
		dup_crypto::mnemonic::Language::English,
	)
	.unwrap();
	println!(
		"Your mnemonic is: \n{}\nPlease note these words somewhere safe. You'll need to type then \
		 each time the server starts.",
		mnemonic.phrase()
	);
	dup_crypto::keys::ed25519::Ed25519KeyPair::from_seed(dup_crypto::mnemonic::mnemonic_to_seed(
		&mnemonic,
	))
}

pub fn read_seed(seed_file_path: &Path) -> dup_crypto::keys::ed25519::Ed25519KeyPair {
	if let Ok(keypair) = read_seed_file(seed_file_path) {
		keypair
	} else {
		read_mnemonic()
	}
}

pub fn write_seed(seed_file_path: &Path) -> dup_crypto::keys::ed25519::Ed25519KeyPair {
	let keypair = loop {
		prompt("Do you prefer to store the seed in clear [1], or to use a mnemonic [2]? ");
		match scanrs::scanln().as_str() {
			"1" => {
				break write_seed_file(Path::new(seed_file_path));
			}
			"2" => {
				if seed_file_path.exists() {
					prompt("Warning: a seed file exists. Remove it? [yN] ");
					if scanrs::scanln().to_lowercase() != "y" {
						continue;
					}
					std::fs::remove_file(seed_file_path).expect("Cannot remove seed file");
				}
				break write_mnemonic();
			}
			_ => {}
		}
	};
	println!("Pubkey: {}", keypair.public_key());
	keypair
}
