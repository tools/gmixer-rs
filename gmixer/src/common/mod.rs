mod config;
pub mod node;
mod ui;

pub use config::*;
use gmixer_common::prelude::*;
pub use ui::*;

use dup_crypto::keys::{ed25519::PublicKey, PublicKey as _};
use sha2::{Digest, Sha256};

pub const SOFTWARE_VERSION: &str = "gmixer-rs 0.0.0d";

/// Request passed by server to p2p, that should be treated directly
#[derive(Debug, PartialEq)]
pub enum InstantRequest {
	MixConfirm(TxIdSeed),
	MixDemand(TxIdSeed),
	TxIdList(TxIdListHash),
}

/// Decode pubkey from base 58
/// Verify checksum if provided
/// Accepts any checksum length >=3
pub fn pubkey_from_b58(raw: &str) -> Result<PublicKey, PubkeyDecodeError> {
	let mut iter = raw.splitn(2, ':');
	let pubkey = PublicKey::from_base58(iter.next().ok_or(PubkeyDecodeError::FormatError)?)
		.map_err(PubkeyDecodeError::DecodeError)?;
	if let Some(checksum) = iter.next() {
		if checksum.len() < 3 {
			return Err(PubkeyDecodeError::FormatError);
		}
		let mut hasher1 = Sha256::new();
		hasher1.update(&pubkey.as_ref()[..32]);
		let mut hasher2 = Sha256::new();
		hasher2.update(hasher1.finalize());
		if !bs58::encode(hasher2.finalize())
			.into_string()
			.starts_with(checksum)
		{
			return Err(PubkeyDecodeError::ChecksumError);
		}
	}
	Ok(pubkey)
}

#[cfg(test)]
pub mod tests {
	use super::*;
	use dup_crypto::keys::{ed25519::Ed25519KeyPair, KeyPair};
	use rand::Rng;

	pub fn random_pubkey() -> PublicKey {
		Ed25519KeyPair::generate_random().unwrap().public_key()
	}

	#[test]
	fn test_tx_id() {
		let mut rng = rand::thread_rng();
		for _ in 0..100 {
			let tx_id = TxId::generate(&[
				TxIdSeed::generate(),
				TxIdSeed::generate(),
				TxIdSeed::generate(),
			]);
			assert_eq!(
				decode_comment(&tx_id.to_comment()).unwrap(),
				DecodedComment::TxId(tx_id)
			);
			let tx_id_list_hash = TxIdListHash(rng.gen());
			assert_eq!(
				decode_comment(&tx_id_list_hash.to_comment()).unwrap(),
				DecodedComment::TxIdListHash(tx_id_list_hash)
			);
		}
	}

	#[test]
	fn test_blockstamp_from_string() {
		assert_eq!(
			Blockstamp::from_string(String::from(
				"330001-000000CAA2F3F98A804BBC66ADD60FC968A10FDB27995BD2A70C149AF4D22615"
			))
			.unwrap(),
			Blockstamp {
				hash: [
					0, 0, 0, 202, 162, 243, 249, 138, 128, 75, 188, 102, 173, 214, 15, 201, 104,
					161, 15, 219, 39, 153, 91, 210, 167, 12, 20, 154, 244, 210, 38, 21
				],
				number: 330001,
			}
		);
	}

	#[test]
	fn test_pubkey_from_b58() {
		pubkey_from_b58("J4c8CARmP9vAFNGtHRuzx14zvxojyRWHW2darguVqjtX").unwrap();
		assert_eq!(
			pubkey_from_b58("J4c8CARmP9vAFNGtHRuzx14zvxojyRWHW2darguVqjtX:KAv").unwrap(),
			PublicKey::from_base58("J4c8CARmP9vAFNGtHRuzx14zvxojyRWHW2darguVqjtX").unwrap()
		);
		pubkey_from_b58("J4c8CARmP9vAFNGtHRuzx14zvxojyRWHW2darguVqjtx:KAv").unwrap_err();
	}
}
