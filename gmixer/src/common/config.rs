use crate::common::GvaAddress;

use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
pub struct NetworkConfig {
	/// List of GVA nodes
	pub gva_nodes: Vec<Vec<GvaAddress>>,
	/// Node storage duration
	pub node_expire: u64,
	/// Number of GVA nodes to send each tx to (for redundancy)
	pub nodes_to_send_tx: u16,
}

impl Default for NetworkConfig {
	fn default() -> Self {
		NetworkConfig {
			gva_nodes: vec![
				vec![GvaAddress(String::from("http://127.0.0.1:30901/gva"))],
				vec![
					GvaAddress(String::from("http://txmn.tk:30901/gva")),
					GvaAddress(String::from("https://g1.librelois.fr/gva")),
					GvaAddress(String::from("https://duniter-g1.p2p.legal/gva")),
				],
			],
			node_expire: 86400,
			nodes_to_send_tx: 3,
		}
	}
}
